using System;
using System.Collections.Generic;
using Person;
using Person.Colleague.Restrictions;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Text.RegularExpressions;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(List<Restriction> restrictions, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(restrictions, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(List<Restriction> restrictions, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            response.Content.Add(new Divider { BorderStyle = "none" });
            Detail detail = new Detail();
            detail.Title = "Holds and Checklist";

            if (restrictions == null || restrictions.Count == 0)
            {
                detail.Body = "You have no holds or checklist items right now!";
                response.Content.Add(detail);
                return Person.Utilities.JSON.Serializer.Serialize(response);
            }

            List holdList = new List();
            holdList.Heading = "My Holds";

            List checklistList = new List();
            checklistList.Heading = "My Checklist Items";

            var linkRegex = new Regex(@"http\S+(?="")");

            foreach (Restriction restriction in restrictions)
            {
                Link restrictionLink = new ExternalLink("");
                if (!String.IsNullOrEmpty(restriction.DetailedDescription))
                {
                    var match = linkRegex.Match(restriction.DetailedDescription);
                    if (match.Success)
                    {
                        restrictionLink = new ExternalLink(match.Groups[0].Value);
                    }
                }
                if (restriction.Severity == 100)
                    holdList.AddListItem(restriction.Title, restriction.Description, restriction.DetailedDescription, restrictionLink);
                else if (restriction.Severity > 0)
                    checklistList.AddListItem(restriction.Title, restriction.Description, restriction.DetailedDescription, restrictionLink);
            }

            if (holdList.Items.Count > 0)
                detail.Content.Add(holdList);

            if (checklistList.Items.Count > 0)
                detail.Content.Add(checklistList);

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}