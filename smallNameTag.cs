using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.Portal.UserProfile userProfile, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(userProfile, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.Portal.UserProfile userProfile, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();

            NameTag nameTag = new NameTag
            {
                NametagStyle = "horizontal",
                Name = userProfile.DisplayName,
                Description = "User ID: " + userProfile.UserID,
                NameFontSize = "1.25rem",
                NameFontWeight = "medium",
                ImageHeight = "7rem",
                ImageWidth = "7rem",
                ImageBorderRadius = "tight",
                Image = new Image
                {
                    Url = userProfile.PicIDURL + "/480",
                    Opacity = 100
                },
                MarginTop = "medium",
                MarginBottom = "none"
            };

            response.Content.Add(nameTag);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}