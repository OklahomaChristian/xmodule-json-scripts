using System;
using XModComm;
using Person;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.CampusInformation.Department department, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(department, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.CampusInformation.Department department, TokenData tokenData)
        {
            XModComm.Response response = new Response();
	    response.Content.Add(new Divider { BorderStyle = "none" });
            Detail detail = new Detail();
            List phoneLocList = new List();

            detail.Title = department.Name;

            Thumbnail thumbnail = new Thumbnail();
            thumbnail.Url = department.Building != null ? String.Format("http://www.oc.edu/{0}", department.Building.bPhoto) : String.Empty;
            thumbnail.Alt = department.Building != null ? department.Building.Title : String.Empty;

            if (!String.IsNullOrEmpty(thumbnail.Url))
                detail.Thumbnail = thumbnail;

            if (!String.IsNullOrEmpty(department.Phone) && department.Phone.Length > 0)
            {
                string phoneNumber = Regex.Replace(department.Phone, @"\D", "");
                string formattedPhoneNumber = Regex.Replace(phoneNumber, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                phoneLocList.AddListItem(formattedPhoneNumber, "Phone", null, new ExternalLink("tel:" + Regex.Replace(phoneNumber, "[^0-9_]", "")));
            }

            if (!String.IsNullOrEmpty(department.Fax) && department.Fax.Length > 0)
            {
                string faxNumber = Regex.Replace(department.Fax, @"\D", "");
                if (faxNumber.Length == 4) // listed as extension
                    faxNumber = String.Format("405425{0}", faxNumber);
                string formattedFaxNumber = Regex.Replace(faxNumber, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                phoneLocList.AddListItem(formattedFaxNumber, "Fax", null);
            }

            if (department.Building != null)
            {
                if (!String.IsNullOrEmpty(department.Building.Title))
                {
                    Dictionary<string, string> queryParameters = new Dictionary<string, string>(2);
                    queryParameters.Add("filter", department.Building.Title);
                    queryParameters.Add("_recenter", "true");
                    phoneLocList.AddListItem(department.Building.Title, "Location", null, new ModuleLink("map", "index", queryParameters));
                }
                else
                {
                    if (department.Building.Title != null)
                    {
                        phoneLocList.AddListItem(department.Building.Title, "Location", null);
                    }
                }
            }

            //memberList.AddListItem(department.DepartmentMembers.Length.ToString(), null, null);

            List memberList = new List();
            if (department.DepartmentMembers != null)
            {
                foreach (Person.CampusInformation.DepartmentMember member in department.DepartmentMembers)
                {
                    if (String.IsNullOrEmpty(member.UserID))
                    {
                        continue;
                    }

                    memberList.AddListItem
                    (
                        String.IsNullOrEmpty(member.DisplayName) ?
                            String.Format("{0} {1}", member.FirstName, member.LastName) :
                            member.DisplayName,
                        member.Title,
                        (!String.IsNullOrEmpty(member.Extension) && member.Extension.Length > 0) ?
                            String.Format("(405) 425-{0}", member.Extension) :
                            "",
                        String.IsNullOrEmpty(member.IdToken) ? null :
                            new XModuleLink("userdetails", String.Format("/{0}", member.IdToken))
                    );
                }
            }

            if (phoneLocList.Items.Count > 0)
                detail.Content.Add(phoneLocList);
            if (memberList.Items.Count > 0)
                detail.Content.Add(memberList);

            response.Content.Add(detail);

            if (detail.Content.Count == 0)
                detail.Content = null;

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
