using System;
using XModComm;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            response.Metadata.RedirectLink = new ExternalLink(Person.Utilities.CashNet.GetCashNetURL(userId).ToString());
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}