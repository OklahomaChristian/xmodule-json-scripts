using System;
using Person;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Ethos.Event[] events, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            response.Content.Add(new Divider() { BorderStyle = "none" });
            Detail detail = new Detail();
            detail.Title = "Chapel Events";

            List eventsList = new List();
            string lastDate = DateTime.Today.ToShortDateString();
            eventsList.Heading = DateTime.Today.ToString("MMMM dd, yyyy");
            eventsList.Heading = eventsList.Heading.ToUpper();

            foreach (Ethos.Event ethosEvent in events)
            {
                if (ethosEvent.EventStart < DateTime.Today)
                    continue;

                if (ethosEvent.EventStart > DateTime.Today.AddDays(30))
                    break;

                if (ethosEvent.PrivateEvent)
                    continue;

                if (ethosEvent.EventStart.ToShortDateString() != lastDate && eventsList.Items.Count > 0) //New day block
                {
                    detail.Content.Add(eventsList); // add day's worth of events
                    eventsList = new List(); // reset items
                    eventsList.Heading = ethosEvent.EventStart.ToString("MMMM dd, yyyy"); // reset date
                    eventsList.Heading = eventsList.Heading.ToUpper();
                }
                lastDate = ethosEvent.EventStart.ToShortDateString(); // set current date

                string title = String.Format("{0}", ethosEvent.EventName);
                string subTitle = String.Format("{0}, {1} - {2}", ethosEvent.EventStart.ToString("MMMM dd"), ethosEvent.EventStart.ToString("h:mm"), ethosEvent.EventEnd.ToString("h:mm tt"));

                eventsList.AddListItem(title, ethosEvent.Category, subTitle, new RelativeLink("/GetEthosEvents/Details/" + ethosEvent.ID));
            }

            if (eventsList.Items.Count == 0)
            {
                eventsList.AddListItem("No upcoming events found", null, null);
            }

            detail.Content.Add(eventsList);

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}