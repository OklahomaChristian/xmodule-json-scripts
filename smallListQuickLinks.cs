using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.Portal.Service[] bookmarks, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(bookmarks, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.Portal.Service[] bookmarks, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();

            BlockHeading blockHeading = new BlockHeading
            {
                Heading = "Bookmarks",
                HeadingTextColor = "#5c0e1d",
                Image = new Image
                {
                    Url = "https://static.modolabs.com/ocu/links.png",
                    BorderRadius = "full",
                    BackgroundColor = "#1173b0",
                    Opacity = 100
                },
                ImageWidth = "2.25rem",
                ImageHeight = "2.25rem",
                MarginBottom = "tight",
                DescriptionLineClamp = 2,
                HeadingLineClamp = 2,
                Buttons = new System.Collections.Generic.List<object>
                {
                    new Dropdown
                    {
                        ScreenReaderTitle = "List of actions",
                        ToggleSize = "small",
                        ToggleActionStyle = "normal",
                        ToggleBorderColor = "transparent",
                        AccessoryIcon = "overflow",
                        ShowDisclosureIcon = false,
                        ToggleTextColor = "theme:tertiary_text_color",
                        Items = new System.Collections.Generic.List<DropdownItem>
                        {
                            new DropdownItem
                            {
                                Title = "View all services",
                                Url = new XModComm.StandardLinks.ModuleLink
                                {
                                    Module = new XModComm.StandardLinks.ModuleObject
                                    {
                                        ID = "services"
                                    }
                                }
                            },
                            new DropdownItem
                            {
                                Title = "Customize my Bookmarks",
                                Url = new XModComm.StandardLinks.XModuleLink
                                {
                                    XModule = new XModComm.StandardLinks.XModuleObject
                                    {
                                        ID = "quicklinks",
                                        RelativePath = "/edit"
                                    }
                                }
                            }

                        }
                    }
                }
            };
            response.Content.Add(blockHeading);

            List list = new List
            {
                ListStyle = "unpadded",
                TextBlockMargin = "xtight",
                BorderColor = "transparent",
                TitleFontSize = "0.875rem",
                TitleFontWeight = "normal",
                MarginTop = "none",
                ShowAccessoryIcons = false
            };

            Array.Sort(bookmarks, (x, y) => x.Title.CompareTo(y.Title));


            //	List editQuickLinksList = new List() { Grouped = true };
            //editQuickLinksList.Items.Add(
            //new Item()
            //{
            //    Title = "Edit QuickLinks",
            //    Link = new XModuleLink("quicklinks", "/edit")
            //}
            //);
            //	detail.Content.Add(editQuickLinksList);



            //foreach quicklink that the user has
            //display the quicklink
            //list items with external links
            SimpleListItem blackboardItem = new SimpleListItem
            {
                Url = new XModComm.StandardLinks.ExternalLink
                {
                    External = "https://bb.oc.edu",
                    TargetNewWindow = true
                },
                Title = "Blackboard"
            };
            list.Items.Add(blackboardItem);

            SimpleListItem emailItem = new SimpleListItem
            {
                Url = new XModComm.StandardLinks.ExternalLink
                {
                    External = "https://webmail.oc.edu",
                    TargetNewWindow = true
                },
                Title = "OC Email"
            };
            list.Items.Add(emailItem);

            SimpleListItem studentAccountItem = new SimpleListItem
            {
                Url = new XModComm.StandardLinks.ExternalLink
                {
                    External = "https://services.oc.edu/redirect/496",
                    TargetNewWindow = true
                },
                Title = "Student Account Online"
            };
            list.Items.Add(studentAccountItem);


            if (bookmarks.Length > 0)
            {
                foreach (Person.Portal.Service quickLink in bookmarks)
                {
                    SimpleListItem link = new SimpleListItem();
                    if (tokenData.PageType == "small" || (tokenData.PageType == "large" && (tokenData.Platform == "ios" || tokenData.Platform == "android")))
                        link.Url = new XModuleLink("utilities", String.Format("/SSORedirect/{0}", quickLink.ID));
                    else
                        link.Url = new ExternalLink(String.Format("{0}", quickLink.RedirectURI));
                    link.Title = quickLink.Title;

                    list.Items.Add(link);
                }
            }
            else
            {
                list.Items.Add(new SimpleListItem
                {
                    Title = "No results found"
                });
            }

            response.Content.Add(list);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}