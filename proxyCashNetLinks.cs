using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using Person.Portal;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Person.Portal.UserProfile userProfile, Dictionary<string, Proxy.ProxyProfile> dependents, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = "Student Account (Cashnet)";

            List list = new List() { };
            int studentAccountDependentCount = 0;

            if (userProfile.isStudent || userProfile.Roles.Contains(Person.UserType.Alumni) || userProfile.Roles.Contains(Person.UserType.SummerAcademy))
            {
                XModComm.Thumbnail thumbnail = new Thumbnail();
                thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
                thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;
                //thumbnail.MaxHeight = 80;
                //thumbnail.MaxWidth = 80;

                list.AddListItem("Student Account for " + userProfile.DisplayName, null, null, new XModuleLink("utilities", "/RedirectToCashNet"), thumbnail);
            }

            if (dependents.Count > 0)
            {
                foreach (Proxy.ProxyProfile dependent in dependents.Values)
                {
                    foreach (Proxy.ProxyPermissions proxyPermissions in dependent.ProxyPermissions)
                    {
                        if (proxyPermissions == Proxy.ProxyPermissions.FinancialAccount)
                        {
                            studentAccountDependentCount++;

                            Thumbnail thumbnail = new Thumbnail();
                            thumbnail.Url = dependent.DependentUserProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(dependent.DependentUserProfile.UserID, dependent.DependentUserProfile.PicIDURL) : String.Empty;
                            thumbnail.Alt = dependent.DependentUserProfile.DisplayName != null ? dependent.DependentUserProfile.DisplayName : String.Empty;
                            //thumbnail.MaxHeight = 80;
                            //thumbnail.MaxWidth = 80;

                            list.AddListItem("Student Account for " + dependent.DependentUserProfile.DisplayName, null, null, new XModuleLink("utilities", "/RedirectProxyToCashNet/" + dependent.DependentID), thumbnail);
                        }
                    }
                }
            }

            
                if (studentAccountDependentCount == 0 && !userProfile.isStudent)
                    list.AddListItem("Student account access has not yet been granted by a student.", null, null);

                detail.Content.Add(list);
                response.Content.Add(detail);
            

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}