using System;
using System.Linq;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(double termGPA, double cumulativeGPA, double cumulativeGPATerm)
        {
            XModComm.Response response = new Response();
			
			response.RegionContent.Add(new Item() { Title = "Term (Final Grades Only)", Label = null, Description = termGPA.ToString("N2") });
			response.RegionContent.Add(new Item() { Title = "Cumulative", Label = null, Description = cumulativeGPA.ToString("N2") });
			response.RegionContent.Add(new Item() { Title = "Cumulative up to selected term", Label = null, Description = cumulativeGPATerm.ToString("N2") });

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}