using System;
using System.Linq;
using System.Text;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Tables;
using Person.Portal;

namespace Dynamic
{
	public class Script
	{
		public string GetJSON(System.Collections.Generic.List<UserProfile> profileList, TokenData tokenData)
		{
			XModComm.Response response = new Response();

			if (tokenData.PageType == "small")
			{
				List list = new List();

				if (profileList != null && profileList.Count > 0)
				{
					foreach (UserProfile profile in profileList)
					{
						Thumbnail thumbnail = new Thumbnail();
						thumbnail.Url = profile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(profile.UserID, profile.PicIDURL) : String.Empty;
						thumbnail.Alt = profile.DisplayName != null ? profile.DisplayName : String.Empty;

						list.AddListItem(profile.DisplayName, null, null, new XModuleLink("userdetails", "/" + profile.UserID.ToString()), thumbnail);
					}
				}
				else
				{
					list.AddListItem("No waitlisted students found", null, null);
				}

				response.RegionContent.Add(list);
			}
			else
			{
				Table table = new Table();

				table.ColumnOptions.Add(new ColumnOption() { Header = "Name" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "ID Number" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Class" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "FERPA" });

				if (profileList != null && profileList.Count > 0)
				{
					foreach (UserProfile profile in profileList)
					{
						Row row = new Row();
						row.Url = new XModuleLink("userdetails", "/" + profile.UserID.ToString());

						//Name
						row.Cells.Add(new Cell() { Title = profile.DisplayName });

						//ID Number
						row.Cells.Add(new Cell() { Title = profile.UserID });

						//Class
						row.Cells.Add(new Cell() { Title = profile.Classification });

						//FERPA
						row.Cells.Add(new Cell() { Title = "View Permissions" });

						table.Rows.Add(row);
					}
				}
				else
				{
					Row row = new Row();
					row.Cells.Add(new Cell() { Title = "No waitlisted students found" });
					row.Cells.Add(new Cell() { Title = "" });
					row.Cells.Add(new Cell() { Title = "" });
					row.Cells.Add(new Cell() { Title = "" });
					table.Rows.Add(row);
				}

				response.RegionContent.Add(table);
			}

			return Person.Utilities.JSON.Serializer.Serialize(response);
		}
	}
}
