using System;
using Person;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;
using System.Collections.Generic;

namespace Dynamic
{
    public class Script
    {
        public static string GetJSON(Ethos.UserProfile up, Ethos.Entry[] userKudos, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();

            XModComm.UI.Event.EventList eventList = new XModComm.UI.Event.EventList()
            {
                TitleLineClamp = 3,
                DescriptionLineClamp = 1,
                MarginTop = "none",
                MarginBottom = "none",
                ShowTopBorder = false,
                ShowBottomBorder = false,
                Heading = new BlockHeading
                {
                    Heading = "My Chapel History",
                    HeadingLevel = "3",
                    MarginTop = "none",
                    MarginBottom = "none"
                },
                Items = new List<XModComm.UI.Event.EventListItem>()
            };

            foreach (Ethos.Entry ethosEvent in userKudos)
            {
                eventList.Items.Add(new XModComm.UI.Event.EventListItem
                {
                    DatetimePrimaryLine = ethosEvent.Event.EventStart.ToString("MMMM dd"),
                    DatetimeSecondaryLine = ethosEvent.Event.EventStart.ToString("h:mm tt"),
                    Title = ethosEvent.Event.EventName,
                    Description = String.Format("{0} via {1}", ethosEvent.CheckInTime, ethosEvent.CheckInTypeName),
                    Url = new XModComm.StandardLinks.XModuleLink
                    {
                        XModule = new XModComm.StandardLinks.XModuleObject
                        {
                            ID = "ethos",
                            RelativePath = "/GetEthosEvents/Details/" + ethosEvent.Event.ID
                        }
                    },
                    DividerColor = "#f04e3e"
                });
            }

            response.Content.Add(eventList);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}