using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using Person.Portal;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Person.Portal.UserProfile userProfile, Dictionary<string, Proxy.ProxyProfile> dependents, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = "Financial Aid";

            List list = new List() {};
            int studentAccountDependentCount = 0;

            if (userProfile.isStudent || userProfile.Roles.Contains(Person.UserType.Admitted) || userProfile.Roles.Contains(Person.UserType.Accepted))
            {
                Thumbnail thumbnail = new Thumbnail();
                thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
                thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;
                //thumbnail.MaxHeight = 80;
                //thumbnail.MaxWidth = 80;

                list.AddListItem("Financial Aid for " + userProfile.DisplayName, null, null, new XModuleLink("utilities", "/SSORedirect/153"), thumbnail);
            }

            if (dependents.Count > 0)
            {
                foreach (Proxy.ProxyProfile dependent in dependents.Values)
                {
                    foreach (Proxy.ProxyPermissions proxyPermissions in dependent.ProxyPermissions)
                    {
                        if (proxyPermissions == Proxy.ProxyPermissions.FinancialAid)
                        {
                            studentAccountDependentCount++;

                            Thumbnail thumbnail = new Thumbnail();
                            thumbnail.Url = dependent.DependentUserProfile.PicIDURL != null ? dependent.DependentUserProfile.PicIDURL + ".jpg" : String.Empty;
                            thumbnail.Alt = dependent.DependentUserProfile.DisplayName != null ? dependent.DependentUserProfile.DisplayName : String.Empty;
                            //thumbnail.MaxHeight = 80;
                            //thumbnail.MaxWidth = 80;

                            list.AddListItem("Financial Aid for " + dependent.DependentUserProfile.DisplayName, null, null, new XModuleLink("utilities", "/RedirectProxyToFinancialAid/" + dependent.DependentUserProfile.IDToken), thumbnail);
                        }
                    }
                }
            }

            //if (list.Items.Count == 1)
            //    response.Metadata.RedirectLink = list.Items[0];
            //else
            //{
                if (studentAccountDependentCount == 0 && (!userProfile.isStudent))
                    list.AddListItem("Financial aid access has not yet been granted by a student.", null, null);

                detail.Content.Add(list);
                response.Content.Add(detail);
            //}

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
