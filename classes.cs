using System;
using System.Linq;
using Person;
using Person.Colleague;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;
using XModComm.Tables;
using Person.Colleague.Schedule;
using ExtensionMethods;
using XModComm.Arguments;
using System.Collections.Generic;

namespace ExtensionMethods
{
    public static class SectionExtensions
    {
        public static T GetSectionDetails<T>(this ClassSchedule.Section section, string term, bool isDependentSchedule) where T : class
        {
            Type genericType = typeof(T);

            object obj = null;
            switch (genericType.Name)
            {
                case "List":
                    obj = GetList(section, term, isDependentSchedule);
                    break;
                case "Row":
                    obj = GetRow(section, term, isDependentSchedule);
                    break;
            }

            return (T)obj ?? null;
        }

        static Row GetRow(ClassSchedule.Section section, String term, bool isDependentSchedule) // large
        {
            Row row = new Row();
            if (!isDependentSchedule)
                row.Url = new XModComm.StandardLinks.RelativePathLink { RelativePath = String.Format("/ClassDetails/{0}/{1}", term, section.STTR_SCHEDULE_ID) };
            //String.Format("/ClassDetails/{0}/{1}", term, section.STTR_SCHEDULE_ID);

            row.Cells.Add(new Cell() { Title = section.ShortTitle }); // Course Title
            row.Cells.Add(new Cell() { Title = section.MidTermGrade ?? "-" }); // MidTerm
            row.Cells.Add(new Cell() { Title = section.VerifiedGrade ?? "-" }); // Final Grades
            //row.Cells.Add(new Cell() { Title = section.Name }); // Course Description
            //row.Cells.Add(new Cell() { Title = section.Credits }); //Credits

            bool showEvaluation = false;
            if (section.EvaluationDateTimes != null && !isDependentSchedule)
            {
                foreach (ClassSchedule.EvaluationDateTime evaluation in section.EvaluationDateTimes)
                {
                    if (evaluation != null &&
                        (((evaluation.StartDateTime.Year != 1967 && evaluation.EndDateTime.Year != 1967) && evaluation.IsScheduledEvaluationOpen) || evaluation.StatusType.ToString() == "Open")
                        && !evaluation.AlreadyEvaluated)
                    {
                        showEvaluation = true;
                        break;
                    }
                }
            }

            var meetingTimeCells = new System.Collections.Generic.List<Cell>(4);
            for (int i = 0; i < 4; i++)
                meetingTimeCells.Add(new Cell());
            if (section.MeetingTimes != null)
            {
                foreach (ClassSchedule.MeetingTime meetingTime in section.MeetingTimes)
                {
                    //Room Number
                    meetingTimeCells[0].Title += meetingTime.Building.Code == "TBA" ? "-" : String.Format("{0} {1}<br>", meetingTime.Building.Code, meetingTime.Room);

                    //Days
                    string days = "";
                    if (meetingTime.Days != null)
                        meetingTime.Days.ToList().ForEach((string day) => { days = String.Format("{0} {1}", days, day); });
                    meetingTimeCells[1].Title += String.Format("{0}<br>", days);

                    //Times
                    meetingTimeCells[2].Title += String.Format("{0}-{1}<br>", meetingTime.StartTime, meetingTime.EndTime);

                    //Dates
                    meetingTimeCells[3].Title += String.Format("{0}-{1}<br>", meetingTime.StartDate.Substring(0, meetingTime.StartDate.Length - 3), meetingTime.EndDate.Substring(0, meetingTime.EndDate.Length - 3));


                }
            }

            //Instructors
            string professors = "";
            if (section.FacultyProfiles != null)
            {
                foreach (Person.Portal.UserProfile professor in section.FacultyProfiles)
                {
                    if (professor != null)
                    {
                        professors = String.Format("{0}{1},", professors, professor.DisplayName);
                    }
                }
            }

            //meetingTimeCells[4].Title += String.Format("{0}", professors);

            string distinctProfessors = "";
            //if (meetingTimeCells[4].Title != null)
            //    meetingTimeCells[4].Title.TrimEnd(',').Split(',').Distinct().ToList().ForEach((string professor) => { distinctProfessors = String.Format("{0} {1},", distinctProfessors, professor); });
            //meetingTimeCells[4].Title = distinctProfessors.TrimEnd(',').TrimStart(' ');

            //if (showEvaluation)
                //meetingTimeCells[4].SubTitle = "Instructor Evaluation Needed";

            meetingTimeCells.ForEach((Cell c) => { row.Cells.Add(c); });

            return row;
        }

        static XModComm.UI.List GetList(ClassSchedule.Section section, String term, bool isDependentSchedule) // small
        {
            // TODO: NO LINK TO ClassDetails

            var list = new XModComm.UI.List();
            list.ListStyle = "grouped";
            list.Heading = String.Format("{0} {1}", section.ShortTitle, section.Name);

            string midTermGrade = section.MidTermGrade ?? "-";
            string verifiedGrade = section.VerifiedGrade ?? "-";
            list.AddListItem(String.Format("{0} / {1} / {2}", section.Credits, midTermGrade, verifiedGrade), "Grade Summary", "Credits/Midterm/Final");

            string times = "";
            string dates = "";
            if (section.MeetingTimes != null)
            {
                foreach (var meetingTime in section.MeetingTimes)
                {
                    if (meetingTime.Days != null)
                        meetingTime.Days.ToList().ForEach((string day) => { times = String.Format("{0} {1}", times, day); });
                    times += String.Format(" {0}-{1} ({2} / {3})<br>", meetingTime.StartTime, meetingTime.EndTime, meetingTime.Building.Code ?? "-", meetingTime.Room ?? "-");
                    dates += String.Format("{0}-{1},", meetingTime.StartDate, meetingTime.EndDate);
                }
            }

            string distinctDates = "";
            dates.TrimEnd(',').Split(',').Distinct().ToList().ForEach((string date) => { distinctDates = String.Format("{0}{1}<br>", distinctDates, date); });
            list.AddListItem(times.TrimStart(' '), "Times/Dates", distinctDates.TrimStart(' '));
            if (!isDependentSchedule)
                list.AddListItem("Class Details", null, null, new RelativeLink(String.Format("/ClassDetails/{0}/{1}", term, section.STTR_SCHEDULE_ID)));
            return list;
        }
    }
}

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(System.Collections.Generic.List<ScheduleArgument> scheduleArguments, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(scheduleArguments, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(System.Collections.Generic.List<ScheduleArgument> scheduleArguments, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            TabContainer tabContainer = new TabContainer();
            System.Collections.Generic.List<string> terms = new System.Collections.Generic.List<string>();
            string currentTerm = null;
            string selectedTerm = "";

            if (scheduleArguments.Count > 0)
            {
                selectedTerm = scheduleArguments[0].SelectedTerm;
            }

            foreach (ScheduleArgument argument in scheduleArguments)
            {
                ClassSchedule classSchedule = argument.ClassSchedule;
                terms.AddRange(argument.ActiveStudentTerms);
                string displayName = argument.DisplayName;

                if (tokenData.PageType == "small" && classSchedule != null)
                {
                    string heading = GetTermDesc(classSchedule.Term) + " CLASSES ";
                    Detail detail = new Detail { Title = heading, Description = displayName };

                    classSchedule.Sections.ToList().ForEach((ClassSchedule.Section section) => { detail.Content.Add(section.GetSectionDetails<XModComm.UI.List>(classSchedule.Term, argument.IsDependentSchedule)); });

                    AjaxList gpaList = new AjaxList() { Grouped = true };
                    gpaList.Heading = "GPA";
                    gpaList.Items = new Item() { AjaxRelativePath = String.Format("/GetGPA/{0}/{1}", classSchedule.Term, classSchedule.UserID) };
                    detail.Content.Add(gpaList);

                    var orderBooksList = new XModComm.UI.List();
                    orderBooksList.ListStyle = "grouped";
                    orderBooksList.AddListItem("Purchase your books from the bookstore", null, null, new ExternalLink("https://oc.slingshotedu.com"));
                    detail.Content.Add(orderBooksList);

                    if (!argument.IsDependentSchedule)
                    {
                        var list = new XModComm.UI.List();
                        list.ListStyle = "grouped";
                        list.AddListItem("Subscribe to your class schedule in your calendar application", null, null, new ExternalLink(String.Format("webcal://calendar.oc.edu/studentSchedule/{0}", argument.IDToken)));
                        detail.Content.Add(list);
                    }

                    response.Content.Add(detail);
                }
                else if (classSchedule != null)// DEFAULT TO LARGE
                {

                    XModComm.UI.Column.TwoColumn twoColumn = new XModComm.UI.Column.TwoColumn()
                    {
                        MarginTop = "responsive",
                        SecondaryColumn = new XModComm.UI.Column.ColumnContents
                        {
                            Content = new List<object>
                            {
                                new BlockHeading
                                {
                                    Heading = "GPA",
                                    HeadingLevel = "2",
                                    MarginTop = "tight",
                                    MarginBottom = "none"
                                },
                                new AjaxList
                                {
                                    Items = new Item() { AjaxRelativePath = String.Format("/GetGPA/{0}/{1}", classSchedule.Term, classSchedule.UserID) }
                                }
                            }
                        },
                        PrimaryColumn = new XModComm.UI.Column.ColumnContents
                        {
                            Content = new List<object>
                            {
                                new BlockHeading
                                {
                                    Heading = GetTermDesc(classSchedule.Term) + " Classes",
                                    HeadingLevel = "1",
                                    MarginTop = "tight",
                                    MarginBottom = "none"
                                }
                            }
                        }
                    };

                    Table classTable = new Table()
                    {
                        ColumnHeaders = true,
                        Rows = new List<XModComm.Tables.Row>()
                        {
                            new XModComm.Tables.Row
                            {
                                Cells = new List<Cell>
                                {
                                    new Cell {Title = "Course Title"},
                                    new Cell {Title = "Midterm"},
                                    new Cell {Title = "Final"},
                                    //new Cell {Title = "Course ID"},
                                    //new Cell {Title = "Credits"},
                                    new Cell {Title = "Room"},
                                    new Cell {Title = "Days"},
                                    new Cell {Title = "Times"},
                                    new Cell {Title = "Dates (With Finals)"},
                                    //new Cell {Title = "Instructor"}
                                }
                            }
                        }
                    };
                    if (classSchedule != null)
                        classSchedule.Sections.ToList().ForEach((ClassSchedule.Section section) => { classTable.Rows.Add(section.GetSectionDetails<Row>(classSchedule.Term, argument.IsDependentSchedule)); });

                    //string heading = GetTermDesc(classSchedule.Term) + " CLASSES ";
                    //Tab tab = new Tab() { Title = displayName };

                    //Table table = new Table(0, 0) { Heading = heading };

                    //new string[] { "Course Title", "Midterm", "Final", "Course ID", "Credits", "Room", "Days", "Times", "Dates (Includes Finales)", "Instructor" }.ToList().ForEach(
                    //    (string header) => { table.ColumnOptions.Add(new ColumnOption() { Header = header }); });

                    //if (classSchedule != null)
                    //    classSchedule.Sections.ToList().ForEach((ClassSchedule.Section section) => { table.Rows.Add(section.GetSectionDetails<Row>(classSchedule.Term, argument.IsDependentSchedule)); });

                    twoColumn.PrimaryColumn.Content.Add(classTable);

                    //AjaxList gpaList = new AjaxList();
                    //gpaList.Heading = "GPA";
                    //gpaList.Items = new Item() { AjaxRelativePath = String.Format("/GetGPA/{0}/{1}", classSchedule.Term, classSchedule.UserID) };
                    //tab.Content.Add(gpaList);

                    twoColumn.PrimaryColumn.Content.Add(new Divider { BorderStyle = "none" });
                    twoColumn.SecondaryColumn.Content.Add(new Divider { BorderStyle = "none" });

                    XModComm.UI.Grid.Grid grid = new XModComm.UI.Grid.Grid
                    {
                        GridStyle = "springboard",
                        GridDensity = "medium",
                        HorizontalAlignment = "center",
                        ImageSize = "xxsmall",
                        TitleFontSize = "0.875rem",
                        TitleTextColor = "theme:primary_text_color",
                        DescriptionFontSize = "0.875rem",
                        DescriptionTextColor = "theme:primary_text_color",
                        MarginTop = "none",
                        MarginBottom = "none",
                        Items = new List<XModComm.UI.Grid.GridItem> { }
                    };

                    XModComm.UI.Grid.GridItem purchaseBooks = new XModComm.UI.Grid.GridItem
                    {
                        Title = "Purchase Books",
                        Image = new XModComm.UI.Grid.Image
                        {
                            URL = "https://static.modolabs.com/xmodule/iconsets/stroke-shaded-black/90/shopping_cart.png"
                        },
                        Link = new XModComm.StandardLinks.ExternalLink
                        {
                            External = "https://oc.slingshotedu.com"
                        }
                    };
                    grid.Items.Add(purchaseBooks);

                    if (!argument.IsDependentSchedule)
                    {
                        XModComm.UI.Grid.GridItem calendar = new XModComm.UI.Grid.GridItem
                        {
                            Title = "Export Calendar",
                            Image = new XModComm.UI.Grid.Image
                            {
                                URL = "https://static.modolabs.com/xmodule/iconsets/stroke-shaded-black/90/calendar.png"
                            },
                            Link = new XModComm.StandardLinks.ExternalLink
                            {
                                External = String.Format("https://calendar.oc.edu/studentSchedule/{0}", argument.IDToken)
                            }
                        };
                        grid.Items.Add(calendar);
                    }

                    twoColumn.SecondaryColumn.Content.Add(grid);

                    //tabContainer.Tabs.Add(tab);
                    response.Content.Add(twoColumn);
                }
                else if (tokenData.PageType == "small")
                {
                    Detail detail = new Detail { Title = "CLASSES", Description = displayName };
                    List list = new List();
                    list.ListStyle = "grouped";
                    list.AddListItem("Class schedule not found for " + argument.SelectedTerm, null, null);
                    detail.Content.Add(list);
                    response.Content.Add(detail);
                }
                else
                {
                    Tab tab = new Tab() { Title = displayName };
                    List list = new List();
                    list.AddListItem("Class schedule not found for " + argument.SelectedTerm, null, null);
                    tab.Content.Add(list);
                    tabContainer.Tabs.Add(tab);
                }
            }

            if (tokenData.PageType == "large")
                response.Content.Add(tabContainer);

            if (response.Content.Count == 0)
            {
                List notFoundList = new List();
                notFoundList.AddListItem("Class schedules found", null, null);
                response.Content.Add(notFoundList);
            }

            if (terms.Count > 0)
            {
                terms = terms.Distinct().ToList();

                terms = Person.Utilities.TermSorter.Sort(terms.ToArray()).ToList();

                terms.Reverse();

                XModComm.Forms.FormInput.Select termSelect = new XModComm.Forms.FormInput.Select();
                termSelect.Name = "term";
                termSelect.Label = "";
                termSelect.Value = selectedTerm;
                Dictionary<string, string> optionList = new Dictionary<string, string> { };
                List<string> termDescList = terms.Select(x => GetTermDesc(x)).ToList();
                int i = 0;
                foreach (string termVal in terms)
                {
                    optionList.Add(termVal, termDescList.ElementAt(i));
                    i++;
                }

                termSelect.Options = optionList;
                termSelect.Width = "third";
                //termSelect.OptionDescriptions = terms;
                //termSelect.EmptyValue = terms;

                Form form = new Form();
                form.Items.Add(termSelect);  // TEST -> FormItem.FormInputType.Search
                ButtonContainer buttonContainer = new ButtonContainer();
                Button button = new Button()
                {
                    Title = "Change Term",
                    Name = "Submit",
                    Value = "1"
                };
                buttonContainer.Buttons.Add(button);
                form.Items.Add(buttonContainer);
                response.ContentContainerWidth = "full";
                response.Content.Insert(0, form);
            }

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public XModComm.Tables.Table GenerateWeeklyTable(ClassSchedule classSchedule)
        {
            Table table = new XModComm.Tables.Table(0, 0) { Heading = "Weekly Schedule" };

            int times = 30;
            bool sundayClass = false;
            int latestTime = 17; // only show to 4:40 by default
            DateTime[] timeColumn = new DateTime[times];
            string[,] scheduleMatrix = new string[7, times];

            DateTime dateNow = DateTime.Now;
            DateTime startTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 8, 0, 0);

            // Initialize through time column 
            for (int timeOffset = 0; timeOffset < 24; timeOffset++)
            {
                timeColumn[timeOffset] = startTime;

                startTime = startTime.AddMinutes(30);
                // Afternoon offset
                if (startTime.Hour == 11 && startTime.Minute == 30)
                    startTime = startTime.AddMinutes(10);

            }

            var dayOfWeek = new Dictionary<string, int>() { { "S", 0 }, { "M", 1 }, { "T", 2 }, { "W", 3 }, { "TH", 4 }, { "F", 5 } };

            // Loop through sections
            foreach (ClassSchedule.Section section in classSchedule.Sections)
            {
                // Loop through meeting times
                foreach (ClassSchedule.MeetingTime meetingTime in section.MeetingTimes)
                {
                    DateTime classStartTime = DateTime.Parse(meetingTime.StartTime);
                    DateTime classEndTime = DateTime.Parse(meetingTime.EndTime);

                    // Loop through sections of day
                    for (int i = 0; i < times; i++)
                    {
                        if (classStartTime <= timeColumn[i] && timeColumn[i] < classEndTime)
                        {
                            foreach (string day in meetingTime.Days)
                            {
                                scheduleMatrix[dayOfWeek[day], i] = section.Name;
                                if (day == "S")
                                    sundayClass = true;
                                if (latestTime < i)
                                    latestTime = i;
                            }
                        }
                    }
                }
            }

            int startDay = 1;
            if (sundayClass)
            {
                new string[] { "", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" }.ToList().ForEach(
                    (string header) => { table.ColumnOptions.Add(new ColumnOption() { Header = header }); });
                startDay = 0;
            }
            else
                new string[] { "", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" }.ToList().ForEach(
    (string header) => { table.ColumnOptions.Add(new ColumnOption() { Header = header }); });

            for (int time = 0; time <= latestTime; time++)
            {
                Row row = new Row();
                row.Cells.Add(new Cell() { Title = timeColumn[time].ToShortTimeString() });

                for (int day = startDay; day < 6; day++)

                    row.Cells.Add(new Cell() { Title = scheduleMatrix[day, time] });

                table.Rows.Add(row);
            }
            return table;
        }

        public static string GetTermDesc(string term)
        {
            if (term.Length != 6)
                return term;

            string year = term.Substring(0, 4);
            string semester = term.Substring(4, 2);

            switch (semester)
            {
                case "SP":
                    return "Spring " + year;
                case "SU":
                    return "Summer " + year;
                case "FA":
                    return "Fall " + year;
                case "WI":
                    return "Winter " + year;
            }

            return term;
        }

    }
}