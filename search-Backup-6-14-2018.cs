using System;
using System.Collections.Generic;
using XModComm;
using System.Text.RegularExpressions;
using System.Text;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(System.Collections.Generic.List<object> objectList, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            List list = new List() { Grouped = true };
			list.Heading = "Search Results";
			list.Heading = list.Heading.ToUpper();
            Dictionary<string, string> queryParameters = null;

            foreach(object obj in objectList)
            {
				Thumbnail thumbnail = new Thumbnail();
				thumbnail.MaxHeight = 80;
				thumbnail.MaxWidth = 60;
						
                switch(obj.ToString())
                {
                    case "Person.Portal.UserProfile":
                        string title = null;
                        string label = null;
                        Person.Portal.UserProfile userProfile = obj as Person.Portal.UserProfile;

                        if (userProfile == null)
                            continue;

                        if (userProfile.PrimaryRole.ToString() == "Staff" || userProfile.PrimaryRole.ToString() == "Faculty")
                        {
                            label = "Employee";

                            if (userProfile.Titles.Count > 0)
                            {
                                for (int i = 0; i < userProfile.Titles.Count; i++)
                                {
                                    title += Regex.Replace(userProfile.Titles[i], @"\\", "");

                                    if ((i + 1) < userProfile.Titles.Count)
                                        title += "; ";
                                }
                            }
                        }
                        else
                        {
                            label = "Student";
                            title = GetClassification(userProfile.Classification);
                        }

                        thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
                        thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;
						
						if (thumbnail.Url != String.Empty)
							list.AddListItem(userProfile.DisplayName, label, title, new XModuleLink("userdetails", "/" + userProfile.IDToken), thumbnail);
						else
							list.AddListItem(userProfile.DisplayName, label, title, new XModuleLink("userdetails", "/" + userProfile.IDToken));
                        
                        break;

                    case "Person.CampusInformation.Department":
                        Person.CampusInformation.Department department = obj as Person.CampusInformation.Department;
						
                        thumbnail.Url = "https://search.oc.edu/icons/department_myocsearch.png";
                        thumbnail.Alt = "Department";

                        if (department.Name != null)
                            list.AddListItem(department.Name, "Department", null, new XModuleLink("departmentdetails", "/" + department.ID), thumbnail);
                        break;

                    case "Person.Portal.Service":
                        Person.Portal.Service service = obj as Person.Portal.Service;
						
                        thumbnail.Url = "https://search.oc.edu/icons/service_myocsearch.png";
                        thumbnail.Alt = "Service";

                        if (service.Title != null)
                            list.AddListItem(service.Title, "Service", null, new ExternalLink(service.RedirectURI.AbsoluteUri), thumbnail);
                        break;

                    case "Person.ZenDesk.Article":
                        Person.ZenDesk.Article zendeskArticle = obj as Person.ZenDesk.Article;
						
                        thumbnail.Url = "https://search.oc.edu/icons/article_myocsearch.png";
                        thumbnail.Alt = "Article";

                        if (zendeskArticle.Title != null)
                            list.AddListItem(zendeskArticle.Title, "Article", zendeskArticle.Section.Name + " / " + zendeskArticle.Section.Category.Name, new ExternalLink(zendeskArticle.HTML_URL), thumbnail);
                        break;

                    case "Person.CampusInformation.Building":
                        Person.CampusInformation.Building building = obj as Person.CampusInformation.Building;
						
                        thumbnail.Url = "https://search.oc.edu/icons/building_myocsearch.png";
                        thumbnail.Alt = "Building";

                        if (building.Title != null && building.kmlGeometry != null)
                        {
							queryParameters = new Dictionary<string, string>(2);
                            queryParameters.Add("filter", building.ShortDescription);
                            queryParameters.Add("_recenter", "true");
                            list.AddListItem(building.Title, "Location", building.ShortDescription, new ModuleLink("map", "index", queryParameters), thumbnail);
                        }
            
                        break;

                    case "Person.CampusInformation.Event":
                        StringBuilder description = new StringBuilder(256);
                        Person.CampusInformation.Event campusEvent = obj as Person.CampusInformation.Event;

                        thumbnail.Url = "https://search.oc.edu/icons/event_myocsearch.png";
                        thumbnail.Alt = "Event";
						
                        if (campusEvent.EventName != null)
                        {
							if (campusEvent.GroupName != null && campusEvent.GroupName != "")
								description.Append("Group: ").Append(campusEvent.GroupName).Append("<br />");
							
							if (campusEvent.ContactName != null && campusEvent.ContactName != "")
								description.Append("Contact: ").Append(campusEvent.ContactName).Append("<br />");
							
							if (campusEvent.EventType != null && campusEvent.EventType != "")
								description.Append("Event Type: ").Append(campusEvent.EventType).Append("<br />");
                            
							if (campusEvent.EventStart != null)
								description.Append("Time: ").Append(campusEvent.EventStart.ToString("dddd MMM d, h:mm tt")).Append(" - ").Append(campusEvent.EventEnd.ToString("hh:mm tt")).Append("<br />");
                            
							if (campusEvent.Building != null && campusEvent.Building != "")
								description.Append("Building: ").Append(campusEvent.Building);
							
                            list.AddListItem(campusEvent.EventName, "Event", description.ToString(), thumbnail);
                        }
                        
                        break;

                    case "Person.Portal.RSSFeed":
                        Person.Portal.RSSFeed rssFeed = obj as Person.Portal.RSSFeed;
                        string feedName = null;
						
						thumbnail.Url = "https://search.oc.edu/icons/rss_myocsearch.png";
                        thumbnail.Alt = "RSS";

                        if (rssFeed.ListName.Contains("Student"))
                            feedName = "student_news";
                        else if (rssFeed.ListName.Contains("Campus"))
                            feedName = "faculty_news";

                        if (feedName != null)
                        {
							queryParameters = new Dictionary<string, string>(2);
                            queryParameters.Add("feed", feedName);
                            queryParameters.Add("id", rssFeed.URL);
                            list.AddListItem(rssFeed.Title, rssFeed.ListName, rssFeed.Body.Truncate(150), new ModuleLink("news", "index", queryParameters), thumbnail);
                        }
                        else
                            list.AddListItem(rssFeed.Title, rssFeed.ListName, rssFeed.Body, thumbnail);

                        break;
                }
            }

            detail.Content.Add(list);
            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public string GetClassification(string studentClassification)
        {
            switch (studentClassification)
            {
                case "FR":
                    return "Freshman";
                case "SO":
                    return "Sophomore";
                case "JR":
                    return "Junior";
                case "SR":
                    return "Senior";
                case "GR":
                    return "Graduate Student";
                default:
                    return "not found";
            }
        }
    }
	
	public static class StringLength
	{
		public static string Truncate(this string value, int maxChars)
		{
			return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
		}
	}
}
