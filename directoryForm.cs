using System;
using Person;
using XModComm;
using System.Text;
using XModComm.UI;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(TokenData tokenData)
        {
            XModComm.Response response = new Response();
		response.Content.Add(new XModComm.UI.Divider
            {
                BorderStyle = "none"
            });
			Detail detail = new Detail();
			detail.Title = "Directory";
            Form form = new Form();
			
			XModComm.Forms.FormInput.Text textBox = new XModComm.Forms.FormInput.Text();
			textBox.Name = "SearchString";
			textBox.Label = "Directory Search";
			textBox.Value = "";
			textBox.Required = true;
			form.Items.Add(textBox);
			
            ButtonContainer buttonContainer = new ButtonContainer();
            Button button = new Button()
            {
                Title = "Search",
                Name = "Submit",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);
            form.Items.Add(buttonContainer);
			detail.Content.Add(form);
            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
