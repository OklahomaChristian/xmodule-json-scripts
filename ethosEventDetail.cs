using System;
using Person;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;
using System.Text;

namespace Dynamic {
    public class Script {
        public string GetJSON(Ethos.Event ethosEvent, Ethos.Location eventLocation, TokenData tokenData = null) {
            //Ethos.Location eventLocation = new Ethos.Location(ethosEvent.LocationID);
            XModComm.Response response = new Response();


            // EVENT NOT FOUND
            if (ethosEvent == null) {
                Detail detail = new Detail();
                detail.Title = "Event not found";
                response.Content.Add(detail);
                return Person.Utilities.JSON.Serializer.Serialize(response);
            }

            response.ContentContainerWidth = "full";

            int roomResult;
            bool eventRoomIsInt = int.TryParse(ethosEvent.Room, out roomResult);
            string eventRoom = ethosEvent.Room;
            if (eventRoomIsInt)
                eventRoom = eventRoom.Insert(0, "Room ");

            string ChapelEventHtmlContent = "";
            StringBuilder ChapelEventHtmlContentBuilder = new StringBuilder(ChapelEventHtmlContent);
            ChapelEventHtmlContentBuilder.Append("<dl><dt>Event title</dt><dd>" + ethosEvent.EventName);
            if (!String.IsNullOrEmpty(ethosEvent.ShortDescription))
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Description</dt><dd>" + ethosEvent.ShortDescription);
            if (!String.IsNullOrEmpty(ethosEvent.Speaker))
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Speaker</dt><dd>" + ethosEvent.Speaker);
            if (!String.IsNullOrEmpty(ethosEvent.SongLeader))
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Song Leader</dt><dd>" + ethosEvent.SongLeader);
            if (ethosEvent.EventStart != null && ethosEvent.EventEnd != null)
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Time</dt><dd>" + String.Format
                            (
                                "{0}, {1}-{2}",
                                ethosEvent.EventStart.ToString("MMMM dd"),
                                ethosEvent.EventStart.ToString("h:mm"),
                                ethosEvent.EventEnd.ToString("h:mm tt")
                            ));
            if (!String.IsNullOrEmpty(eventLocation.Title))
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Location</dt><dd>" + eventLocation.Title + ", " + eventRoom);
            if (!String.IsNullOrEmpty(ethosEvent.ContactName)) {
                ChapelEventHtmlContentBuilder.Append("</dd><dt>Organizer</dt><dd>" + ethosEvent.ContactName);
                if (!String.IsNullOrEmpty(ethosEvent.ContactEmail))
                    ChapelEventHtmlContentBuilder.Append("</dd><dt>Organizer Contact</dt><a href='mailto:" + ethosEvent.ContactEmail + "'><dd>" + ethosEvent.ContactEmail + "</a>");
            }
            ChapelEventHtmlContentBuilder.Append("</dd></dl>");
            ChapelEventHtmlContent = ChapelEventHtmlContentBuilder.ToString();

            //TODO Add the other info

            XModComm.UI.Column.ThreeColumn threeColumn = new XModComm.UI.Column.ThreeColumn {
                MarginTop = "responsive",
                SecondaryColumn1 = new XModComm.UI.Column.ColumnContents {
                    Content = new System.Collections.Generic.List<object>
                    {
                        new BlockHeading
                        {
                            Heading = "Chapel Event Details",
                            MarginTop = "tight",
                            MarginBottom = "none"
                        }
                    }
                },
                PrimaryColumn = new XModComm.UI.Column.ColumnContents {
                    Content = new System.Collections.Generic.List<object>
                    {
                        new HtmlFragment
                        {
                            Html = ChapelEventHtmlContent
                        }
                    }
                }
            };

            //List eventInfo = new List();
            //eventInfo.Heading = "Event Information";
            //eventInfo.Heading = eventInfo.Heading.ToUpper();

            //// PRIVATE EVENT
            //if (ethosEvent.PrivateEvent)
            //{
            //	eventInfo.AddListItem("Private Event", null, null);
            //	detail.Content.Add(eventInfo);
            //	response.Content.Add(detail);
            //	return Person.Utilities.JSON.Serializer.Serialize(response);
            //}

            //// PUBLIC EVENT
            //if (!String.IsNullOrEmpty(ethosEvent.Description))
            //	eventInfo.AddListItem(ethosEvent.Description, "What?", null);
            //eventInfo.AddListItem(ethosEvent.Category, "Ethos Category", null);
            //if (eventLocation.ID > 9000) // Off-campus event
            //{
            //	eventInfo.AddListItem
            //	(
            //		eventLocation.Title,
            //		"Where?",
            //		null,
            //		eventLocation.Address != null ? new ExternalLink("https://maps.apple.com/maps?q=" + Uri.EscapeDataString(eventLocation.Address)) : null
            //	);
            //}
            //else // On-campus event
            //{
            //	int roomResult;
            //	bool eventRoomIsInt = int.TryParse(ethosEvent.Room, out roomResult);
            //	string eventRoom = ethosEvent.Room;
            //	if (eventRoomIsInt)
            //		eventRoom = eventRoom.Insert(0, "Room ");

            //	eventInfo.AddListItem(eventLocation.Title + ", " + eventRoom, "Where?", null);
            //}
            //eventInfo.AddListItem(ethosEvent.SelfCheckin.ToString(), "Self-checkin available?", null);

            //detail.Content.Add(eventInfo);

            //List contactInfo = new List();
            //contactInfo.Heading = "Contact Information";
            //contactInfo.Heading = contactInfo.Heading.ToUpper();
            //contactInfo.AddListItem(ethosEvent.ContactName, null, null);
            //contactInfo.AddListItem(ethosEvent.ContactEmail, null, null, new ExternalLink("mailto:" + ethosEvent.ContactEmail));
            //contactInfo.AddListItem(ethosEvent.ContactPhone, null, null, new ExternalLink("tel:" + ethosEvent.ContactPhone));

            //detail.Content.Add(contactInfo);

            //response.Content.Add(detail);
            response.Content.Add(threeColumn);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}