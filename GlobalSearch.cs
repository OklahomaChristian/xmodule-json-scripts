using System;
using System.Collections.Generic;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using SkunkWorks;

namespace Dynamic
{
	public class Script
	{
		public bool TestJSON(System.Collections.Generic.List<object> objectList, TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(objectList, tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}

		public string GetJSON(System.Collections.Generic.List<object> objectList, TokenData tokenData)
		{
			List userProfileList = new List();
			List departmentList = new List();
			List serviceList = new List();
			List articleList = new List();
			List buildingList = new List();
			List rssList = new List();
			Item item = null;
			Dictionary<string, string> queryParameters = null;

			if (objectList != null && objectList.Count > 0)
			{
				foreach (object obj in objectList)
				{
					string objType = obj.ToString();
					switch (objType)
					{
						case "Person.Portal.UserProfile":
							Person.Portal.UserProfile userProfile = obj as Person.Portal.UserProfile;

							if (userProfile == null)
								continue;

							item = new Item();
							item.Title = userProfile.DisplayName;
							item.Link = new XModuleLink("userdetails", "/" + userProfile.IDToken);

							Thumbnail thumbnail = new Thumbnail();
							thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
							thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;
							//thumbnail.MaxHeight = 80;
							//thumbnail.MaxWidth = 80;
							item.Thumbnail = thumbnail;

							userProfileList.Items.Add(item);

							break;

						case "Person.CampusInformation.Department":
							Person.CampusInformation.Department department = obj as Person.CampusInformation.Department;

							if (department == null)
								continue;

							item = new Item();
							item.Title = department.Name;
							item.Link = new XModuleLink("departments", "/" + department.ID);
							departmentList.Items.Add(item);

							break;

						case "Person.Portal.Service":
							Person.Portal.Service service = obj as Person.Portal.Service;

							if (service == null || !service.Visible || !service.Enabled)
								continue;

							item = new Item();
							item.Title = service.Title;
							item.Link = new ExternalLink(service.RedirectURI.AbsoluteUri);
							serviceList.Items.Add(item);

							break;

						case "SkunkWorks.Service":
							SkunkWorks.Service searchService = obj as SkunkWorks.Service;

							if (searchService == null)
								continue;

							item = new Item();
							item.Title = searchService.Title;
							item.Link = new ExternalLink(searchService.RedirectURI.AbsoluteUri);
							serviceList.Items.Add(item);

							break;

						case "Person.ZenDesk.Article":
							Person.ZenDesk.Article zendeskArticle = obj as Person.ZenDesk.Article;

							if (zendeskArticle == null)
								continue;

							item = new Item();
							item.Title = zendeskArticle.Title;
							item.Link = new ExternalLink(zendeskArticle.HTML_URL);
							articleList.Items.Add(item);

							break;

						case "Person.CampusInformation.Building":
							Person.CampusInformation.Building building = obj as Person.CampusInformation.Building;

							if (building == null)
								continue;

							queryParameters = new Dictionary<string, string>(2);
							queryParameters.Add("filter", building.Title);
							queryParameters.Add("_recenter", "true");

							item = new Item();
							item.Title = building.Title;
							item.Link = new ModuleLink("map", "index", queryParameters);
							buildingList.Items.Add(item);

							break;

						case "Person.Portal.RSSFeed":
							Person.Portal.RSSFeed rssFeed = obj as Person.Portal.RSSFeed;
							string feedName = null;

							if (rssFeed == null)
								continue;

							if (rssFeed.ListName.Contains("Student"))
								feedName = "student_news";
							else if (rssFeed.ListName.Contains("Campus"))
								feedName = "employee_news";

							queryParameters = new Dictionary<string, string>(2);
							queryParameters.Add("feed", feedName);
							queryParameters.Add("id", rssFeed.URL);

							item = new Item();
							item.Title = rssFeed.Title;
							item.Link = new ModuleLink(feedName, "index", queryParameters);
							rssList.Items.Add(item);

							break;
					}
				}
			}

			item = new Item();
			item.Title = "No results found";


			if (userProfileList.Items.Count == 0)
				userProfileList.Items.Add(item);
			if (departmentList.Items.Count == 0)
				departmentList.Items.Add(item);
			if (serviceList.Items.Count == 0)
				serviceList.Items.Add(item);
			if (articleList.Items.Count == 0)
				articleList.Items.Add(item);
			if (buildingList.Items.Count == 0)
				buildingList.Items.Add(item);
			if (rssList.Items.Count == 0)
				rssList.Items.Add(item);


			Dictionary<string, string> jsonDictionary = new Dictionary<string, string>();

			if (userProfileList.Items.Count > 0)
			{
				XModComm.Response userProfileResponse = new Response();
				userProfileResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				userProfileResponse.Content.Add(userProfileList);
				jsonDictionary.Add("userProfile", Person.Utilities.JSON.Serializer.Serialize(userProfileResponse));
			}

			if (departmentList.Items.Count > 0)
			{
				XModComm.Response departmentResponse = new Response();
				departmentResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				departmentResponse.Content.Add(departmentList);
				jsonDictionary.Add("department", Person.Utilities.JSON.Serializer.Serialize(departmentResponse));
			}

			if (serviceList.Items.Count > 0)
			{
				XModComm.Response serviceResponse = new Response();
				serviceResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				serviceResponse.Content.Add(serviceList);
				jsonDictionary.Add("service", Person.Utilities.JSON.Serializer.Serialize(serviceResponse));
			}

			if (articleList.Items.Count > 0)
			{
				XModComm.Response articleResponse = new Response();
				articleResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				articleResponse.Content.Add(articleList);
				jsonDictionary.Add("article", Person.Utilities.JSON.Serializer.Serialize(articleResponse));
			}

			if (buildingList.Items.Count > 0)
			{
				XModComm.Response buildingResponse = new Response();
				buildingResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				buildingResponse.Content.Add(buildingList);
				jsonDictionary.Add("building", Person.Utilities.JSON.Serializer.Serialize(buildingResponse));
			}

			if (rssList.Items.Count > 0)
			{
				XModComm.Response rssResponse = new Response();
				rssResponse.Metadata.SearchOptions = new SearchOptions { TruncateResults = false };
				rssResponse.Content.Add(rssList);
				jsonDictionary.Add("rss", Person.Utilities.JSON.Serializer.Serialize(rssResponse));
			}

			return Person.Utilities.JSON.Serializer.Serialize(jsonDictionary);
		}
	}
}
