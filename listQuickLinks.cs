using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.Portal.Service[] bookmarks, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(bookmarks, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.Portal.Service[] bookmarks, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            response.ContentContainerWidth = "narrow";
            response.Content.Add(new Divider());
            Detail detail = new Detail();
            detail.Title = "QuickLinks";
            List quickLinksList = new List();
            quickLinksList.ShowAccessoryIcons = false;
            quickLinksList.ListStyle = "grouped";
            quickLinksList.Heading = "";

            //	List editQuickLinksList = new List() { Grouped = true };
            //editQuickLinksList.Items.Add(
            //new Item()
            //{
            //    Title = "Edit QuickLinks",
            //    Link = new XModuleLink("quicklinks", "/edit")
            //}
            //);
            //	detail.Content.Add(editQuickLinksList);

            Array.Sort(bookmarks, (x, y) => x.Title.CompareTo(y.Title));

            //foreach quicklink that the user has
            //display the quicklink
            //list items with external links
            if (bookmarks.Length > 0)
            {
                foreach (Person.Portal.Service quickLink in bookmarks)
                {
                    Link link = null;
                    if (tokenData.PageType == "small" || (tokenData.PageType == "large" && (tokenData.Platform == "ios" || tokenData.Platform == "android")))
                        link = new XModuleLink("utilities", String.Format("/SSORedirect/{0}", quickLink.ID));
                    else
                        link = new ExternalLink(String.Format("{0}", quickLink.RedirectURI));

                    quickLinksList.AddListItem(quickLink.Title, null, null, link);
                }
            }
            else
            {
                quickLinksList.AddListItem("No results found", null, null);
            }
            quickLinksList.Items.Add(
                    new Item()
                    {
                        Title = "Edit QuickLinks",
                        Link = new XModuleLink("quicklinks", "/edit")
                    }
                );

            detail.Content.Add(quickLinksList);
            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}