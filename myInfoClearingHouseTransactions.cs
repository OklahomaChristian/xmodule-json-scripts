using System;
using XModComm;
using XModComm.UI;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(string html, TokenData tokenData)
        {
            XModComm.Response response = new Response();

            HtmlFragment htmlFragment = new HtmlFragment();
            htmlFragment.Html = html;

            response.Content.Add(htmlFragment);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}