using System;
using System.Linq;
using System.Text;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;
using System.Collections.Generic;

namespace Dynamic
{
	public class Script
	{
		public string GetJSON(Person.Colleague.Schedule.FacultySections facultySections, TokenData tokenData)
		{
			var section = (facultySections.Sections[0] != null) ? facultySections.Sections[0] : null;
			XModComm.Response response = new Response();

			Detail detail = new Detail();
			detail.Title = String.Format("{0} ({1})", section.ShortTitle, section.Name);
			HtmlFragment header = new HtmlFragment();
			List meetingTimesList = new List();
			meetingTimesList.Heading = "Meeting Times";

			if (section.MeetingTimes != null && section.MeetingTimes != null)
			{
				header.Html += "<table style='margin-bottom:0px;'>";

				foreach (var meetingTime in section.MeetingTimes)
				{
					// Meeting Times
					string times = "";
					string dates = "";
					if (meetingTime.Days != null)
						meetingTime.Days.ToList().ForEach((string day) => { times = String.Format("{0} {1}", times, day); });
					times += String.Format(" {0}-{1} ({2} / {3})", meetingTime.StartTime, meetingTime.EndTime, meetingTime.Building.Code ?? "-", meetingTime.Room ?? "-");
					dates += String.Format("{0}-{1}", meetingTime.StartDate, meetingTime.EndDate);

					if (times.Length > 0)
						meetingTimesList.AddListItem(times.TrimStart(' '), "Times/Dates", dates.TrimStart(' '));

					// Final Times
					if (meetingTime.FinalsTimes != null)
					{
						foreach (var finalTime in meetingTime.FinalsTimes)
						{
							string finalTimes = "";
							if (finalTime.Days != null)
								finalTime.Days.ToList().ForEach((string day) => { finalTimes = String.Format("{0} {1}", finalTimes, day); });
							finalTimes += String.Format(" {0}-{1}", finalTime.StartTime, finalTime.EndTime);

							if (finalTimes.Length > 0)
								meetingTimesList.AddListItem(finalTimes.TrimStart(' '), "Final Exam Time", null);
						}
					}

					// Faculty Info
					if (meetingTime.Faculty != null)
					{
						foreach (Person.Portal.UserProfile faculty in meetingTime.Faculty)
						{
							if (!header.Html.Contains(faculty.PicIDURL))
							{
								header.Html += String.Format("<tr><td style='border:0px;width:100px;'><img src='{0}'></td>", XModComm.Utilities.GetUserPhotoURL(faculty.UserID, faculty.PicIDURL));
								header.Html += String.Format("<td style='border:0px;vertical-align:top;'><h2><a href='../userdetails/page?relativePath=%2F{0}'>{1}</a></h2></td>", faculty.IDToken, faculty.DisplayName);
							}
						}
					}
				}

				header.Html += "</table>";
			}

			// Course Roster Download
			List courseRosterList = new List();
			courseRosterList.Heading = "Course Roster";

			if (tokenData.PageType == "large")
			{
				string csvRosterLink = String.Format("https://facultytools.oc.edu/course/downloadcsv?sectionId={0}&term={1}", section.CourseSectionId, facultySections.Term);
				courseRosterList.AddListItem("Download Roster as Spreadsheet", null, null, new ExternalLink(csvRosterLink));

				string pdfRosterLink = String.Format("https://facultytools.oc.edu/course/downloadpdfroster?sectionId={0}&term={1}", section.CourseSectionId, facultySections.Term);
				courseRosterList.AddListItem("Download Photo Roster as PDF", null, null, new ExternalLink(pdfRosterLink));
			}

			// Add Header
			detail.Content.Add(header);
			if (courseRosterList.Items.Count > 0)
				detail.Content.Add(courseRosterList);
			if (meetingTimesList.Items.Count > 0)
				detail.Content.Add(meetingTimesList);

			response.Content.Add(detail);

			// Tabs
			TabContainer tabContainer = new TabContainer()
			{
				ShowTitle = true,
				ForceAjaxOnLoad = true
			};

			// Enrolled Students Tab
			Tab enrolledTab = new Tab() { Title = "Enrolled" };
			enrolledTab.Content = null;
			enrolledTab.AjaxContent = new TabContent { AjaxRelativePath = String.Format("/LoadSectionRoster/{0}", section.CourseSectionId) };

			tabContainer.Tabs.Add(enrolledTab);

			// Withdrawn Students Tab
			Tab withdrawnTab = new Tab() { Title = "Withdrawn / Dropped" };
			withdrawnTab.Content = null;
			withdrawnTab.AjaxContent = new TabContent { AjaxRelativePath = String.Format("/LoadWithdrawnProfiles/{0}", section.CourseSectionId) };

			tabContainer.Tabs.Add(withdrawnTab);

			// Waitlisted Students Tab
			Tab waitlistedTab = new Tab() { Title = "Waitlisted" };
			waitlistedTab.Content = null;
			waitlistedTab.AjaxContent = new TabContent { AjaxRelativePath = String.Format("/LoadWaitListedProfiles/{0}", section.CourseSectionId) };

			tabContainer.Tabs.Add(waitlistedTab);

			response.Content.Add(tabContainer);

			return Person.Utilities.JSON.Serializer.Serialize(response);
		}
	}
}
