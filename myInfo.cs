using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Collections.Generic;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            Person.Shoretel.MailBox voicemail,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            TokenData tokenData,
            out Exception returnEx
        )
        {
            try
            {
                GetJSON(userProfile, printBalance, mealPlan, voicemail, advisors, vehicleRegistrations, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            Person.Shoretel.MailBox voicemail,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            TokenData tokenData
        )
        {
            XModComm.Response response = new Response();
	    response.ContentContainerWidth = "full";

            NameTag nameTag = new NameTag
            {
                NametagStyle = "horizontal",
                Name = userProfile.DisplayName,
                Description = "User ID: " + userProfile.UserID,
                NameFontSize = "1.25rem",
                NameFontWeight = "medium",
                ImageHeight = "7rem",
                ImageWidth = "7rem",
                ImageBorderRadius = "tight",
                Image = new Image
                {
                    Url = userProfile.PicIDURL + "/480",
                    Opacity = 100
                },
                MarginTop = "medium",
                MarginBottom = "none"
            };

            response.Content.Add(nameTag);

            string securityPin = "Pin Not Found";
            string passwordExpireDate = "Password Expiration Date Not Found";

            if (!String.IsNullOrEmpty(userProfile.IDPin))
                securityPin = userProfile.IDPin;
            if (!String.IsNullOrEmpty(userProfile.PasswordExpireDate.ToString()))
            {
                passwordExpireDate = userProfile.PasswordExpireDate.ToString();
            }
            XModComm.UI.Column.ThreeColumn securitySection = new XModComm.UI.Column.ThreeColumn
            {
                //MarginTop = "responsive",
                Gutters = true,
                SecondaryColumn1 = new XModComm.UI.Column.ColumnContents
                {
                    Content = new List<object>
                    {
                        new BlockHeading
                        {
                            Heading = "Security Information",
                            MarginTop = "tight",
                            MarginBottom = "none"
                        }
                    }
                },
                PrimaryColumn = new XModComm.UI.Column.ColumnContents
                {
                    Content = new List<object>
                    {
                        new HtmlFragment
                        {
                            Html = "<dl><dt>Card Pin</dt><dd>" + securityPin + "</dd><dt>Password Expiration Date</dt><dd>" + passwordExpireDate + "</dd></dl>"
                        }
                    }
                },
                SecondaryColumn2 = new XModComm.UI.Column.ColumnContents
                {
                    Content = new List<object>
                    {
                        new List
                        {
                            ListStyle = "unpadded",
                            TitleFontSize = "small",
                            TitleFontWeight = "normal",
                            MarginTop = "none",
                            ShowTopBorder = false,
                            ShowBottomBorder = false,
                            Items = new List<object>
                            {
                                new SimpleListItem
                                {
                                    Title = "Change my password",
                                    Url = new XModComm.StandardLinks.ExternalLink
                                    {
                                        External = "https://account.oc.edu/password"
                                    }
                                }
                            }
                        }
                    }
                }
            };
            response.Content.Add(securitySection);
            response.Content.Add(new Divider());

            Detail detail = new Detail();
            detail.Title = userProfile.DisplayName;
            detail.Description = "User Id: " + userProfile.UserID;

            detail.Thumbnail = new XModComm.Thumbnail();
            detail.Thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
            detail.Thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;
            detail.Thumbnail.Opacity = 100;

            // uniFlow
            if (userProfile.PrimaryRole == Person.UserType.Student)
            {
                if (!String.IsNullOrEmpty(printBalance))
                {
                    decimal balance = System.Convert.ToDecimal(printBalance);
                    List uniFlow = new List();
                    uniFlow.ListStyle = "grouped";
                    uniFlow.Heading = "Printing";
                    uniFlow.AddListItem("Print Balance", "", balance.ToString("C2", new CultureInfo("en-US")), new ExternalLink("http://printers.oc.edu/pwclient"));
                    response.Content.Add(uniFlow);
                }
            }

            // HR Information
            if (userProfile.isEmployee)
            {
                List hrInfo = new List();
                hrInfo.ListStyle = "grouped";
                hrInfo.Heading = "HR Info";

                if (userProfile.Titles != null)
                {
                    foreach (string title in userProfile.Titles)
                    {
                        string titleReplaced = Regex.Replace(title, @"\\", "");
                        hrInfo.AddListItem(titleReplaced, "Job Title", "");
                    }
                }

                hrInfo.AddListItem(userProfile.OfficeBuilding + " (" + userProfile.OfficeBuildingCode + ") " + userProfile.OfficeRoomNumber, "On Campus", userProfile.Department);

                string homeAddress = null;
                if (userProfile.Addresses != null)
                {
                    foreach (Person.Portal.Address address in userProfile.Addresses)
                    {
                        if (address.Type == Person.Portal.AddressType.Home &&
                            address.AddressLines != null &&
                            address.City != "" &&
                            address.State != "" &&
                            address.Zip != "")
                        {
                            homeAddress = address.AddressLines.Lines[0] + address.City + ", " + address.State + " " + address.Zip;
                            break;
                        }
                    }
                    hrInfo.AddListItem("Home Address", "Off Campus", homeAddress);
                }

                if (userProfile.PhoneNumbers != null)
                {
                    foreach (Person.Portal.PhoneNumber phone in userProfile.PhoneNumbers)
                    {
                        hrInfo.AddListItem(phone.NumberType.ToString(), "", phone.Number);
                    }
                }

                hrInfo.AddListItem("Update your information with HR", "", "", new ExternalLink("https://services.oc.edu/158"));

                response.Content.Add(hrInfo);
            }

            // Housing and Mailroom
            List housingMailroom = new List();
            housingMailroom.ListStyle = "grouped";
            housingMailroom.Heading = "Housing and Mailroom Info";

            if (!String.IsNullOrEmpty(userProfile.HousingBuilding))
                housingMailroom.AddListItem("Housing Building", "", userProfile.HousingBuilding);
            if (!String.IsNullOrEmpty(userProfile.HousingRoomNumber))
                housingMailroom.AddListItem("Room Number", "", userProfile.HousingRoomNumber);
            if (!String.IsNullOrEmpty(userProfile.MailBox))
                housingMailroom.AddListItem("Mailbox", "", userProfile.MailBox, new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox"));
            if (!String.IsNullOrEmpty(userProfile.MailBoxCombo))
                housingMailroom.AddListItem("Mailbox Combo", "", userProfile.MailBoxCombo, new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox"));

            if (housingMailroom.Items.Count > 0)
                response.Content.Add(housingMailroom);

            // Meal Info
            List mealList = new List();
            mealList.ListStyle = "grouped";
            mealList.Heading = "MEAL INFORMATION";

            if (!String.IsNullOrEmpty(mealPlan.MealPlanName))
                mealList.AddListItem("Meal Plan", "Plan Information", mealPlan.MealPlanName); // TODO: Add URL to dining
            if (!String.IsNullOrEmpty(mealPlan.MealBoard) && mealPlan.MealBoard != "0.00")
            {
                // Check homePage as well
                string mealsRemaining = mealPlan.MealBoard;
                if (mealPlan.MealPlanName == "All Access 7")
                    mealsRemaining = "All-Access 7 days";
                if (mealPlan.MealPlanName == "All Access 5")
                    mealsRemaining = "All-Access Weekdays";
                if (userProfile.isEmployee)
                    mealList.AddListItem("Meals Remaining", "", mealsRemaining);
                else
                    mealList.AddListItem("Meals Remaining", "", mealsRemaining, new ExternalLink("https://services.oc.edu/redirect/520"));
            }
            if (!String.IsNullOrEmpty(mealPlan.MealPoints))
                mealList.AddListItem("Balance Remaining", "Eagle Bucks", "$" + mealPlan.MealPoints);

            if (userProfile.isStudent)
                mealList.AddListItem("Purchase Eagle Bucks For Yourself", "", "", new ExternalLink("https://services.oc.edu/redirect/521"));

            if (mealList.Items.Count > 0)
                detail.Content.Add(mealList);

            // Phone Information
            if (userProfile.Extensions != null && userProfile.Extensions.Count > 0)
            {
                List phoneList = new List();
                phoneList.ListStyle = "grouped";
                phoneList.Heading = "PHONE INFORMATION";

                if (!String.IsNullOrEmpty(userProfile.Extensions.ToString()))
                    phoneList.AddListItem("Extension", "", userProfile.Extensions[0].Number);// TODO: Loop through all numbers
                foreach (Person.Portal.LongDistancePin pin in userProfile.LongDistancePin)
                {
                    phoneList.AddListItem("Long Distance Pin", pin.Description, pin.Pin);
                }
                //phoneList.AddListItem("Voicemails", "", voicemail.NumberOfMessages.ToString(), new ExternalLink("https://shoretel.oc.edu"));
                if (voicemail != null)
                    phoneList.AddListItem("Voicemail Password", "", voicemail.Password); //TODO: add link and code to change password

                detail.Content.Add(phoneList);
            }

            // mealList.AddListItem("Transactions", null, null, new XModuleLink("my_info", "/Transactions"));

            // Advisors
            List advisorList = new List();
            advisorList.ListStyle = "grouped";
            advisorList.Heading = "ADVISORS";

            if (advisors != null && advisors.Count > 0)
            {
                foreach (KeyValuePair<string, Person.Portal.UserProfile> kvp in advisors)
                {
                    Person.Portal.UserProfile advisorProfile = kvp.Value;

                    Thumbnail thumbnail = new Thumbnail();
                    thumbnail.Url = advisorProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(advisorProfile.UserID, advisorProfile.PicIDURL) : String.Empty;
                    thumbnail.Alt = advisorProfile.DisplayName != null ? advisorProfile.DisplayName : String.Empty;
                    //thumbnail.MaxHeight = 80;
                    //thumbnail.MaxWidth = 80;

                    string title = "";
                    switch (kvp.Key)
                    {
                        case "PFC":
                            title = "Personal Financial Counselor";
                            break;
                        case "Academic":
                            title = "Academic Advisor";
                            break;
                        case "Resident Director":
                            title = "Resident Director";
                            break;
                        case "Mentor":
                            title = "Program Mentor";
                            break;
                    }

                    advisorList.Items.Add
                    (
                        new XModComm.Item()
                        {
                            Title = title,
                            Description = kvp.Value.DisplayName,
                            Link = new XModuleLink("userdetails", "/" + advisorProfile.IDToken),
                            Thumbnail = thumbnail
                        }
                    );
                }
                detail.Content.Add(advisorList);
            }

            // AUTO DATA
            if (vehicleRegistrations != null)
            {
                List vehicleList = new List();
                vehicleList.ListStyle = "grouped";
                vehicleList.Heading = "VEHICLE REGISTRATIONS";

                foreach (var registration in vehicleRegistrations)
                {
                    vehicleList.Items.Add(new XModComm.Item() { Title = "Permit ID", Description = registration.PermitId });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "License Plate", Description = registration.Plate });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "License Plate State", Description = registration.PlateState });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "Vehicle Year", Description = registration.VehicleYear });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "Vehicle Color", Description = registration.VehicleColor });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "Vehicle Make", Description = registration.VehicleMake });
                    vehicleList.Items.Add(new XModComm.Item() { Title = "Vehicle Model", Description = registration.VehicleModel });
                }

                detail.Content.Add(vehicleList);
            }

            // CLIFTON_STRENGTHS DATA
            if (userProfile.CliftonStrengths != null)
            {
                List strengthList = new List
                {
                    ListStyle = "grouped",
                    Heading = "CLIFTON STRENGTHS"
                };

                List<string> strengthsArray = new List<string>();
                foreach (Person.Portal.CliftonStrength cliftonStrength in userProfile.CliftonStrengths)
                {
                    if (cliftonStrength.strengthName != null)
                        strengthsArray.Add(cliftonStrength.strengthName);
                }
                strengthList.AddListItem("", String.Join(" | ", strengthsArray), "Click to see description of strengths", new ExternalLink("https://services.oc.edu/783"));
                detail.Content.Add(strengthList);


            }

            //response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}