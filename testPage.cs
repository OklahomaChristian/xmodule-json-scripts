using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using Person;
using Person.Portal;
using Ethos;
using XModComm;
using XModComm.UI;
using XModComm.UI.MultiColumn;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Person.Portal.UserProfile userProfile, Ethos.UserProfile ethosUserProfile, Ethos.Event[] events, Person.Portal.Service[] quickLinks, TokenData tokenData)
        {
            XModComm.Response response = new Response();
			
			if (tokenData.PageType == "small")
            {
				switch (userProfile.PrimaryRole)
				{
					case Person.UserType.Student:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/studenthome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Staff:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/staffhome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Faculty:
					case Person.UserType.Adjunct:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/facultyhome", new Dictionary<string, string>());
						break;
					
					case Person.UserType.Proxy:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/proxyhome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Alumni:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/alumnihome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Retired:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/retiredhome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.UDining:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/udininghome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Accepted:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/acceptedhome", new Dictionary<string, string>());
						break;
						
					case Person.UserType.Guest:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/guesthome", new Dictionary<string, string>());
						break;
					
					case Person.UserType.SummerAcademy:
						response.Metadata.RedirectLink = new ModuleLink("mobile_home", "_/summeracademyhome", new Dictionary<string, string>());
						break;
				}

                return Person.Utilities.JSON.Serializer.Serialize(response);
            }
            else
            {
                MultiColumn multiColumn = new MultiColumn();
                System.Collections.Generic.List<object> column1 = new System.Collections.Generic.List<object>();
                System.Collections.Generic.List<object> column2 = new System.Collections.Generic.List<object>();
                System.Collections.Generic.List<object> column3 = new System.Collections.Generic.List<object>();

                // Column One
                FeedPortlet announcements = new FeedPortlet();
                announcements.NavbarTitle = "Announcements";

                if (userProfile.isEmployee)
                {
                    announcements.Feed = "employee_news";
                    announcements.Module = "employee_news";
                }
                else
                {
                    announcements.Feed = "student_news";
                    announcements.Module = "student_news";
                }

                announcements.Height = "small";

                FeedPortlet social = new FeedPortlet();
                social.NavbarTitle = "Social";
                social.Feed = "twitter";
                social.Module = "social";
                social.ContentType = "social";
                social.Height = "small";

                FeedPortlet photos = new FeedPortlet();
                photos.NavbarTitle = "Photos";
                photos.Feed = "instagram";
                photos.Module = "photos";
                photos.Height = "medium";

                FeedPortlet videos = new FeedPortlet();
                videos.NavbarTitle = "Videos";
                videos.Feed = "youtube_channel";
                videos.Module = "video";
                videos.Height = "medium";

                // Column Two
				Portlet oldMyOC = new Portlet();
				oldMyOC.NavbarTitle = "Previous MyOC";
				oldMyOC.NavbarIcon = "links";
				//oldMyOC.Height = "small";
				List oldMyOCList = new List();
				
				Link oldMyOCLink = new ExternalLink("http://my2.oc.edu");
				oldMyOCList.AddListItem("Previous MyOC Version", null, null, oldMyOCLink);
				oldMyOC.Content.Add(oldMyOCList);
				
                Portlet myInfo = new Portlet();
                myInfo.NavbarTitle = "My Info";
                myInfo.NavbarIcon = "info";
                myInfo.NavbarLink = new XModuleLink("my_info", "");
                List myInfoList = new List();

                if (userProfile != null && userProfile.PrimaryRole != Person.UserType.Proxy)
                {
                    myInfoList = MyInfoPortlet(userProfile);
                    myInfo.Content.Add(myInfoList);
                }

                Portlet ethos = new Portlet();
                ethos.NavbarTitle = "Ethos";
                ethos.NavbarIcon = "religious_services";

                if (userProfile != null && (userProfile.PrimaryRole == Person.UserType.Student || userProfile.PrimaryRole == Person.UserType.Proxy))
                    ethos.NavbarLink = new XModuleLink("ethos", "");
                else
                    ethos.NavbarLink = new XModuleLink("ethos", "/GetEthosEvents");

                List<object> ethosList = EthosPortlet(userProfile, ethosUserProfile, events);

                foreach (object ethosListObject in ethosList)
                {
                    ethos.Content.Add(ethosListObject); // returns List<object>
                }

                // Column Three
                Portlet quicklinks = new Portlet();
                quicklinks.NavbarTitle = "Quicklinks";
                quicklinks.NavbarIcon = "links";
                quicklinks.NavbarLink = new XModuleLink("quicklinks", "/edit");
                List quicklinksList = QuicklinksPortlet(quickLinks, userProfile, tokenData);
                if (quicklinksList.Items.Count > 0)
                    quicklinks.Content.Add(quicklinksList);

                Portlet proxylinks = new Portlet();
                proxylinks.NavbarTitle = "Links";
                proxylinks.NavbarIcon = "links";
                List<object> proxylinksList = new List<object>();

                if (userProfile != null && userProfile.PrimaryRole == Person.UserType.Proxy)
                {
                    proxylinksList = ProxylinksPortlet(userProfile);

                    foreach (object proxylistObject in proxylinksList)
                    {
                        proxylinks.Content.Add(proxylistObject);
                    }
                }

                Portlet proxyDependentAddresses = new Portlet();
                proxyDependentAddresses.NavbarTitle = "Student Addresses";
                proxyDependentAddresses.NavbarIcon = "links";
                List addressesLinksList = DependentAddressesPortlet(userProfile);

                if (userProfile != null && userProfile.PrimaryRole == Person.UserType.Proxy)
                    proxyDependentAddresses.Content.Add(addressesLinksList);

                if (tokenData.PageType == "large" && (tokenData.Platform == "ios" || tokenData.Platform == "android"))
                {
                    int announcementsLength = 4;
                    int ethosLength = 7;
                    int quicklinksLength = quicklinksList.Items.Count;
                    int myinfoLength = myInfoList.Items.Count;
                    int socialLength = 4;
                    int photosLength = 8;
                    int videosLength = 8;

                    int leftColumnLength = announcementsLength + myinfoLength + ethosLength;
                    int rightColumnLength = quicklinksLength;

					column1.Add(oldMyOC);
                    column1.Add(announcements);
                    if (myInfo.Content.Count > 0)
                        column1.Add(myInfo);
                    if (ethos.Content.Count > 0)
                        column1.Add(ethos);
                    if (proxylinks.Content.Count > 0)
                        column3.Add(proxylinks);
                    if (proxyDependentAddresses.Content.Count > 0)
                        column3.Add(proxyDependentAddresses);
                    if (quicklinks.Content.Count > 0)
                        column2.Add(quicklinks);

                    if (rightColumnLength < leftColumnLength)
                    {
                        column2.Add(social);
                        rightColumnLength += socialLength;
                    }
                    else
                    {
                        column1.Add(social);
                        leftColumnLength += socialLength;
                    }

                    if (rightColumnLength < leftColumnLength)
                    {
                        column2.Add(photos);
                        rightColumnLength += photosLength;
                    }
                    else
                    {
                        column1.Add(photos);
                        leftColumnLength += photosLength;
                    }

                    if (rightColumnLength < leftColumnLength)
                    {
                        column2.Add(videos);
                        rightColumnLength += videosLength;
                    }
                    else
                    {
                        column1.Add(videos);
                        leftColumnLength += videosLength;
                    }

                }
                else
                {
                    column1.Add(social);
                    column1.Add(photos);
                    column1.Add(videos);

					column1.Add(oldMyOC);
                    column2.Add(announcements);

                    if (myInfo.Content.Count > 0)
                        column2.Add(myInfo);
                    if (ethos.Content.Count > 0)
                        column2.Add(ethos);

                    if (proxylinks.Content.Count > 0)
                        column3.Add(proxylinks);
                    if (proxyDependentAddresses.Content.Count > 0)
                        column3.Add(proxyDependentAddresses);
                    if (quicklinks.Content.Count > 0)
                        column3.Add(quicklinks);

                }

                // Combine 
                if (column3.Count > 0)
                    multiColumn.Columns.Add(column3);
                if (column2.Count > 0)
                    multiColumn.Columns.Add(column2);
                if (column1.Count > 0)
                    multiColumn.Columns.Add(column1);

                response.Content.Add(multiColumn);

                return Person.Utilities.JSON.Serializer.Serialize(response);
            }
        }

        public static List QuicklinksPortlet(Person.Portal.Service[] quickLinks, Person.Portal.UserProfile userProfile, TokenData tokenData)
        {
            List quickLinksList = new List();
            quickLinksList.Grouped = false;

            Array.Sort(quickLinks, (x, y) => x.Title.CompareTo(y.Title));

			if (quickLinks.Length == 0)
			{
                if (userProfile != null && userProfile.PrimaryRole != Person.UserType.Proxy)
                    quickLinksList.AddListItem("No results found", null, null);
				return quickLinksList;
			}

			if (userProfile.PrimaryRole == Person.UserType.Student || userProfile.PrimaryRole == Person.UserType.Faculty || userProfile.PrimaryRole == Person.UserType.Staff)
			{
				Dictionary<string, ExternalLink> topServices = new Dictionary<string, ExternalLink>();
				topServices.Add("Blackboard", new ExternalLink("https://bb.oc.edu"));
				topServices.Add("OC Email", new ExternalLink("http://gmail.oc.edu"));
				topServices.Add("Services", new ExternalLink("https://mynew.oc.edu/services"));
				foreach (var topSerivce in topServices)
				{
					quickLinksList.AddListItem(topSerivce.Key, null, null, topSerivce.Value);
				}
			}
			
			foreach (Person.Portal.Service quickLink in quickLinks)
			{
				Link link = null;
				if (tokenData.PageType == "small" || (tokenData.PageType == "large" && (tokenData.Platform == "ios" || tokenData.Platform == "android")))
				{
					link = new XModuleLink("utilities", String.Format("/SSORedirect/{0}", quickLink.ID));
				}
				else
					link = new ExternalLink(String.Format("{0}", quickLink.RedirectURI));
				
				quickLinksList.AddListItem(quickLink.Title, null, null, link);
			}

            return quickLinksList;
        }

        public static List<object> ProxylinksPortlet(Person.Portal.UserProfile userProfile)
        {
            List<object> proxylinksReturn = new List<object>();

            List studentAccountList = new List();
            studentAccountList.Heading = "Student Account";
            List finAidList = new List();
            finAidList.Heading = "Financial Aid";
            
            List list = new List();

            if (userProfile != null && userProfile.PrimaryRole == Person.UserType.Proxy)
            {
                int studentAccountDependentCount = 0;
                int finAidDependentCount = 0;
                Dictionary<string, Proxy.ProxyProfile> dependents = Proxy.ProxyProfile.GetDependents(userProfile.UserID);

                if (dependents.Count > 0)
                {
                    foreach (Proxy.ProxyProfile dependent in dependents.Values)
                    {
                        foreach (Proxy.ProxyPermissions proxyPermissions in dependent.ProxyPermissions)
                        {
                            if (proxyPermissions == Proxy.ProxyPermissions.FinancialAccount)
                            {
                                studentAccountDependentCount++;
								studentAccountList.AddListItem("Student Account for " + dependent.DependentUserProfile.DisplayName, null, null, new XModuleLink("utilities","/RedirectProxyToCashNet/" + dependent.DependentID));
                            }
                            else if (proxyPermissions == Proxy.ProxyPermissions.FinancialAid)
                            {
                                finAidDependentCount++;
								finAidList.AddListItem("Financial Aid for " + dependent.DependentUserProfile.DisplayName, null, null, new XModuleLink("utilities","/RedirectProxyToFinancialAid/" + dependent.DependentUserProfile.IDToken));
                            }
                        }
                    }

                    if (studentAccountDependentCount == 0)
                    {
                        studentAccountList.AddListItem("Access to CASHNet is not available for any of your students.", null, null);
                    }

                    if (finAidDependentCount == 0)
                    {
                        finAidList.AddListItem("Access to Financial Aid is not available for any of your students.", null, null);
                    }
                }
				else
					list.AddListItem("Access has not yet been granted by any of your students.", null, null);
            }

			if (list.Items.Count > 0)
				proxylinksReturn.Add(list);
			else
			{			
				proxylinksReturn.Add(studentAccountList);
				proxylinksReturn.Add(finAidList);
			}

            return proxylinksReturn;
        }

        public static List DependentAddressesPortlet(Person.Portal.UserProfile userProfile)
        {
            List addressLinksReturn = new List();

            List addressList = new List();
            addressList.Heading = "Student Addresses";

            addressList.AddListItem("TEST TITLE", "Label", "Description");

            Dictionary<string, Proxy.ProxyProfile> dependents = Proxy.ProxyProfile.GetDependents(userProfile.UserID);

            if (dependents.Count > 0)
            {
                foreach (Proxy.ProxyProfile dependent in dependents.Values)
                {
                    addressLinksReturn.AddListItem(dependent.DependentUserProfile.OCEmailAddress, "Email", null, new ExternalLink(String.Format("mailto:{0}", dependent.DependentUserProfile.OCEmailAddress)));
                    string mailingAddress = String.Format("{0} {1}, SB# {2}<br>Oklahoma Christian University<br>2501 E. Memorial Rd.<br>Edmond, OK 73013-5599", dependent.DependentUserProfile.PreferedFirstName, dependent.DependentUserProfile.LastName, dependent.DependentUserProfile.MailBox);
                    addressLinksReturn.AddListItem(mailingAddress, "Mailing Address", null);
                }
            }
//            addressLinksReturn.Add(addressList);
            return addressLinksReturn;
        }

		public static List<object> EthosPortlet(Person.Portal.UserProfile userProfile, Ethos.UserProfile ethosUserProfile, Ethos.Event[] events)
        {
            List<object> ethosReturn = new List<object>();

            if (ethosUserProfile != null && ethosUserProfile.PrimaryRole == Person.UserType.Student)
            {
                List infoList = new List();
                infoList.Heading = "Semester Overview";
                infoList.AddListItem("Kudos Earned: " + ethosUserProfile.CurrentSemesterCheckinCount, null, null, new XModuleLink("ethos", "/GetEthosHistory"));
                infoList.AddListItem("Kudos Required: " + ethosUserProfile.RequiredKudos, null, null);
                string notice = null;
                if (ethosUserProfile.EstimatedProgress > ethosUserProfile.CurrentSemesterCheckinCount)
                    notice = (ethosUserProfile.EstimatedProgress - ethosUserProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                infoList.AddListItem("Kudos Pace: " + ethosUserProfile.EstimatedProgress, notice, "Kudos you should have this far in the semester to reach your goal");

                //List linksList = new List();
                //linksList.Heading = "Ethos Links";
                infoList.AddListItem("Upcoming Ethos Events", null, null, new XModuleLink("ethos", "/GetEthosEvents"));
                //linksList.AddListItem("Submit an Ethos Event", null, null, new ExternalLink("http://www.oc.edu/spiritual-life/ethos/ethos-event-approval-form.html"));
                //linksList.AddListItem("Ethos Requirements and Guidelines", null, null, new ExternalLink("http://www.oc.edu/spiritual-life/ethos/event-requirements-and-guidelines.html"));
                ethosReturn.Add(infoList);
                //ethosReturn.Add(linksList);
            }
            else if (userProfile != null && userProfile.PrimaryRole == Person.UserType.Proxy)
            {
                int chapelViewDependentCount = 0;
                Dictionary<string, Person.Portal.Proxy.ProxyProfile> dependents = Person.Portal.Proxy.ProxyProfile.GetDependents(userProfile.UserID);

                if (dependents.Count > 0)
                {
                    foreach (Person.Portal.Proxy.ProxyProfile dependent in dependents.Values)
                    {
                        foreach (Person.Portal.Proxy.ProxyPermissions proxyPermissions in dependent.ProxyPermissions)
                        {
                            if (proxyPermissions.ToString() == "ChapelView")
                            {
                                chapelViewDependentCount++;

                                Ethos.UserProfile depEthosUserProfile = new Ethos.UserProfile(dependent.DependentID);

                                List infoList = new List();
                                infoList.Heading = "Ethos Information for " + depEthosUserProfile.DisplayName;
                                infoList.AddListItem("Kudos Earned: " + depEthosUserProfile.CurrentSemesterCheckinCount, null, null, new XModuleLink("ethos", "/GetEthosHistory"));
                                infoList.AddListItem("Kudos Required: " + depEthosUserProfile.RequiredKudos, null, null);
                                string notice = null;
                                if (depEthosUserProfile.EstimatedProgress > depEthosUserProfile.CurrentSemesterCheckinCount)
                                    notice = (depEthosUserProfile.EstimatedProgress - depEthosUserProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                                infoList.AddListItem("Kudos Pace: " + depEthosUserProfile.EstimatedProgress, notice, "Kudos you should have this far in the semester to reach your goal");
                                ethosReturn.Add(infoList);

                                break;
                            }
                        }
                    }

                    if (chapelViewDependentCount == 0)
                    {
                        List infoList = new List();
                        infoList.AddListItem("Access to Ethos information is not available for any students.", null, null);
                        ethosReturn.Add(infoList);
                    }
                }
            }
            else
            {
                List eventsList = new List();
                eventsList.Heading = "Upcoming Events";

                if (events != null)
                {
                    int eventsShowCount = 6;

                    if (events.Length < eventsShowCount)
                        eventsShowCount = events.Length;

                    for (int i = 0; i < eventsShowCount; i++)
                    {
                        if (events[i].PrivateEvent)
                            continue;

                        Ethos.Location eventLocation = new Ethos.Location(events[i].LocationID);
                        StringBuilder eventDescription = new StringBuilder();

                        int roomResult;
                        bool eventRoomIsInt = int.TryParse(events[i].Room, out roomResult);
                        string eventRoom = events[i].Room;
                        if (eventRoomIsInt)
                            eventRoom = eventRoom.Insert(0, "Room ");

                        eventDescription.Append(events[i].EventStart.ToString("MMM dd, h:mm tt")).Append("-").Append(events[i].EventEnd.ToString("h:mm tt")).Append(", ").Append(eventRoom).Append(", ").Append(eventLocation.Title);
                        eventsList.AddListItem(events[i].EventName, null, eventDescription.ToString(), new XModuleLink("ethos", "/GetEthosEvents/Details/" + events[i].ID));
                    }
                }
                else
                {
                    eventsList.AddListItem("No Events Found", null, null);
                }

				if (eventsList.Items.Count == 0)
                    eventsList.AddListItem("No Events Found", null, null);
                ethosReturn.Add(eventsList);
            }

            return ethosReturn;
        }

        public static List MyInfoPortlet(Person.Portal.UserProfile userProfile)
        {
            List myInfoList = new List();

            if (userProfile.PrimaryRole == Person.UserType.Student)
            {
                if (!String.IsNullOrEmpty(userProfile.HousingBuildingCode))
                    myInfoList.AddListItem("Housing " + userProfile.HousingBuildingCode, null, null);

                if (!String.IsNullOrEmpty(userProfile.HousingRoomNumber))
                    myInfoList.AddListItem("Housing Room #: " + userProfile.HousingRoomNumber, null, null);

                if (!String.IsNullOrEmpty(userProfile.MailBox))
                    myInfoList.AddListItem("Mail Box #" + userProfile.MailBox + ": " + userProfile.MailBoxCombo, null, null, new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox"));

                string printBalance = Person.uniFLOW.UserInformation.GetUserPrintBalance(userProfile.UserID);
                if (!String.IsNullOrEmpty(printBalance))
                {
                    decimal balance = System.Convert.ToDecimal(printBalance);
                    myInfoList.AddListItem("Print Balance: " + balance.ToString("C3", new CultureInfo("en-US")), null, null, new ExternalLink("https://printers.oc.edu/pwclient"));
                }
            }

            Person.Portal.MealPlan mealPlan = new Person.Portal.MealPlan(userProfile.UserID);

            if (!String.IsNullOrEmpty(mealPlan.MealBoard))
                myInfoList.AddListItem("Meals Remaining: " + mealPlan.MealBoard, null, null, new ExternalLink("https://services.oc.edu/redirect/520"));

            if (!String.IsNullOrEmpty(mealPlan.MealPoints))
                myInfoList.AddListItem("Eagle Bucks Remaining: " + mealPlan.MealPoints, null, null, new ExternalLink("https://services.oc.edu/redirect/521"));

            if (!String.IsNullOrEmpty(userProfile.IDPin))
                myInfoList.AddListItem("ID Card Pin #", null, null, new XModuleLink("my_info", "/IdPin"));

            if (!String.IsNullOrEmpty(userProfile.PasswordExpireDate.ToString()))
            {
                DateTime passwordExpireDate = userProfile.PasswordExpireDate;
                myInfoList.AddListItem("Password Expires on " + passwordExpireDate.ToString("MM/dd/yyyy"), null, null, new ExternalLink("https://account.oc.edu/password"));
            }

            if (userProfile.isEmployee)
            {
                if (userProfile.LongDistancePin.Count > 0)
                {
                    foreach (Person.Portal.LongDistancePin longDistancePin in userProfile.LongDistancePin)
                    {
                        myInfoList.AddListItem("Long Distance Pin #" + longDistancePin.Pin.ToString(), null, null);
                    }
                }

                if (userProfile.Extensions != null && userProfile.Extensions.Count > 0)
                {
                    Person.Shoretel.MailBox mailBox = new Person.Shoretel.MailBox(userProfile.Extensions[0].Number.ToString());
                    myInfoList.AddListItem("Voicemail Password: " + mailBox.Password, null, null);
                    myInfoList.AddListItem("Voicemails: " + mailBox.NumberOfMessages.ToString(), null, null);
                }

            }

            return myInfoList;
        }
    }
}