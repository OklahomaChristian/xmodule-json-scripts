using System;
using System.Collections.Generic;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Person.Portal.ServiceCategory[] serviceCategories, string searchString, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            response.Content.Add(new Divider { BorderStyle = "none" });
            Detail detail = new Detail();
            detail.Title = "Services";
            //detail.Buttons = new List<object>{ new LinkButton
            //{
            //    Title = "Edit Quicklinks",
            //    Link = new XModComm.StandardLinks.XModuleLink
            //    {
            //        XModule = new XModComm.StandardLinks.XModuleObject
            //        {
            //            ID = "quicklinks",
            //            RelativePath = "/edit"
            //        }
            //    },
            //    AccessoryIcon = "edit"
            //}};

            Form form = new Form();
            form.RelativePath = "/SearchServices";

            XModComm.Forms.FormInput.Search textBox = new XModComm.Forms.FormInput.Search();
            textBox.Name = "SearchString";
            textBox.Label = "Services Search";
            textBox.Value = searchString;
            textBox.Required = false;
            form.Items.Add(textBox);

            ButtonContainer buttonContainer = new ButtonContainer();

            Button button = new Button()
            {
                Title = "Search",
                Name = "Submit",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);

            button = new Button()
            {
                Title = "Clear Search",
                Name = "Clear",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);

            form.Items.Add(buttonContainer);
            detail.Content.Add(form);

            //List quickLinksList = new List();
            //quickLinksList.Items.Add(
            //    new Item()
            //    {
            //        Title = "Edit QuickLinks",
            //        Description = "Bookmark your favorite services",
            //        Link = new XModuleLink("quicklinks", "/edit")
            //    }
            //);
            //detail.Content.Add(quickLinksList);
            detail.Content.Add(new Divider { BorderStyle = "none" });

            foreach (Person.Portal.ServiceCategory category in serviceCategories)
            {
                Collapsible collapsible = new Collapsible()
                {
                    MarginTop = "none",
                    MarginBottom = "none",
                    BorderTopWidth = "0px",
                    BorderBottomWidth = "0px"
                };
                collapsible.Collapsed = false;
                collapsible.Title = category.Name;
                collapsible.TitleTextColor = "#000000";
                collapsible.TitleFontWeight = "bold";
                List list = new List();
                list.ItemSize = "xsmall";
                list.ListStyle = "grouped";
                list.MarginTop = "none";
                list.MarginBottom = "none";

                foreach (Person.Portal.Service service in category.Services)
                {
                    Link link = null;
                    if (tokenData.PageType == "small" || (tokenData.PageType == "large" && (tokenData.Platform == "ios" || tokenData.Platform == "android")))
                        link = new XModuleLink("utilities", String.Format("/SSORedirect/{0}", service.ID));
                    else
                        link = new ExternalLink(String.Format("{0}", service.RedirectURI));

                    //if (service.Title.Contains("Independent Study Form"))
                    //    Person.Utilities.Email.SendFromServer("peyton.chenault@oc.edu", "ModoGateway@oc.edu", "Independent Study Form Values", service.Visible.ToString() + "  " + service.Enabled.ToString());

                    if (service.Visible && service.Enabled)
                    {

                        list.Items.Add(
                            new Item()
                            {
                                Title = service.Title,
                                Link = link,
                                AccessoryIcon = "none"
                            }
                        );
                    }
                }

                collapsible.Content.Add(list);
                detail.Content.Add(collapsible);
            }

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}