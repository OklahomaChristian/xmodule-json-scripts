using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
		public bool TestJSON(Person.Portal.UserProfile userProfile, string printBalance, Person.Portal.MealPlan mealPlan, Person.Shoretel.MailBox voicemail, TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(userProfile, printBalance, mealPlan, voicemail, tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}
		
        public string GetJSON(Person.Portal.UserProfile userProfile, string printBalance, Person.Portal.MealPlan mealPlan, Person.Shoretel.MailBox voicemail, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = userProfile.DisplayName;
            detail.Subtitle = userProfile.UserID;
            
			detail.Thumbnail = new XModComm.Thumbnail();
            detail.Thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
            detail.Thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;

            // security
            List security = new List();
            security.Grouped = true;
            security.Heading = "Security Information";
			security.Heading = security.Heading.ToUpper();

            if (!String.IsNullOrEmpty(userProfile.IDPin))
                security.AddListItem("ID Card Pin #", null, null, new XModuleLink("my_info", "/IdPin"));
            if (!String.IsNullOrEmpty(userProfile.PasswordExpireDate.ToString()))
            {
                security.AddListItem("Change Password", "", userProfile.PasswordExpireDate.ToShortDateString(), new ExternalLink("https://account.oc.edu/password"));
            }
            if (security.Items.Count > 0)
                detail.Content.Add(security);

            // uniFlow
            if (userProfile.PrimaryRole == Person.UserType.Student)
            {
                if (!String.IsNullOrEmpty(printBalance))
                {
                    decimal balance = System.Convert.ToDecimal(printBalance);
                    List uniFlow = new List();
                    uniFlow.Grouped = true;
                    uniFlow.Heading = "Printing";
					uniFlow.Heading = uniFlow.Heading.ToUpper();
                    uniFlow.AddListItem("Print Balance", "", balance.ToString("C3", new CultureInfo("en-US")), new ExternalLink("http://printers.oc.edu/pwclient"));
                    detail.Content.Add(uniFlow);
                }
            }

            // HR Information
            if (userProfile.isEmployee)
            {
                List hrInfo = new List();
                hrInfo.Grouped = true;
                hrInfo.Heading = "Human Resources Information".ToUpper();

				if (userProfile.Titles != null) 
				{
					foreach (string title in userProfile.Titles)
					{						
						string titleReplaced = Regex.Replace(title, @"\\", "");
						hrInfo.AddListItem(titleReplaced, "Job Title", "");
					}	
				}	

                hrInfo.AddListItem(userProfile.OfficeBuilding + " (" + userProfile.OfficeBuildingCode + ") " + userProfile.OfficeRoomNumber, "On Campus", userProfile.Department);

                string homeAddress = null;
                if (userProfile.Addresses != null)
                {
                    foreach (Person.Portal.Address address in userProfile.Addresses)
                    {
                        if (address.Type == Person.Portal.AddressType.Home &&
                            address.AddressLines != null &&
                            address.City != "" &&
                            address.State != "" &&
                            address.Zip != "")
                        {
                            homeAddress = address.AddressLines.lines[0] + address.City + ", " + address.State + " " + address.Zip;
                            break;
                        }
                    }
                    hrInfo.AddListItem("Home Address", "Off Campus", homeAddress);
                }

				if (userProfile.PhoneNumbers != null)
				{
					foreach (Person.Portal.PhoneNumber phone in userProfile.PhoneNumbers)
					{
						hrInfo.AddListItem(phone.NumberType.ToString(), "", phone.Number);
					}
				}

                hrInfo.AddListItem("Update your information with HR", "", "", new ExternalLink("https://services.oc.edu/158"));

                detail.Content.Add(hrInfo);
            }

            // Housing and Mailroom
            List housingMailroom = new List();
            housingMailroom.Grouped = true;
            housingMailroom.Heading = "Housing and Mailroom Information".ToUpper();

            if (!String.IsNullOrEmpty(userProfile.HousingBuilding))
                housingMailroom.AddListItem("Housing Building", "", userProfile.HousingBuilding);
            if (!String.IsNullOrEmpty(userProfile.HousingRoomNumber))
                housingMailroom.AddListItem("Room Number", "", userProfile.HousingRoomNumber);
            if (!String.IsNullOrEmpty(userProfile.MailBox))
                housingMailroom.AddListItem("Mailbox", "", userProfile.MailBox, new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox"));
            if (!String.IsNullOrEmpty(userProfile.MailBoxCombo))
                housingMailroom.AddListItem("Mailbox Combo", "", userProfile.MailBoxCombo, new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox"));

            if (housingMailroom.Items.Count > 0)
                detail.Content.Add(housingMailroom);

            // Meal Info
            List mealList = new List();
            mealList.Grouped = true;
            mealList.Heading = "Meal Information".ToUpper();

            if (!String.IsNullOrEmpty(mealPlan.MealPlanName))
                mealList.AddListItem("Meal Plan", "Plan Information", mealPlan.MealPlanName); // TODO: Add URL to dining
            if (!String.IsNullOrEmpty(mealPlan.MealBoard) && mealPlan.MealBoard != "0.00")
                mealList.AddListItem("Meals Remaining", "", mealPlan.MealBoard, new ExternalLink("https://services.oc.edu/redirect/520"));
            if (!String.IsNullOrEmpty(mealPlan.MealPoints) && mealPlan.MealPoints != "0.00")
                mealList.AddListItem("Balance Remaining", "Eagle Bucks", "$" + mealPlan.MealPoints, new ExternalLink("https://services.oc.edu/redirect/521"));

            if (mealList.Items.Count > 0)
                detail.Content.Add(mealList);

            // Phone Information
            if (userProfile.Extensions != null && userProfile.Extensions.Count > 0)
            {
                List phoneList = new List();
                phoneList.Grouped = true;
                phoneList.Heading = "Phone Information".ToUpper();

                if (!String.IsNullOrEmpty(userProfile.Extensions.ToString()))
                    phoneList.AddListItem("Extension", "", userProfile.Extensions[0].Number);// TODO: Loop through all numbers
                foreach (Person.Portal.LongDistancePin pin in userProfile.LongDistancePin)
                {
                    phoneList.AddListItem("Long Distance Pin", pin.Description, pin.Pin);
                }
                phoneList.AddListItem("Voicemails", "", voicemail.NumberOfMessages.ToString(), new ExternalLink("https://shoretel.oc.edu"));
                phoneList.AddListItem("Voicemail Password", "", voicemail.Password); //TODO: add link and code to change password

                detail.Content.Add(phoneList);
            }

            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}