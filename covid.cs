using System;
using System.Linq;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Net.Mail;
using System.Collections.Generic;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(bool covidAssessmentStatus, bool covidStatus, TokenData tokenData)
        {
            XModComm.Response response = new Response();

            // Default is covidAssessmentStatus == true (needs form)
            string image = "";
            string title = "";
            object externalLink = null;

            Person.Utilities.Email.SendFromServer("luther.hartman@oc.edu", "gateway.svc@oc.edu", "COVID Assessment status: ", covidAssessmentStatus.ToString(), false);

            if (!covidAssessmentStatus)
            {
                image = "https://xmodule-examples.modolabs.net/docs/images/sky.jpg";
                title = "You must complete your COVID assessment for today.";
//                externalLink = new XModComm.Links.ModuleLink("mobile_home", "_/covid_assessment", new Dictionary<string, string>());
                externalLink = new XModComm.Links.ExternalLink("https://services.oc.edu/forms/covidassessment");
            }
            else if (covidAssessmentStatus && !covidStatus)
            {
                image = "https://static.modolabs.com/workready/healthcheck/verified.png";
                title = "You're COVID Cleared!";
            }
            else
            {
                image = "https://static.modolabs.com/workready/healthcheck/denied.png";
                title = "COVID Denied. Pease see a doctor!";
            }

            var output = new { elementType = "publish:mondrian:contentTile", title = title, 
                //foreground = new { alt = "Altnerate image of sun", url =  },
                background = new { url = image},
                link = externalLink
            };

            response.Content = new List<object> { output };

            //response.Content = new List<object> { 
            //  new
            //  {
            //    elementType = "publish:mondrian:contentTile"
            //  }
            //};

            string outputJSON = Person.Utilities.JSON.Serializer.Serialize(response);
            Person.Utilities.Email.SendFromServer("luther.hartman@oc.edu", "gateway.svc@oc.edu", "COVID Assessment test", outputJSON, false);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}