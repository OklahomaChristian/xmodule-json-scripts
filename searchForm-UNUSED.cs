using System;
using Person;
using XModComm;
using System.Text;
using XModComm.UI;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(TokenData tokenData)
        {
            XModComm.Response response = new Response();
			Detail detail = new Detail();
			detail.Title = "myOC Search";
			detail.Title = detail.Title.ToUpper();
            Form form = new Form();
            form.AddItem("searchString", "", false, FormItem.FormInputType.Text);  // TEST -> FormItem.FormInputType.Search
            ButtonContainer buttonContainer = new ButtonContainer();
            Button button = new Button()
            {
                Title = "Submit",
                Name = "Submit",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);
            form.Items.Add(buttonContainer);
			detail.Content.Add(form);
            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}