using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Tables;
using Person.Colleague.Schedule;

namespace Dynamic
{
	public class Script
	{
		public string GetJSON(System.Collections.Generic.List<FacultySections.SectionRosterProfile> profileList, TokenData tokenData)
		{
			XModComm.Response response = new Response();

			if (tokenData.PageType == "small")
			{
				List list = new List();

				if (profileList != null && profileList.Count > 0)
				{
					foreach (FacultySections.SectionRosterProfile profile in profileList)
					{
						Thumbnail thumbnail = new Thumbnail();
						thumbnail.Url = profile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(profile.UserID, profile.PicIDURL) : String.Empty;
						thumbnail.Alt = profile.DisplayName != null ? profile.DisplayName : String.Empty;
						//thumbnail. = 80;
						//thumbnail.MaxWidth = 80;

						string listDescription = "Final Grade: ";

						listDescription = profile.FinalGrade != null ? String.Format("{0} {1}", listDescription, ConvertGrade(profile.FinalGrade)) : String.Format("{0} {1}", listDescription, "-");
						list.AddListItem(profile.DisplayName, null, listDescription, new XModuleLink("userdetails", "/" + profile.IDToken), thumbnail);
					}
				}
				else
				{
					list.AddListItem("No enrolled students found", null, null);
				}

				response.RegionContent.Add(list);
			}
			else
			{
				Table table = new Table();

				table.ColumnOptions.Add(new ColumnOption() { Header = "Name" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "ID Number" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Status" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "A/D/W Date" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "P/F?" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Audit" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Class" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Final Grade" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "Verified Grade" });
				table.ColumnOptions.Add(new ColumnOption() { Header = "FERPA" });
				table.ColumnHeaders = true;

				if (profileList != null && profileList.Count > 0)
				{
					Row titleRow = new Row();
					titleRow.Cells.Add(new Cell() { Title = "Name" });
					titleRow.Cells.Add(new Cell() { Title = "ID #" });
					titleRow.Cells.Add(new Cell() { Title = "Status" });
					titleRow.Cells.Add(new Cell() { Title = "Changed" });
					titleRow.Cells.Add(new Cell() { Title = "P/F?" });
					titleRow.Cells.Add(new Cell() { Title = "Audit" });
					titleRow.Cells.Add(new Cell() { Title = "Class" });
					titleRow.Cells.Add(new Cell() { Title = "Final" });
					titleRow.Cells.Add(new Cell() { Title = "Verified" });
					titleRow.Cells.Add(new Cell() { Title = " " });
					table.Rows.Add(titleRow);

					foreach (FacultySections.SectionRosterProfile profile in profileList)
					{
						Row row = new Row();
						row.Url = new XModuleLink("userdetails", "/" + profile.IDToken);

						string firstName = !String.IsNullOrEmpty(profile.Nickname) ? profile.Nickname : profile.FirstName;
						string displayName = firstName + " " + profile.LastName;

						//Name
						row.Cells.Add(new Cell() { Title = displayName, Image = new Image() { Url = profile.PicIDURL, CropStyle = "fill" } });

						//ID Number
						row.Cells.Add(new Cell() { Title = profile.UserID });

						//Status
						if (profile.Status == "N")
						{
							row.Cells.Add(new Cell() { Title = "New" });
						}
						else if (profile.Status == "W")
						{
							row.Cells.Add(new Cell() { Title = "Waitlisted" });
						}
						else if (profile.Status == "A")
						{
							row.Cells.Add(new Cell() { Title = "Add" });
						}
						else
						{
							row.Cells.Add(new Cell() { Title = "" });
						}

						//ADW Date
						row.Cells.Add(new Cell() { Title = profile.StatusDate.ToShortDateString() });

						//P/F
						if (profile.PassFail)
						{
							row.Cells.Add(new Cell() { Title = "True" });
						}
						else
						{
							row.Cells.Add(new Cell() { Title = "" });
						}

						//Audit
						if (profile.Audit)
						{
							row.Cells.Add(new Cell() { Title = "True" });
						}
						else
						{
							row.Cells.Add(new Cell() { Title = "" });
						}

						//Class
						row.Cells.Add(new Cell() { Title = profile.Classification });

						//Final Grade
						row.Cells.Add(new Cell() { Title = ConvertGrade(profile.FinalGrade) });

						//Verified Grade
						row.Cells.Add(new Cell() { Title = ConvertGrade(profile.VerifiedGrade) });

						//FERPA
						row.Cells.Add(new Cell() { Title = "View Permissions" });

						table.Rows.Add(row);
					}
				}
				else
				{
					Row row = new Row();
					row.Cells.Add(new Cell() { Title = "No enrolled students found" });
					row.Cells.Add(new Cell() { Title = "" });
					row.Cells.Add(new Cell() { Title = "" });
					table.Rows.Add(row);
				}

				response.RegionContent.Add(table);
			}

			return Person.Utilities.JSON.Serializer.Serialize(response);
		}

		// Convert Colleague integer grade to letter grade
		static string ConvertGrade(string gradeNumber)
		{
			if (String.IsNullOrEmpty(gradeNumber))
			{
				return "";
			}
			string value = "";
			GradeDictionary.TryGetValue(gradeNumber, out value);
			return value;
		}

		static readonly Dictionary<string, string> GradeDictionary = new Dictionary<string, string>
		{
			{"1","A"},
			{"2","B"},
			{"3","C"},
			{"4","D"},
			{"5","F"},
			{"6","P"},
			{"7","NP"},
			{"8","I"},
			{"9","W"},
			{"10","AU"},
			{"11","P*"},
			{"12","WP"},
			{"13","NC"},
			{"14","WF"},
			{"16","A"},
			{"17","B"},
			{"18","C"},
			{"19","D"},
			{"20","F"},
			{"21","NP"},
			{"22","P"},
			{"23","I"},
			{"24","W"},
			{"25","AU"},
			{"26","P*"},
			{"27","WP"},
			{"28","NC"},
			{"29","WF"},
			{"30","E"},
			{"32","L"},
			{"33","L"}
		};
	}
}
