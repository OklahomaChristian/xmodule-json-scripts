using System;
using XModComm;
using XModComm.UI;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Person.Portal.UserProfile userProfile, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = userProfile.DisplayName;
            detail.Subtitle = userProfile.UserID;
            detail.Thumbnail = new XModComm.Thumbnail();
            detail.Thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
            detail.Thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;

            // security
            List security = new List();
            security.Grouped = true;
            security.Heading = "Security Information".ToUpper();

            if (!String.IsNullOrEmpty(userProfile.IDPin))
                security.AddListItem("ID Card Pin Number", "", userProfile.IDPin);
           
            if (security.Items.Count > 0)
                detail.Content.Add(security);

            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}