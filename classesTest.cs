using System;
using System.Linq;
using Person;
using Person.Colleague;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;
using XModComm.Tables;
using Person.Colleague.Schedule;
using ExtensionMethods;
using XModComm.Arguments;

namespace ExtensionMethods
{
    public static class SectionExtensions
    {
        public static T GetSectionDetails<T>(this ClassSchedule.Section section, string term, bool isDependentSchedule) where T : class
        {
            Type genericType = typeof(T);

            object obj = null;
            switch (genericType.Name)
            {
                case "List":
                    obj = GetList(section, term, isDependentSchedule);
                    break;
                case "Row":
                    obj = GetRow(section, term, isDependentSchedule);
                    break;
            }

            return (T)obj ?? null;
        }

        static Row GetRow(ClassSchedule.Section section, String term, bool isDependentSchedule) // large
        {
            Row row = new Row();
            if (!isDependentSchedule)
                row.Link = new RelativeLink(String.Format("/ClassDetails/{0}/{1}", term, section.STTR_SCHEDULE_ID));

            row.Cells.Add(new Cell() { Title = section.ShortTitle }); // Course Title
            row.Cells.Add(new Cell() { Title = section.MidTermGrade ?? "-" }); // MidTerm
            row.Cells.Add(new Cell() { Title = section.VerifiedGrade ?? "-" }); // Final Grades
            row.Cells.Add(new Cell() { Title = section.Name }); // Course Description
            row.Cells.Add(new Cell() { Title = section.Credits }); //Credits

            bool showEvaluation = false;
            if (section.EvaluationDateTimes != null && !isDependentSchedule)
            {
                foreach (ClassSchedule.EvaluationDateTime evaluation in section.EvaluationDateTimes)
                {
                    if (evaluation != null &&
                        (((evaluation.StartDateTime.Year != 1967 && evaluation.EndDateTime.Year != 1967) && evaluation.isScheduledEvaluationOpen) || evaluation.StatusType.ToString() == "Open")
                        && !evaluation.AlreadyEvaluated)
                    {
                        showEvaluation = true;
                        break;
                    }
                }
            }

            var meetingTimeCells = new System.Collections.Generic.List<Cell>(5);
            for (int i = 0; i < 5; i++)
                meetingTimeCells.Add(new Cell());
            if (section.MeetingTimes != null)
            {
                foreach (ClassSchedule.MeetingTime meetingTime in section.MeetingTimes)
                {
                    //Room Number
                    meetingTimeCells[0].Title += meetingTime.Building.Code == "TBA" ? "-" : String.Format("{0} {1}<br>", meetingTime.Building.Code, meetingTime.Room);

                    //Days
                    string days = "";
                    if (meetingTime.Days != null)
                        meetingTime.Days.ToList().ForEach((string day) => { days = String.Format("{0} {1}", days, day); });
                    meetingTimeCells[1].Title += String.Format("{0}<br>", days);

                    //Times
                    meetingTimeCells[2].Title += String.Format("{0}-{1}<br>", meetingTime.StartTime, meetingTime.EndTime);

                    //Dates
                    meetingTimeCells[3].Title += String.Format("{0}-{1}<br>", meetingTime.StartDate, meetingTime.EndDate);

                    //Instructors
                    string professors = "";
                    if (meetingTime.Faculty != null)
                        meetingTime.Faculty.ToList().ForEach((Person.Portal.UserProfile professor) => { professors = String.Format("{0}{1},", professors, professor.DisplayName); });

                    meetingTimeCells[4].Title += String.Format("{0}", professors);
                }
            }

            string distinctProfessors = "";
            if (meetingTimeCells[4].Title != null)
				meetingTimeCells[4].Title.TrimEnd(',').Split(',').Distinct().ToList().ForEach((string professor) => { distinctProfessors = String.Format("{0} {1},", distinctProfessors, professor); });
            meetingTimeCells[4].Title = distinctProfessors.TrimEnd(',').TrimStart(' ');

            if (showEvaluation)
                meetingTimeCells[4].SubTitle = "Instructor Evaluation Needed";

            meetingTimeCells.ForEach((Cell c) => { row.Cells.Add(c); });

            return row;
        }

        static XModComm.UI.List GetList(ClassSchedule.Section section, String term, bool isDependentSchedule) // small
        {
            // TODO: NO LINK TO ClassDetails

            var list = new XModComm.UI.List() { Grouped = true };
            list.Heading = String.Format("{0} {1}", section.ShortTitle, section.Name);

            string midTermGrade = section.MidTermGrade ?? "-";
            string verifiedGrade = section.VerifiedGrade ?? "-";
            list.AddListItem(String.Format("{0} / {1} / {2}", section.Credits, midTermGrade, verifiedGrade), "Grade Summary", "Credits/Midterm/Final");

            string times = "";
            string dates = "";
            if (section.MeetingTimes != null)
            {
                foreach (var meetingTime in section.MeetingTimes)
                {
                    if (meetingTime.Days != null)
                        meetingTime.Days.ToList().ForEach((string day) => { times = String.Format("{0} {1}", times, day); });
                    times += String.Format(" {0}-{1} ({2} / {3})<br>", meetingTime.StartTime, meetingTime.EndTime, meetingTime.Building.Code ?? "-", meetingTime.Room ?? "-");
                    dates += String.Format("{0}-{1},", meetingTime.StartDate, meetingTime.EndDate);
                }
            }

            string distinctDates = "";
            dates.TrimEnd(',').Split(',').Distinct().ToList().ForEach((string date) => { distinctDates = String.Format("{0}{1}<br>", distinctDates, date); });
            list.AddListItem(times.TrimStart(' '), "Times/Dates", distinctDates.TrimStart(' '));
            if (!isDependentSchedule)
                list.AddListItem("Class Details", null, null, new RelativeLink(String.Format("/ClassDetails/{0}/{1}", term, section.STTR_SCHEDULE_ID)));
            return list;
        }
    }
}

namespace Dynamic
{
    public class Script
    {
		public bool TestJSON(System.Collections.Generic.List<ScheduleArgument> scheduleArguments, TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(scheduleArguments, tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}
		
        public string GetJSON(System.Collections.Generic.List<ScheduleArgument> scheduleArguments, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            TabContainer tabContainer = new TabContainer();
            System.Collections.Generic.List<string> terms = new System.Collections.Generic.List<string>();
			string currentTerm = null;

            foreach (ScheduleArgument argument in scheduleArguments)
            {
                ClassSchedule classSchedule = argument.ClassSchedule;
                System.Collections.Generic.List<string> activeStudentTerms = argument.ActiveStudentTerms;
                terms.AddRange(activeStudentTerms);
                string displayName = argument.DisplayName;

                if (tokenData.PageType == "small" && classSchedule != null)
                {
					if (String.IsNullOrEmpty(currentTerm))
						currentTerm = classSchedule.Term;
                    Term term = new Term(classSchedule.Term, false, null);
                    string heading = term.TERM_DESC + " Classes";
                    Detail detail = new Detail { Title = heading.ToUpper(), Subtitle = displayName };

                    classSchedule.Sections.ToList().ForEach((ClassSchedule.Section section) => { detail.Content.Add(section.GetSectionDetails<XModComm.UI.List>(classSchedule.Term, argument.IsDependentSchedule)); });

                    AjaxList gpaList = new AjaxList() { Grouped = true };
                    gpaList.Heading = "GPA".ToUpper();
                    gpaList.Items = new Item() { AjaxRelativePath = String.Format("/GetGPA/{0}/{1}", classSchedule.Term, classSchedule.UserID) };
                    detail.Content.Add(gpaList);

					var orderBooksList = new XModComm.UI.List() { Grouped = true };
					orderBooksList.AddListItem("Purchase your books from the bookstore", null, null, new ExternalLink("http://occampusstore.com"));
					detail.Content.Add(orderBooksList);
					
                    if (!argument.IsDependentSchedule)
                    {
                        var list = new XModComm.UI.List() { Grouped = true };
                        list.AddListItem("Subscribe to your class schedule in your calendar application", null, null, new ExternalLink(String.Format("webcal://calendar.oc.edu/studentSchedule/{0}", argument.IDToken)));
                        detail.Content.Add(list);
                    }

                    response.Content.Add(detail);
                }
                else if (classSchedule != null)// DEFAULT TO LARGE
                {
					if (String.IsNullOrEmpty(currentTerm))
						currentTerm = classSchedule.Term;
                    Term term = new Term(classSchedule.Term, false, null);
                    string heading = term.TERM_DESC + " Classes";
                    Tab tab = new Tab() { Title = displayName };

                    Table table = new Table(0, 0) { Heading = heading.ToUpper() };

                    new string[] { "Course Title", "Midterm", "Final", "Course ID", "Credits", "Room", "Days", "Times", "Dates (Includes Finales)", "Instructor" }.ToList().ForEach(
                        (string header) => { table.ColumnOptions.Add(new ColumnOption() { Header = header }); });

                    if (classSchedule != null)
                        classSchedule.Sections.ToList().ForEach((ClassSchedule.Section section) => { table.Rows.Add(section.GetSectionDetails<Row>(classSchedule.Term, argument.IsDependentSchedule)); });

                    tab.Content.Add(table);

                    AjaxList gpaList = new AjaxList();
                    gpaList.Heading = "GPA".ToUpper();
                    gpaList.Items = new Item() { AjaxRelativePath = String.Format("/GetGPA/{0}/{1}", classSchedule.Term, classSchedule.UserID) };
                    tab.Content.Add(gpaList);

					HtmlFragment orderBooksList = new HtmlFragment();
					orderBooksList.Html = "<a href='http://occampusstore.com'>Click here to purchase your books from the bookstore</a>";
					tab.Content.Add(orderBooksList);
					
                    if (!argument.IsDependentSchedule)
                    {
                        HtmlFragment calendar = new HtmlFragment();
                        calendar.Html = "<h3>Subscribe to your class schedule in your calendar application (Google Calendar, Mac OS Calendar, iOS, Android, etc.)</h3>";
                        calendar.Html += String.Format("<a href='webcal://calendar.oc.edu/studentSchedule/{0}'>Click here to subscribe</a> or copy this link: https://calendar.oc.edu/studentSchedule/{0}", argument.IDToken);
                        tab.Content.Add(calendar);
                    }

                    tabContainer.Tabs.Add(tab);
                }
				else if (tokenData.PageType == "small")
				{
                    Detail detail = new Detail { Title = "Classes".ToUpper(), Subtitle = displayName };
					List list = new List() { Grouped = true };
					list.AddListItem("Class Schedule Not Found", null, null);
                    detail.Content.Add(list);
					response.Content.Add(detail);
				}
				else
				{
					Tab tab = new Tab() { Title = displayName };
					List list = new List();
					list.AddListItem("Class Schedule Not Found", null, null);
                    tab.Content.Add(list);
					tabContainer.Tabs.Add(tab);
				}
            }

            if (tokenData.PageType == "large")
                response.Content.Add(tabContainer);

            if (response.Content.Count == 0)
            {
                List notFoundList = new List();
                notFoundList.AddListItem("Class schedule not found", null, null);
                response.Content.Add(notFoundList);
            }

            if (terms.Count > 0)
            {
                terms = terms.Distinct().ToList();

                System.Collections.Generic.List<Term> termList = new System.Collections.Generic.List<Term>();

                foreach (var term in terms)
                {
                    termList.Add(new Term(term, false, null));
                }

                termList.Sort();
                termList.Reverse();

                Select termSelect = new Select();
                termSelect.Name = "term";
                termSelect.Label = "";
				termSelect.Value = currentTerm;
                termSelect.OptionLabels = termList.Select(x => x.TERM_DESC).ToList();
                termSelect.OptionValues = termList.Select(x => x.TERM_REPORTING_TERM).ToList();

                Form form = new Form();
                form.Items.Add(termSelect);  // TEST -> FormItem.FormInputType.Search
                ButtonContainer buttonContainer = new ButtonContainer();
                Button button = new Button()
                {
                    Title = "Change Term",
                    Name = "Submit",
                    Value = "1"
                };
                buttonContainer.Buttons.Add(button);
                form.Items.Add(buttonContainer);
                response.Content.Insert(0, form);
            }

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
	
}