using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Forms;
using XModComm.Arguments;


namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.Portal.Proxy.ProxyProfile[] proxyProfiles, string savedMessage, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(proxyProfiles, savedMessage, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.Portal.Proxy.ProxyProfile[] proxyProfiles, string savedMessage, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            response.Content.Add(
                new Divider
                {
                    BorderStyle = "none"
                });

            Form form = new Form();
            Detail detail = new Detail();
            detail.Title = "3rd Party Access";
            detail.Description = "Choose who has what access to your information at OC";

            /*
			if (savedMessage != "##$test$##")
			{
				HtmlFragment htmlFrag = new HtmlFragment();

				htmlFrag.Html = "<a href='http://my2.oc.edu/'>Please use the old portal for 3rd Party Access</a>";

				form.Items.Add(htmlFrag);

				detail.Content.Add(form);
				response.Content.Add(detail);

				return Person.Utilities.JSON.Serializer.Serialize(response);
			}
			*/

            ///////////////////////////
            // DISPLAY SAVED MESSAGE
            ///////////////////////////
            if (!String.IsNullOrEmpty(savedMessage))
            {
                response.Metadata.AddBanner(new Banner()
                {
                    Message = savedMessage,
                    Type = "confirmation"
                });
            }

            ////////////////////////////////////////////
            // DISPLAY NO ACCOUNT OR PERMISSIONS FORMS
            ////////////////////////////////////////////
            foreach (Person.Portal.Proxy.ProxyProfile profile in proxyProfiles)
            {
                detail.Content.Add(new Divider());
                if (String.IsNullOrEmpty(profile.ProxyUserProfile.UserName))
                {
                    // NO ACCOUNT FOUND
                    detail.Content.Add(GetNoAccountForm(profile));
                }
                else
                {
                    // DISPLAY PERMISSIONS FORM
                    detail.Content.Add(GetPermissionsForm(profile));
                }
            }

            ////////////////////
            // NEW PROXY FORM
            ////////////////////
            detail.Content.Add(new Divider());
            detail.Content.Add(GetRequestProxyForm());


            response.Content.Add(detail);
            response.Content.Add(
                new Divider
                {
                    BorderStyle = "none"
                });
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        private Form GetPermissionsForm(Person.Portal.Proxy.ProxyProfile profile)
        {
            Form form = new Form
            {
                RelativePath = "/UpdateProxyPermissions"
            };

            form.Items.Add(GetRelationshipHeading(profile));

            AddPermissionsInputs(form, profile);

            form.Items.Add
            (
                new ButtonContainer()
                {
                    Buttons = new List<IButton>()
                    {
                        new Button
                        {
                            Title = "Update Permissions",
                            Name = "UpdatePermissions",
                            ButtonType = "submit"
                        }
                    }
                }
            );

            return form;
        }

        private Form GetNoAccountForm(Person.Portal.Proxy.ProxyProfile profile)
        {
            Form form = new Form()
            {
                RelativePath = "/SendAccountCreationEmail"
            };

            form.Items.Add(GetRelationshipHeading(profile));

            form.Items.Add(GetNoAccountStatusMessage(profile));

            if (profile.ProxyUserProfile.EmailAddresses.Count > 0)
            {
                form.Items.Add(GetEmailAddressRadioButtons(profile));
                form.Items.Add
                (
                    new Label()
                    {
                        _Label = " ",
                        Value = "If the above address(es) are invalid please type a valid email address in the text " +
                        "box below:"
                    }
                );
            }
            else
            {
                form.Items.Add
                (
                    new Label()
                    {
                        _Label = " ",
                        Value = "Please type a valid email address in the text box below:"
                    }
                );
            }

            form.Items.Add
            (
                new FormInput.Text
                {
                    Name = "typedEmailAddress",
                    Label = "New Proxy Email:",
                    Required = false
                }
            );

            AddPermissionsInputs(form, profile);

            form.Items.Add
            (
                new ButtonContainer()
                {
                    Buttons = new List<IButton>()
                    {
                        new Button
                        {
                            Title = "Send Account Request",
                            Name = "SendAccountRequest",
                            ButtonType = "submit"
                        }
                    }
                }
            );

            return form;
        }

        private bool CheckPermissions(Person.Portal.Proxy.ProxyProfile profile, Person.Portal.Proxy.ProxyPermissions permission)
        {
            if (profile.ProxyPermissions != null)
            {
                return profile.ProxyPermissions.Contains(permission);
            }
            return false;
        }

        private void AddPermissionsInputs(Form form, Person.Portal.Proxy.ProxyProfile profile)
        {
            form.Items.Add
            (
                new Label
                {
                    _Label = " ",
                    Value = profile.ProxyUserProfile.FirstName + " can see the following on myOC:"
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "chapelView",
                    Label = "Chapel",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.ChapelView)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "scheduleView",
                    Label = "Grades",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.ScheduleView)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "financialAid",
                    Label = "Financial Aid",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.FinancialAid)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "taxDocuments",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.TaxDocuments)).ToString(), //Need to set the enum to tax info
                    Label = "Tax Documents (1098-T)"
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "financialAccount",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.FinancialAccount)).ToString(),
                    Label =
                        "Student Account <span style =\"font-style:italic;margin-left:10px;\">" +
                        profile.ProxyUserProfile.FirstName +
                        " will not be able to access your account until tomorrow.</span>"
                }
            );

            form.Items.Add
            (
                new Label
                {
                    _Label = " ",
                    Value =
                        profile.ProxyUserProfile.FirstName +
                        " can talk to OC employees in the following areas:"
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "facultyAndRegistrar",
                    Label = "Faculty and Registrar's Office",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.FacultyAndRegistrar)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "spiritualLife",
                    Label = "Spiritual Life",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.SpiritualLife)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "studentLife",
                    Label = "Student Life",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.StudentLife)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "studentFinancialServices",
                    Label = "Student Financial Services",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.StudentFinancialServices)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "careerServices",
                    Label = "Career Services (Day Six)",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.CareerServices)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Checkbox
                {
                    Name = "athletics",
                    Label = "Athletics",
                    Value = Convert.ToInt32(CheckPermissions(profile, Person.Portal.Proxy.ProxyPermissions.Athletics)).ToString()
                }
            );

            form.Items.Add
            (
                new FormInput.Hidden
                {
                    Name = "proxyId",
                    Value = profile.ProxyUserProfile.UserID
                }
            );

            form.Items.Add
            (
                new FormInput.Hidden
                {
                    Name = "relationship",
                    Value = profile.Relationship
                }
            );
        }

        private Label GetNoAccountStatusMessage(Person.Portal.Proxy.ProxyProfile profile)
        {

            string message = profile.AccountCreationEmail == null ?
                String.Format
                (
                    "{0} does not have an OC account.",
                    profile.ProxyUserProfile.FirstName
                ) :
                String.Format
                (
                    "An email has been sent to {0} at " +
                    "{1} but the account has not " +
                    "been created yet. If you would like, you can resend the email.",
                    profile.ProxyUserProfile.FirstName,
                    profile.AccountCreationEmail
                );

            return new Label
            {
                _Label = " ",
                Value = message
            };
        }

        private Heading GetRelationshipHeading(Person.Portal.Proxy.ProxyProfile profile)
        {
            string relationship = char.ToUpper(profile.Relationship[0]) +
                profile.Relationship.Substring(1).ToLower();

            return new Heading()
            {
                Title = String.Format("{0} ({1})", profile.ProxyUserProfile.DisplayName, relationship),
            };
        }

        private FormInput.Radio GetEmailAddressRadioButtons(Person.Portal.Proxy.ProxyProfile profile)
        {
            Dictionary<string, string> emailAddresses =
                new Dictionary<string, string>(profile.ProxyUserProfile.EmailAddresses.Count);
            foreach (Person.Portal.EmailAddress emailAddress in profile.ProxyUserProfile.EmailAddresses)
            {
                if (!emailAddresses.ContainsKey(emailAddress.Address))
                {
                    emailAddresses.Add(emailAddress.Address, emailAddress.Address);
                }
            }

            return new FormInput.Radio
            {
                Required = false,
                Name = "colleagueEmailAddress",
                Label = "<b>Please select their email address to send them an account creation request</b>",
                Options = emailAddresses

            };
        }

        private Form GetRequestProxyForm()
        {
            Form form = new Form();
            form.RelativePath = "/RequestProxy";

            form.Items.Add
            (
                new Heading() { Title = "Add A Person Not Listed Here" }
            );

            form.Items.Add
            (
                new HtmlFragment()
                {
                    Html = "If you would like to grant access to someone not listed above, please fill" +
                    " out the form below. We will setup this relationship and notify you so you can " +
                    "request an account for them."
                }
            );

            form.Items.Add
            (
                new FormInput.Text() { Name = "FirstName", Label = "First Name", Required = true }
            );

            form.Items.Add
            (
                new FormInput.Text() { Name = "LastName", Label = "Last Name", Required = true }
            );

            form.Items.Add
            (
                new FormInput.Text()
                {
                    Name = "Email",
                    Label = "Email Address (use OC address if available)",
                    Required = true,
                    Description = "(This is the email address they will use to sign in)"
                }
            );

            form.Items.Add
            (
                new FormInput.Text()
                {
                    Name = "StreetAddress",
                    Label = "Street Address",
                    Required = true,
                    Description = "(We use this to see if this person is already in our system. " +
                    "Names alone are not sufficient.)"
                }
            );

            form.Items.Add
            (
                new FormInput.Text() { Name = "City", Label = "City", Required = true }
            );

            form.Items.Add
            (
                new FormInput.Text() { Name = "State", Label = "State", Required = true }
            );

            form.Items.Add
            (
                new FormInput.Text() { Name = "Zip", Label = "Zip", Required = true }
            );

            List<string> relationshipTypes = new List<string>
            {
                "Parent",
                "Guardian",
                "Sibling",
                "Spouse",
                "Step-Parent",
                "Grandparent",
                "Friend",
                "Child",
                "Uncle",
                "Aunt",
                "Cousin",
                "In-law",
                "Other"
            };
            form.Items.Add
            (
                new FormInput.Select()
                {
                    Name = "Relationship",
                    Label = "Relationship to You",
                    OptionDescriptions = relationshipTypes,
                    Options = relationshipTypes.Zip(relationshipTypes, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v),
                    Required = true,
                }
            ); ;

            form.Items.Add
            (
                new ButtonContainer()
                {
                    Buttons = new List<IButton>()
                    {
                        new Button()
                        {
                            Title = "Add Proxy",
                            Name = "AddProxyButton",
                            ButtonType = "submit",
                        }
                    }
                }
            );

            return form;
        }
    }
}