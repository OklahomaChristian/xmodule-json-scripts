using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Person.Portal.ServiceCategory[] serviceCategories, Person.Portal.Service[] bookmarks, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(serviceCategories, bookmarks, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Person.Portal.ServiceCategory[] serviceCategories, Person.Portal.Service[] bookmarks, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = "QuickLinks";
            detail.Description = "Check all the items you would like to add/remove from your QuickLinks then click the nearest Save button. You do NOT have to click save after every section.";
            detail.HeaderMarginTop = "tight";
            Form form = null;
            XModComm.Forms.FormInput.Checkbox checkbox = new XModComm.Forms.FormInput.Checkbox();

            List backList = new List();
            backList.AddListItem("Return to QuickLinks List", null, null, new RelativeLink("/list"));
            detail.Content.Add(backList);

            // Create a new form for each category
            form = new Form();
            form.RelativePath = "";

            foreach (Person.Portal.ServiceCategory category in serviceCategories)
            {
                bool check = false;

                string categoryTitle = null;
                if (detail.Content.Count == 1)
                    categoryTitle = "<h2 style=\"margin-top:25px;color:#222222;\">" + category.Name.ToUpper() + "</h2>";
                else
                    categoryTitle = "<h2 style=\"color:#222222;\">" + category.Name.ToUpper() + "</h2>";

                ButtonContainer buttonContainer = new ButtonContainer();
                form.Items.Add(
                    new HtmlFragment()
                    {
                        Html = categoryTitle
                    }
                );

                foreach (Person.Portal.Service service in category.Services)
                {
                    check = false;
                    checkbox = new XModComm.Forms.FormInput.Checkbox();

                    //check to see if user has this as a quickLink
                    foreach (Person.Portal.Service bookmark in bookmarks)
                    {
                        if ((bookmark.ID == service.ID))
                        {
                            check = true;
                        }
                    }

                    checkbox.Name = "service_" + service.ID;
                    checkbox.Label = service.Title;
                    checkbox.Value = Convert.ToInt32(check).ToString();
                    form.Items.Add(checkbox);
                }
                Button button = new Button()
                {
                    Title = "Save",
                    Name = "1",
                    Value = "1"
                };
                buttonContainer.Buttons.Add(button);
                form.Items.Add(buttonContainer);
            }

            //add the form to the content list
            detail.Content.Add(form);


            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
