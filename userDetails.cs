using System;
using System.Globalization;
using Person.CampusInformation;
using Person.Portal;
using XModComm;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using XModComm.UI;
using XModComm.Links;
using System.Linq;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON
        (
            Person.Portal.UserProfile directoryProfile,
            Person.Portal.UserProfile portalUserProfile,
            List<Department> departments,
            Person.Portal.Proxy.ProxyProfile[] proxyProfiles,
            Person.Colleague.EmergencyContact[] emergencyContacts,
            Dictionary<string, string> buildingNames,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            TokenData tokenData,
            out Exception returnEx
        )
        {
            try
            {
                GetJSON(directoryProfile, portalUserProfile, departments, proxyProfiles, emergencyContacts, buildingNames, vehicleRegistrations, advisors, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public class FieldPermissions
        {
            public bool ShowEmail { get; private set; }
            public bool ShowDisplayName { get; private set; }
            public bool ShowTitle { get; private set; }
            public bool ShowUserId { get; private set; }
            public bool ShowClassification { get; private set; }
            public bool ShowMailbox { get; private set; }
            public bool ShowOfficePhone { get; private set; }
            public bool ShowOfficeBuilding { get; private set; }
            public bool ShowOfficeRoomNumber { get; private set; }
            public bool ShowHousingBuilding { get; private set; }
            public bool ShowHousingRoomNumber { get; private set; }
            public bool ShowHousingSuite { get; private set; }
            public bool ShowHomePhone { get; private set; }
            public bool ShowHomeLocation { get; private set; }
            public bool ShowEmergencyContact { get; private set; }
            public bool ShowFerpaPermissions { get; private set; }
            public bool ShowCliftonStrengths { get; private set; }
            public bool ShowAdvisors { get; private set; }
            public bool ShowGender { get; private set; }
            public bool ShowMajor { get; private set; }

            public FieldPermissions(Person.Portal.UserProfile userProfile, Person.Portal.UserProfile portalUser)
            {
                ShowEmail = true;
                ShowDisplayName = true;
                ShowTitle = false;
                ShowUserId = false;
                ShowClassification = true;
                ShowMailbox = true;
                ShowOfficePhone = true;
                ShowOfficeBuilding = true;
                ShowOfficeRoomNumber = true;
                ShowHousingBuilding = false;
                ShowHousingRoomNumber = false;
                ShowHousingSuite = false;
                ShowHomePhone = false;
                ShowHomeLocation = false;
                ShowEmergencyContact = false;
                ShowFerpaPermissions = false;
                ShowCliftonStrengths = false;
                ShowAdvisors = false;
                ShowGender = false;
                ShowMajor = false;

                if (portalUser.isEmployee)
                {
                    ShowTitle = true;
                    ShowUserId = true;
                    if (userProfile.isStudent && !userProfile.isEmployee)
                    {
                        ShowHousingBuilding = true;
                        ShowHousingRoomNumber = true;
                        ShowHousingSuite = true;
                        ShowHomePhone = true;
                        ShowHomeLocation = true;
                        ShowEmergencyContact = true;
                        ShowFerpaPermissions = true;
                        ShowGender = true;
                        ShowMajor = true;
                    }
                    ShowCliftonStrengths = true;
                    ShowAdvisors = true;
                }
                else if (portalUser.isStudent)
                {
                    if (userProfile.isEmployee)
                    {
                        ShowTitle = true;
                        ShowCliftonStrengths = true;
                    }
                }
                else if (portalUser.Roles.Contains(Person.UserType.UDining))
                {
                    ShowTitle = true;
                    ShowUserId = true;
                    ShowHousingBuilding = true;
                    ShowHousingRoomNumber = true;
                    ShowHousingSuite = true;
                }
            }
        }

        public string GetJSON
        (
            Person.Portal.UserProfile directoryProfile,
            Person.Portal.UserProfile portalUserProfile,
            List<Department> departments,
            Person.Portal.Proxy.ProxyProfile[] proxyProfiles,
            Person.Colleague.EmergencyContact[] emergencyContacts,
            Dictionary<string, string> buildingNames,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            TokenData tokenData
        )
        {
            XModComm.Response response = new Response();
            response.ContentContainerWidth = "narrow";
            response.Content.Add(new XModComm.UI.Divider
            {
                BorderStyle = "none"
            });

            Person.Portal.UserProfile userProfile = directoryProfile;

            Detail detail = new Detail();
            List contactList = new List();
            List departmentList = new List();
            List officeList = new List();
            List housingList = new List();
            List homeList = new List();
            List emergencyContactList = new List();
            List ferpaPermissionsList = new List();
            List noContentList = new List();
            List vehicleRegistrationList = new List();
            List advisorList = new List();
            List strengthList = new List();

            FieldPermissions permissions = new FieldPermissions(userProfile, portalUserProfile);

            // Detail
            Thumbnail thumbnail = new Thumbnail();
            thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
            thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;

            if (!String.IsNullOrEmpty(thumbnail.Url))
                detail.Thumbnail = thumbnail;

            detail.Title = userProfile.DisplayName;

            string title = null;
            string userId = null;
            string classification = null;
            string gender = null;
            List<string> majors = null;

            if (permissions.ShowTitle && userProfile.Titles.Count > 0)
            {
                for (int i = 0; i < userProfile.Titles.Count; i++)
                {
                    title += Regex.Replace(userProfile.Titles[i], @"\\", "");

                    if ((i + 1) < userProfile.Titles.Count)
                        title += "<br>";
                }
            }

            if (permissions.ShowUserId && !String.IsNullOrEmpty(userProfile.UserID))
                userId = userProfile.UserID;

            if (permissions.ShowMajor && userProfile.Majors[0] != null)
                majors = userProfile.Majors.ToList<string>();

            if (permissions.ShowGender && !String.IsNullOrEmpty(userProfile.Gender))
            {
                switch (userProfile.Gender)
                {
                    case "M":
                        gender = "Male";
                        break;
                    case "F":
                        gender = "Female";
                        break;
                    case "0":
                        gender = "Other";
                        break;
                    default:
                        gender = "Gender not provided";
                        break;
                }
            }

            if (permissions.ShowClassification && !String.IsNullOrEmpty(userProfile.Classification))
                classification = GetClassification(userProfile.Classification);

            if (permissions.ShowClassification && userProfile.isNewCollege)
                classification += "<br>New College Student";


            if (!String.IsNullOrEmpty(title) || !String.IsNullOrEmpty(userId) || !String.IsNullOrEmpty(classification))
            {
                detail.Description = "";

                if (!String.IsNullOrEmpty(gender))
                    detail.Description += gender;

                if (!String.IsNullOrEmpty(title))
                {
                    if (!String.IsNullOrEmpty(gender))
                        detail.Description += "<br>";
                    detail.Description += title;
                }

                if (!String.IsNullOrEmpty(classification))
                {
                    if (!String.IsNullOrEmpty(title) || !String.IsNullOrEmpty(gender))
                        detail.Description += "<br>";
                    detail.Description += classification;
                }

                if (majors != null)
                {
                    if (!String.IsNullOrEmpty(title) || !String.IsNullOrEmpty(gender) || !String.IsNullOrEmpty(classification))
                    {
                        foreach (string major in majors)
                        {
                            if (!String.IsNullOrEmpty(major))
                            {
                                detail.Description += "<br>";
                                detail.Description += major;
                            }
                        }
                    }
                }

                if (!String.IsNullOrEmpty(userId))
                {
                    if (!String.IsNullOrEmpty(classification) || !String.IsNullOrEmpty(title) || !String.IsNullOrEmpty(gender) || majors != null)
                        detail.Description += "<br>";
                    detail.Description += userId;
                }


            }

            contactList.Heading = "CONTACT";
            // Email
            if (permissions.ShowEmail && userProfile.EmailAddresses != null)
            {
                foreach (Person.Portal.EmailAddress email in userProfile.EmailAddresses)
                {
                    if (email.Type == "OC")
                    {
                        contactList.AddListItem(email.Address.ToString(), "Email", "", new ExternalLink("mailto:" + email.Address.ToString()));
                    }
                }
            }

            // Mailbox
            if (permissions.ShowMailbox && !String.IsNullOrEmpty(userProfile.MailBox))
            {
                contactList.AddListItem(userProfile.MailBox, "Mailbox", "");
            }

            // Department
            departmentList.Heading = "DEPARTMENT";
            if (departments != null && departments.Count > 0)
            {
                foreach (Person.CampusInformation.Department department in departments)
                {
                    departmentList.AddListItem(department.Name, "Department", "", new XModuleLink("departments", "/" + department.ID));
                }
            }

            // VehicleRegistration
            vehicleRegistrationList.Heading = "VEHICLE REGISTRATIONS";
            if (vehicleRegistrations != null)
            {
                foreach (var registration in vehicleRegistrations)
                {
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Permit Id", Title = registration.PermitId });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Permit Type", Title = registration.PermitType });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Make", Title = registration.VehicleMake });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Model", Title = registration.VehicleModel });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Color", Title = registration.VehicleColor });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Plate", Title = registration.Plate });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "State", Title = registration.PlateState });
                    vehicleRegistrationList.Items.Add(new Item() { Label = "Year", Title = registration.VehicleYear });
                }
            }

            // Office
            officeList.Heading = "Office";
            officeList.Heading = officeList.Heading.ToUpper();
            if (permissions.ShowOfficePhone && !String.IsNullOrEmpty(userProfile.OfficePhone))
            {
                string phoneNumber = Regex.Replace(userProfile.OfficePhone,
                                        @"^\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*$",
                                        "($1$2$3) $4$5$6-$7$8$9$10");
                officeList.AddListItem(phoneNumber, "Office Phone", "", new ExternalLink("tel:" + Regex.Replace(phoneNumber, "[^0-9_]", "")));
            }

            if (permissions.ShowOfficeBuilding && !String.IsNullOrEmpty(userProfile.OfficeBuilding))
            {
                Dictionary<string, string> queryParameters = new Dictionary<string, string>(2);
                string mapBuildingName = "";
                buildingNames.TryGetValue(userProfile.OfficeBuildingCode, out mapBuildingName);

                if (!String.IsNullOrEmpty(mapBuildingName))
                {
                    queryParameters.Add("filter", mapBuildingName);
                    queryParameters.Add("_recenter", "true");
                    officeList.AddListItem(mapBuildingName, "Office Building", "", new ModuleLink("map", "index", queryParameters));
                }
                else
                {
                    officeList.AddListItem(userProfile.OfficeBuilding, "Office Building", "");
                }
            }

            if (permissions.ShowOfficeBuilding && !String.IsNullOrEmpty(userProfile.OfficeBuildingCode))
                officeList.AddListItem(userProfile.OfficeBuildingCode, "Office Building Code", "");

            if (permissions.ShowOfficeRoomNumber && !String.IsNullOrEmpty(userProfile.OfficeRoomNumber))
                officeList.AddListItem(userProfile.OfficeRoomNumber, "Office Room Number", "");

            // Housing
            housingList.Heading = "Housing";
            housingList.Heading = housingList.Heading.ToUpper();
            if (permissions.ShowHousingBuilding && !String.IsNullOrEmpty(userProfile.HousingBuilding))
            {
                if (userProfile.HousingBuilding != "OFF CAMPUS")
                {
                    Dictionary<string, string> queryParameters = new Dictionary<string, string>(2);
                    queryParameters.Add("filter", userProfile.HousingBuilding);
                    queryParameters.Add("_recenter", "true");
                    housingList.AddListItem(userProfile.HousingBuilding, "Housing Building", "", new ModuleLink("map", "index", queryParameters));
                }
                else
                    housingList.AddListItem(userProfile.HousingBuilding, "Housing Building", "");
            }

            if (permissions.ShowHousingRoomNumber && !String.IsNullOrEmpty(userProfile.HousingRoomNumber))
            {
                housingList.AddListItem(userProfile.HousingRoomNumber, "Housing Room Number", "");
            }

            if (permissions.ShowHousingSuite && !String.IsNullOrEmpty(userProfile.HousingSuite))
            {
                housingList.AddListItem(userProfile.HousingSuite, "Housing Suite", "");
            }

            // Home
            homeList.Heading = "Home";
            homeList.Heading = homeList.Heading.ToUpper();
            if (permissions.ShowHomeLocation && userProfile.Addresses != null)
            {
                foreach (Person.Portal.Address address in userProfile.Addresses)
                {
                    if (address.Type.ToString() == "Preferred" && !String.IsNullOrEmpty(address.City) && !String.IsNullOrEmpty(address.State))
                    {
                        homeList.AddListItem(String.Join(" | ", address.AddressLines.Lines) + " " + address.City + ", " + address.State + " " + address.Zip, "Home Address", "");
                    }
                    else if (address.Type.ToString() == "Preferred" && !String.IsNullOrEmpty(address.State))
                    {
                        homeList.AddListItem(address.State, "Home Address", "");
                    }
                }
            }

            // Emergency Contacts
            emergencyContactList.Heading = "EMERGENCY CONTACTS";
            if (permissions.ShowEmergencyContact)
            {
                if (emergencyContacts != null)
                {
                    emergencyContactList.AddListItem("Contact Campus Police First @ (405) 425-5500", "", "", new ExternalLink("tel:4054255500"));

                    foreach (Person.Colleague.EmergencyContact emergencyContact in emergencyContacts)
                    {
                        string phoneNumber = Regex.Replace(emergencyContact.EMER_DAYTIME_PHONE,
                                                @"^\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*$",
                                                "($1$2$3) $4$5$6-$7$8$9$10");

                        string name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emergencyContact.EMER_NAME.ToLower());
                        string relationship = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(emergencyContact.EMER_RELATIONSHIP.ToLower());
                        emergencyContactList.AddListItem(name, relationship, "Phone: " + phoneNumber, new ExternalLink("tel:" + Regex.Replace(phoneNumber, "[^0-9_]", "")));
                    }
                }
            }

            // FERPA Permissions
            ferpaPermissionsList.Heading = "FERPA Permissions";
            ferpaPermissionsList.Heading = ferpaPermissionsList.Heading.ToUpper();
            if (permissions.ShowFerpaPermissions)
            {
                if (proxyProfiles.Length == 0 || proxyProfiles == null)
                {
                    ferpaPermissionsList.AddListItem(userProfile.DisplayName + " does not allow anyone to talk to OC employees.", "", "");
                }
                else
                {
                    foreach (Proxy.ProxyProfile proxy in proxyProfiles)
                    {
                        string ferpaPermissions = null;

                        if (proxy.ProxyPermissions != null && proxy.ProxyPermissions.Length > 0)
                        {
                            foreach (Person.Portal.Proxy.ProxyPermissions proxyPermissions in proxy.ProxyPermissions)
                            {
                                string ferpaText = GetFerpa(proxyPermissions.ToString());

                                if (ferpaPermissions != null)
                                    ferpaPermissions += "  |  ";

                                if (ferpaText != null)
                                    ferpaPermissions += ferpaText;
                            }

                            if (ferpaPermissions != null)
                            {
                                string relationship = proxy.Relationship.ToLower();
                                ferpaPermissionsList.AddListItem
                                (
                                    proxy.ProxyUserProfile.DisplayName,
                                    char.ToUpper(relationship[0]) + relationship.Substring(1),
                                    ferpaPermissions
                                );
                            }
                        }
                    }

                    if (ferpaPermissionsList.Items.Count == 0)
                    {
                        ferpaPermissionsList.AddListItem
                            (userProfile.DisplayName + " does not allow anyone to talk to OC employees.", "", "");
                    }
                    else
                    {
                        ferpaPermissionsList.Items.Insert(0,
                            new Item()
                            {
                                Title = userProfile.DisplayName + " gives permission for the following people to " +
                                "talk ONLY to OC employees designated in the listed offices:",
                                Label = "",
                                Description = ""
                            }
                        );
                    }
                }
            }

            //Advisors
            if (permissions.ShowAdvisors)
            {
                advisorList.Heading = "ADVISORS";

                if (advisors != null && advisors.Count > 0)
                {
                    foreach (KeyValuePair<string, Person.Portal.UserProfile> kvp in advisors)
                    {
                        Person.Portal.UserProfile advisorProfile = kvp.Value;

                        Thumbnail adThumbnail = new Thumbnail();
                        adThumbnail.Url = advisorProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(advisorProfile.UserID, advisorProfile.PicIDURL) : String.Empty;
                        adThumbnail.Alt = advisorProfile.DisplayName != null ? advisorProfile.DisplayName : String.Empty;
                        //adThumbnail.MaxHeight = 80;
                        //adThumbnail.MaxWidth = 80;

                        string adTitle = "";
                        switch (kvp.Key)
                        {
                            case "PFC":
                                adTitle = "Personal Financial Counselor";
                                break;
                            case "Academic":
                                adTitle = "Academic Advisor";
                                break;
                            case "Resident Director":
                                adTitle = "Resident Director";
                                break;
                            case "Mentor":
                                adTitle = "Program Mentor";
                                break;
                        }

                        advisorList.Items.Add
                        (
                            new XModComm.Item()
                            {
                                Title = adTitle,
                                Description = kvp.Value.DisplayName,
                                Link = new XModuleLink("userdetails", "/" + advisorProfile.IDToken),
                                Thumbnail = adThumbnail
                            }
                        );
                    }
                }
            }

            //CLIFTON STRENGTHS
            if (permissions.ShowCliftonStrengths)
            {
                if (userProfile.CliftonStrengths != null)
                {
                    strengthList.Heading = "CLIFTON STRENGTHS";

                    List<string> strengthsArray = new List<string>();
                    foreach (Person.Portal.CliftonStrength cliftonStrength in userProfile.CliftonStrengths)
                    {
                        if (cliftonStrength.strengthName != null)
                            strengthsArray.Add(cliftonStrength.strengthName);
                    }
                    strengthList.AddListItem("", String.Join(" | ", strengthsArray), "Click to see description of strengths", new ExternalLink("https://services.oc.edu/783"));
                }
            }

            // Add lists to detail content
            if (contactList.Items.Count > 0)
                detail.Content.Add(contactList);
            if (vehicleRegistrationList.Items.Count > 0)
                detail.Content.Add(vehicleRegistrationList);
            if (departmentList.Items.Count > 0)
                detail.Content.Add(departmentList);
            if (officeList.Items.Count > 0)
                detail.Content.Add(officeList);
            if (housingList.Items.Count > 0)
                detail.Content.Add(housingList);
            if (homeList.Items.Count > 0)
                detail.Content.Add(homeList);
            if (emergencyContactList.Items.Count > 0)
                detail.Content.Add(emergencyContactList);
            if (ferpaPermissionsList.Items.Count > 0)
                detail.Content.Add(ferpaPermissionsList);
            if (advisorList.Items.Count > 0)
                detail.Content.Add(advisorList);
            if (strengthList.Items.Count > 0)
                detail.Content.Add(strengthList);


            if (detail.Content.Count == 0)
            {
                noContentList.AddListItem("No details available", null, null);
                detail.Content.Add(noContentList);
            }

            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public string GetClassification(string studentClassification)
        {
            switch (studentClassification)
            {
                case "FR":
                    return "Freshman";
                case "SO":
                    return "Sophomore";
                case "JR":
                    return "Junior";
                case "SR":
                    return "Senior";
                case "GR":
                    return "Graduate Student";
                default:
                    return "not found";
            }
        }

        public string GetFerpa(string ferpaPermission)
        {
            switch (ferpaPermission)
            {
                case "FacultyAndRegistrar":
                    return "Faculty and Registrar";
                case "SpiritualLife":
                    return "Spiritual Life";
                case "StudentLife":
                    return "Student Life";
                case "StudentFinancialServices":
                    return "Student Financial Services";
                case "CareerServices":
                    return "Career Services (Day Six)";
                default:
                    return null;
            }
        }

        public string GetMapBuildingName(string buildingCode)
        {
            switch (buildingCode)
            {
                case "BH":
                    return "Benson Hall";
                case "BRN":
                    return "Dave Smith Athletic Center";
                case "CAH":
                    return "Cogswell-Alexander Hall";
                case "DAH":
                    return "Davisson American Heritage";
                case "DF":
                    return "Dobson Baseball Field";
                case "DHE":
                case "RDH":
                    return "Honors House at Davisson East";
                case "DHW":
                    return "Honors House at Davisson West";
                case "ES":
                    return "Enterprise Square";
                case "FH":
                    return "Fails Hall";
                case "GC":
                case "JT":
                case "KFA":
                case "LA":
                case "HAUD":
                case "MC":
                    return "Garvey Center";
                case "GH":
                    return "Gaylord Hall";
                case "GHEH":
                case "GHS":
                    return "Gunn-Henderson Hall";
                case "GHWH":
                    return "Gunn-Henderson West Hall";
                case "HBC":
                    return "Harvey Business Center";
                case "HHA3":
                    return "University Village, Phase 3";
                case "HHA4":
                    return "University Village, Phase 4";
                case "HHA5":
                    return "McNally House";
                case "HP":
                    return "Heritage Plaza";
                case "HSH":
                    return "Herold Science Hall";
                case "HV":
                    return "Heritage Village Offices";
                case "LC":
                    return "Mabee Learning Center";
                case "LSTF":
                case "WS":
                    return "Lawson Softball Complex";
                case "MIT":
                case "MURC":
                    return "Bobby Murcer Indoor Facility";
                case "MNT":
                    return "Physical Plant Services";
                case "NC":
                    return "Nowlin Center";
                case "NSW":
                    return "Noble Science Wing";
                case "NURS":
                    return "Heritage Plaza";
                case "PAC":
                case "GYM":
                case "PPEB":
                case "REC":
                case "FC":
                    return "Payne Athletic Center";
                case "PEC":
                    return "Prince Engineering Center";
                case "SC":
                    return "Gaylord University Center";
                case "TEH":
                    return "Tinius East Hall";
                case "TGF":
                    return "Thelma Gaylord Forum";
                case "TMR":
                case "TRC":
                    return "Teal Ridge Apartments";
                case "TWH":
                    return "Tinius West Hall";
                case "UA6A":
                    return "University Village, Phase 6, Building A";
                case "UA6B":
                    return "University Village, Phase 6, Building B";
                case "UA6C":
                case "HHA6":
                    return "University Village, Phase 6, Building C";
                case "UA6D":
                    return "University Village, Phase 6, Building D";
                case "UA6E":
                    return "University Village, Phase 6, Building E";
                case "UHN":
                    return "University House North";
                case "UHS":
                    return "University House South";
                case "VH":
                    return "Vose Hall";
                case "VT":
                    return "Vaughn Track";
                case "BSC":
                case "WBC":
                case "SCTT":
                    return "Williams-Branch Center for Biblical Studies";
                case "WEH":
                    return "Wilson East Hall";
                case "WH":
                    return "Warlick Hall";
                case "WWH":
                    return "Wilson West Hall";
                default:
                    return null;
            }
        }
    }
}
