using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Collections.Generic;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            TokenData tokenData,
            out Exception returnEx
        )
        {
            try
            {
                GetJSON(userProfile, printBalance, mealPlan, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            TokenData tokenData
        )
        {
            XModComm.Response response = new Response();

            XModComm.UI.Container.Container container = new XModComm.UI.Container.Container
            {
                WrapperStyle = "subfocal",
                BorderRadius = "tight",
                Padding = "medium",
                MarginTop = "medium",
            };

            XModComm.UI.Grid.Grid grid = new XModComm.UI.Grid.Grid
            {
                GridStyle = "springboard",
                GridDensity = "medium",
                HorizontalAlignment = "center",
                ImageSize = "xxsmall",
                TitleFontSize = "0.875rem",
                TitleTextColor = "theme:primary_text_color",
                DescriptionFontSize = "0.875rem",
                DescriptionTextColor = "theme:primary_text_color",
                MarginTop = "none",
                MarginBottom = "none",
                Items = new List<XModComm.UI.Grid.GridItem> { }
            };

            //Eagle Bucks
            XModComm.UI.Grid.GridItem eagleBucks = new XModComm.UI.Grid.GridItem
            {
                Title = "Eagle Bucks",
                Image = new XModComm.UI.Grid.Image
                {
                    URL = "https://static.modolabs.com/xmodule/iconsets/stroke-shaded-black/90/creditcard.png"
                },
                Link = new XModComm.StandardLinks.ExternalLink
                {
                    External = "https://services.oc.edu/redirect/521"
                },
                Description = "$" + mealPlan.MealPoints.ToString()
            };
            grid.Items.Add(eagleBucks);

            //Meals
            string mealCount = "No Meal Plan";
            if (!String.IsNullOrEmpty(mealPlan.MealBoard) && mealPlan.MealBoard != "0.00")
            {
                // Check homePage as well
                string mealsRemaining = mealPlan.MealBoard;
                if (mealPlan.MealPlanName == "All Access 7")
                    mealCount = "All-Access 7 days";
                else if (mealPlan.MealPlanName == "All Access 5")
                    mealCount = "All-Access Weekdays";
                else
                    mealCount = mealsRemaining + " meals";
            }
            XModComm.UI.Grid.GridItem meals = new XModComm.UI.Grid.GridItem
            {
                Title = "Meal Plan",
                Image = new XModComm.UI.Grid.Image
                {
                    URL = "https://static.modolabs.com/xmodule/iconsets/stroke-shaded-black/90/dining.png"
                },
                Link = new XModComm.StandardLinks.ExternalLink
                {
                    External = "https://services.oc.edu/redirect/520"
                },
                Description = mealCount
            };
            grid.Items.Add(meals);

            // uniFlow
            if (userProfile.PrimaryRole == Person.UserType.Student)
            {
                if (!String.IsNullOrEmpty(printBalance))
                {
                    decimal balance = System.Convert.ToDecimal(printBalance);
                    balance = decimal.Round(balance, 2, MidpointRounding.AwayFromZero);
                    XModComm.UI.Grid.GridItem printing = new XModComm.UI.Grid.GridItem
                    {
                        Title = "Print Balance",
                        Image = new XModComm.UI.Grid.Image
                        {
                            URL = "https://static.modolabs.com/xmodule/iconsets/stroke-shaded-black/90/registrar.png"
                        },
                        Link = new XModComm.StandardLinks.ExternalLink
                        {
                            External = "https://studentprinters.oc.edu"
                        },
                        Description = "$" + balance.ToString("#.##")
                    };
                    grid.Items.Add(printing);
                }
            }

            container.Content = new List<object>
            {
                grid
            };

            response.Content.Add(container);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}