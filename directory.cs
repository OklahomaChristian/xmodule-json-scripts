using System;
using Person;
using XModComm;
using System.Text;
using System.Text.RegularExpressions;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(System.Collections.Generic.List<object> objectList, string directoryString, TokenData tokenData)
        {
            XModComm.Response response = new Response();
		response.Content.Add(new XModComm.UI.Divider
            {
                BorderStyle = "none"
            });

            if (objectList == null)
                objectList = new System.Collections.Generic.List<object>();

            Detail detail = new Detail();
            detail.Title = "Directory";
            List list = new List();
            //list.Heading = "Directory";

            Form form = new Form();

            XModComm.Forms.FormInput.Text textBox = new XModComm.Forms.FormInput.Text();
            textBox.Name = "SearchString";
            textBox.Label = "Directory Search";
            textBox.Value = directoryString;
            textBox.Required = true;
            form.Items.Add(textBox);

            ButtonContainer buttonContainer = new ButtonContainer();
            Button button = new Button()
            {
                Title = "Search",
                Name = "Submit",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);
            form.Items.Add(buttonContainer);
            detail.Content.Add(form);

            foreach (object obj in objectList)
            {
                string title = null;

                Person.Portal.UserProfile userProfile = obj as Person.Portal.UserProfile;

                if (userProfile == null)
                {
                    continue;
                }

                if (userProfile.PrimaryRole.ToString() == "Staff" || userProfile.PrimaryRole.ToString() == "Faculty")
                {
                    for (int i = 0; i < userProfile.Titles.Count; i++)
                    {
                        title += Regex.Replace(userProfile.Titles[i], @"\\", "");

                        if ((i + 1) < userProfile.Titles.Count)
                            title += "; ";
                    }
                }
                else
                {
                    title = GetClassification(userProfile.Classification);
                }

                Thumbnail thumbnail = new Thumbnail();
                thumbnail.Url = userProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL) : String.Empty;
                thumbnail.Alt = userProfile.DisplayName != null ? userProfile.DisplayName : String.Empty;

                if (!String.IsNullOrEmpty(thumbnail.Url))
                    list.AddListItem(userProfile.DisplayName, null, title, new XModuleLink("userdetails", "/" + userProfile.IDToken), thumbnail);
                else
                    list.AddListItem(userProfile.DisplayName, null, title, new XModuleLink("userdetails", "/" + userProfile.IDToken));
            }

            detail.Content.Add(list);
            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public string GetClassification(string studentClassification)
        {
            switch (studentClassification)
            {
                case "FR":
                    return "Freshman";
                case "SO":
                    return "Sophomore";
                case "JR":
                    return "Junior";
                case "SR":
                    return "Senior";
                case "GR":
                    return "Graduate Student";
                default:
                    return "not found";
            }
        }
    }
}
