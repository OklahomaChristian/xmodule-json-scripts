using System;
using System.Linq;
using Person.Portal;
using Person.Colleague.Schedule;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Text;
using System.Collections.Generic;

namespace Dynamic
{
    public class Script
    {
        public class MeetingTimesFormater
        {
            public bool ClassTimesAreAvailable
            {
                get
                {
                    return _meetingTime.Days != null;
                }
            }

            public bool FinalsTimesAreAvailable
            {
                get
                {
                    return _meetingTime.FinalsTimes != null;
                }
            }

            private ClassSchedule.MeetingTime _meetingTime = null;

            public MeetingTimesFormater(ClassSchedule.MeetingTime meetingTime)
            {
                this._meetingTime = meetingTime;
            }

            public Item GetClassTimes()
            {
                if (_meetingTime.Days == null)
                    return null;

                Item item = new Item
                {
                    Title = String.Format
                            (
                                "{0} {1}-{2} ({3} / {4})",
                                String.Join(" ", _meetingTime.Days),
                                _meetingTime.StartTime,
                                _meetingTime.EndTime,
                                _meetingTime.Building.Code ?? "-",
                                _meetingTime.Room ?? "-"
                            ),
                    Label = "Times/Dates",
                    Description = String.Format("{0}-{1}", _meetingTime.StartDate, _meetingTime.EndDate)
                };

                return item;
            }

            public Item[] GetFinalsTimes()
            {
                if (_meetingTime.FinalsTimes == null)
                    return null;

                System.Collections.Generic.List<Item> itemList = new System.Collections.Generic.List<Item>(_meetingTime.FinalsTimes.Count());
                foreach (ClassSchedule.MeetingTime meetingTime in _meetingTime.FinalsTimes)
                {
                    if (meetingTime.Days == null)
                        continue;

                    itemList.Add
                    (
                        new Item()
                        {
                            Title = String.Format
                            (
                                "{0} {1}-{2}",
                                String.Join(" ", meetingTime.Days),
                                meetingTime.StartTime,
                                meetingTime.EndTime
                            ),
                            Label = "Final Exam Time"
                        }
                    );
                }

                return itemList.ToArray();
            }
        }

        public class EvaluationArguments
        {
            public ClassSchedule.EvaluationDateTime[] EvaluationDateTimeList { get; set; }
            public string WebAdvisorToken { get; set; }
            public string CourseSectionId { get; set; }
            public string Term { get; set; }
            public string PageType { get; set; }
        }

        public class FacultyTable
        {
            private StringBuilder html = new StringBuilder(1024);
            private System.Collections.Generic.List<string> processedUserIds = new System.Collections.Generic.List<string>();
            private EvaluationArguments _evaluationArguments = null;

            private bool EvaluationsAreAvailable
            {
                get
                {
                    return _evaluationArguments != null;
                }
            }

            public FacultyTable(EvaluationArguments evaluationArguments)
            {
                html.AppendLine("<table style='margin-bottom:0px;'>");

                if (evaluationArguments != null)
                {
                    _evaluationArguments = evaluationArguments;
                }
            }

            public void AddFaculty(UserProfile userProfile)
            {
                if (processedUserIds.Contains(userProfile.UserID))
                    return;
                processedUserIds.Add(userProfile.UserID);

                // ADD PHOTO
                string photoURL = XModComm.Utilities.GetUserPhotoURL(userProfile.UserID, userProfile.PicIDURL);
                html.Append("<tr><td style='border:0px;width:100px;'><img src='").Append(photoURL).AppendLine("'></td>");

                // ADD NAME LINK
                html.Append("<td style='border:0px;vertical-align:top;'><h2><a href='../userdetails/page?relativePath=%2F");
                html.Append(userProfile.IDToken).Append("'>").Append(userProfile.DisplayName).AppendLine("</a></h2>");

                // ADD EVALUATIONS
                if (EvaluationsAreAvailable)
                {
                    if (_evaluationArguments.PageType == "small")
                    {
                        html.Append("Evaluations are open but not available on mobile.");
                    }
                    else
                    {
                        string link = GetEvaluationLink(userProfile.UserID);
                        if (!String.IsNullOrEmpty(link))
                        {
                            html.Append("<a href='").Append(link).Append("' target='_blank'>Complete Instructor Evaluation</a>");
                        }
                    }
                }

                html.AppendLine("</td></tr>");
            }

            private string GetEvaluationLink(string userId)
            {
                foreach (ClassSchedule.EvaluationDateTime evaluation in _evaluationArguments.EvaluationDateTimeList)
                {
                    // blank userId means generic evaluation link; not tied to faculty header
                    if (!String.IsNullOrEmpty(userId) && evaluation.FacultyID != userId)
                        continue;

                    if (!evaluation.IsScheduledEvaluationOpen && evaluation.StatusType != ClassSchedule.EvaluationDateTime.EvaluationStatusType.Open)
                        continue;

                    if (evaluation.AlreadyEvaluated)
                        continue;

                    return String.Format
                    (
                        "https://webadvisor.oc.edu/wwiz/wwiz.exe/wwiz_asp?wwizmstr=WEB.EVAL.SURVEY&STORAGE_TOKEN={0}&section_id={1}&faculty_id={2}&scs_id={3}&term={4}&location=MAIN",
                        _evaluationArguments.WebAdvisorToken,
                        _evaluationArguments.CourseSectionId,
                        evaluation.FacultyID,
                        evaluation.STC_STUDENT_COURSE_SEC,
                        _evaluationArguments.Term
                    );
                }

                return null;
            }

            public override string ToString()
            {
                html.AppendLine("</table>");
                return html.ToString();
            }
        }

        public class FacultyFormater
        {
            private UserProfile _userProfile = null;

            public FacultyFormater(UserProfile userProfile)
            {
                _userProfile = userProfile;
            }

            public string GetFacultyRow()
            {
                return "";
            }

        }

        public bool TestJSON(ClassSchedule classSchedule, string webAdvisorToken, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(classSchedule, webAdvisorToken, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(ClassSchedule classSchedule, string webAdvisorToken, TokenData tokenData)
        {
            XModComm.Response response = new Response();
	response.Content.Add(new XModComm.UI.Divider
            {
                BorderStyle = "none"
            });
            Detail detail = new Detail();

            if (classSchedule == null || classSchedule.Sections == null || classSchedule.Sections[0] == null)
            {
                List list = new List();
                list.AddListItem("Class details not available", null, null);
                detail.Content.Add(list);
                response.Content.Add(detail);
                return Person.Utilities.JSON.Serializer.Serialize(response);
            }

            ClassSchedule.Section section = classSchedule.Sections[0];

            detail.Title = section.ShortTitle.ToUpper();
            detail.Description = section.Name.ToUpper();

            /////////////////////
            // FACULTY
            /////////////////////

            // GET FACULTY PROFILES
            System.Collections.Generic.List<UserProfile> facultyProfiles = new System.Collections.Generic.List<UserProfile>();

            if (section.FacultyProfiles != null)
                section.FacultyProfiles.ForEach(f => { facultyProfiles.Add(f); });

            // REMOVE DUPLICATE PROFILES
            // TODO: is this needed now?
            facultyProfiles = facultyProfiles.GroupBy(o => o.UserID).Select(g => g.First()).ToList();

            // EVALUATION ARGUMENTS
            EvaluationArguments evaluationArguments = null;
            if (section.EvaluationDateTimes != null)
            {
                evaluationArguments = new EvaluationArguments()
                {
                    EvaluationDateTimeList = section.EvaluationDateTimes,
                    WebAdvisorToken = webAdvisorToken,
                    CourseSectionId = section.CourseSectionId,
                    Term = classSchedule.Term,
                    PageType = tokenData.PageType
                };
            }

            FacultyTable facultyTable = new FacultyTable(evaluationArguments);
            facultyProfiles.ForEach(profile => { facultyTable.AddFaculty(profile); });

            detail.Content.Add(new HtmlFragment { Html = facultyTable.ToString() });

            // EVALUATION LINK FOR 'MISSING' FACULTY (e.g. online courses don't have meeting times and therefore no faculty.
            // They may need to be evaluated, however)
            if (section.EvaluationDateTimes != null)
            {
                foreach (ClassSchedule.EvaluationDateTime evaluation in section.EvaluationDateTimes)
                {
                    bool facultyMissing = true;
                    foreach (UserProfile faculty in facultyProfiles)
                    {
                        if (faculty.UserID == evaluation.FacultyID)
                        {
                            facultyMissing = false;
                            continue;
                        }
                    }

                    if (facultyMissing)
                    {
                        if (!evaluation.IsScheduledEvaluationOpen && evaluation.StatusType != ClassSchedule.EvaluationDateTime.EvaluationStatusType.Open)
                            continue;

                        if (evaluation.AlreadyEvaluated)
                            continue;

                        Person.Utilities.Email.SendFromServer("luther.hartman@oc.edu", "portal@oc.edu", "Evaluation link needed for " + section.Name, "Glad we added this!", false);


                        string link = String.Format
                        (
                            "https://webadvisor.oc.edu/wwiz/wwiz.exe/wwiz_asp?wwizmstr=WEB.EVAL.SURVEY&STORAGE_TOKEN={0}&section_id={1}&faculty_id={2}&scs_id={3}&term={4}&location=MAIN",
                            webAdvisorToken,
                            section.CourseSectionId,
                            evaluation.FacultyID,
                            evaluation.STC_STUDENT_COURSE_SEC,
                            classSchedule.Term
                        );

                        StringBuilder html = new StringBuilder(1024);
                        html.Append("<a href='").Append(link).Append("' target='_blank'>Complete Course Evaluation</a>");

                        detail.Content.Add(new HtmlFragment { Html = html.ToString() });
                    }

                }
            }
            ///////////////////////
            // MEETING TIMES
            ///////////////////////

            if (section.MeetingTimes != null)
            {
                List meetingTimesList = new List();
                meetingTimesList.Heading = "MEETING TIMES";

                foreach (var meetingTime in section.MeetingTimes)
                {
                    MeetingTimesFormater meetingTimesFormater = new MeetingTimesFormater(meetingTime);

                    // CLASS TIMES
                    if (meetingTimesFormater.ClassTimesAreAvailable)
                        meetingTimesList.Items.Add(meetingTimesFormater.GetClassTimes());

                    // FINALS TIMES
                    if (meetingTimesFormater.FinalsTimesAreAvailable)
                        meetingTimesList.Items.AddRange(meetingTimesFormater.GetFinalsTimes());
                }

                detail.Content.Add(meetingTimesList);
            }

            ///////////////////////
            // STUDENT ROSTER
            ///////////////////////
            ///

            CardSet cardSet = new CardSet()
            {
                MarginTop = "none",
                MarginBottom = "none",
                Items = new List<object> { }
            };

            if (section.Roster != null)
            {
                //List list = new List();
                //list.Heading = "STUDENTS";

                foreach (UserProfile student in section.Roster)
                {
                    ContentCard studentCard = new ContentCard
                    {
                        Size = "small",
                        ImageStyle = "hero",
                        Image = new Image
                        {
                            Url = student.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(student.UserID, student.PicIDURL, 480) : String.Empty,
                            Alt = student.DisplayName != null ? student.DisplayName : String.Empty
                        }
                    };
                    studentCard.Title = student.DisplayName;
                    studentCard.Link = new XModComm.StandardLinks.XModuleLink
                    {
                        XModule = new XModComm.StandardLinks.XModuleObject
                        {
                            ID = "userdetails",
                            RelativePath = "/" + student.GetIDToken()
                        }
                    };

                    cardSet.Items.Add(studentCard);
                    //list.Items.Add
                    //(
                    //    new Item()
                    //    {
                    //        Title = student.DisplayName,
                    //        Link = new XModuleLink("userdetails", "/" + student.GetIDToken()),
                    //        Thumbnail = new Thumbnail
                    //        {
                    //            Url = String.IsNullOrEmpty(student.PicIDURL) ? String.Empty : XModComm.Utilities.GetUserPhotoURL(student.UserID, student.PicIDURL),
                    //            Alt = String.IsNullOrEmpty(student.DisplayName) ? String.Empty : student.DisplayName,
                    //            //MaxHeight = 80,
                    //            //MaxWidth = 80
                    //        }
                    //    }
                    //);
                }

                detail.Content.Add(cardSet);
            }

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}