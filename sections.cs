using System;
using System.Linq;
using XModComm;
using XModComm.UI;
using XModComm.Forms;
using XModComm.Tables;
using XModComm.Links;
using Person.Colleague;
using Person.Colleague.Schedule;
using ExtensionMethods;
using System.Collections.Generic;

namespace ExtensionMethods {
	public static class SectionExtensions {
		public static T GetSectionDetails<T>(this ClassSchedule.Section section, string term, string userId) where T : class {
			Type genericType = typeof(T);

			object obj = null;
			switch (genericType.Name) {
				case "List":
					obj = GetList(section, term, userId);
					break;
				case "Row":
					obj = GetRow(section, term, userId);
					break;
			}

			return (T)obj ?? null;
		}

		static Row GetRow(ClassSchedule.Section section, string term, string userId) // large
		{
			Row row = new Row();
			row.Url = new RelativeLink(String.Format("/SectionDetails/{0}/{1}", term, section.CourseSectionId));

			row.Cells.Add(new Cell() { Title = section.ShortTitle }); // Course Title
			row.Cells.Add(new Cell() { Title = section.Name }); // Course Description
			row.Cells.Add(new Cell() { Title = section.ActiveStudents.ToString() }); // Students
			row.Cells.Add(new Cell() { Title = section.Capacity.ToString() }); // Max
			row.Cells.Add(new Cell() { Title = section.WaitListedStudents != null ? section.WaitListedStudents.Length.ToString() : "0" }); // Wait

			var meetingTimeCells = new System.Collections.Generic.List<Cell>(5);
			for (int i = 0; i < 5; i++)
				meetingTimeCells.Add(new Cell());
			if (section.MeetingTimes != null) {
				foreach (Person.Colleague.Schedule.ClassSchedule.MeetingTime meetingTime in section.MeetingTimes) {
					//Room Number
					meetingTimeCells[0].Title += meetingTime.Building.Code == "TBA" ? "-" : String.Format("{0} {1}<br>", meetingTime.Building.Code, meetingTime.Room);

					//Days
					string days = "";
					if (meetingTime.Days != null) meetingTime.Days.ToList().ForEach((string day) => { days = String.Format("{0} {1}", days, day); });
					meetingTimeCells[1].Title += String.Format("{0}<br>", days);

					//Times
					meetingTimeCells[2].Title += String.Format("{0}-{1}<br>", meetingTime.StartTime, meetingTime.EndTime);

					//Dates
					meetingTimeCells[3].Title += String.Format("{0}-{1}<br>", meetingTime.StartDate, meetingTime.EndDate);
				}
			}
			meetingTimeCells.ForEach((Cell c) => { row.Cells.Add(c); });

			// Evaluations
			if (section.EvaluationDateTimes != null) {
				section.EvaluationDateTimes.ToList().ForEach((ClassSchedule.EvaluationDateTime time) => {
					if (time != null && time.FacultyID == userId) {
						row.Cells[9].Url = new XModComm.StandardLinks.ExternalLink { External = String.Format("https://webadvisor.oc.edu/wwiz/wwiz.exe/wwiz_asp?wwizmstr=WEB.EVAL.MANAGE&faculty_id={0}&term={1}&STORAGE_TOKEN={2}", userId, term, "") };
						row.Cells[9].Title = time.StatusType == ClassSchedule.EvaluationDateTime.EvaluationStatusType.NotOpen ? "Need Eval" : time.StatusType.ToString();
					}
				});
			}

			return row;
		}

		static XModComm.UI.List GetList(ClassSchedule.Section section, string term, string userId) // small
		{
			// TODO: NO LINK TO ClassDetails

			var list = new XModComm.UI.List();
			list.Heading = String.Format("{0} ({1})", section.ShortTitle, section.Name);

			string studentDesc = "Students/Max";
			string studentInfo = String.Format("{0}/{1}", section.ActiveStudents.ToString(), section.Capacity.ToString());

			if (section.WaitListedStudents != null) {
				studentDesc += String.Format("{0}/{1}", studentDesc, "Wait");
				studentInfo += String.Format("{0}/{1}", studentInfo, section.WaitListedStudents.Count().ToString());
			}

			list.AddListItem(studentInfo, "Number of Students", studentDesc);

			string times = "";
			string dates = "";
			if (section.MeetingTimes != null) {
				foreach (var meetingTime in section.MeetingTimes) {
					if (meetingTime.Days != null)
						meetingTime.Days.ToList().ForEach((string day) => { times = String.Format("{0} {1}", times, day); });
					times += String.Format(" {0}-{1} ({2} / {3})", meetingTime.StartTime, meetingTime.EndTime, meetingTime.Building.Code ?? "-", meetingTime.Room ?? "-");
					dates += String.Format("{0}-{1}", meetingTime.StartDate, meetingTime.EndDate);
				}
			}
			list.AddListItem(times, "Times/Dates", dates);
			list.AddListItem("Section Details", null, null, new RelativeLink(String.Format("/SectionDetails/{0}/{1}", term, section.CourseSectionId)));
			return list;
		}
	}
}


namespace Dynamic {
	public class Script {
		public bool TestJSON(Person.Colleague.Schedule.FacultySections facultySections, System.Collections.Generic.List<Term> termList, string term, TokenData tokenData, out Exception returnEx) {
			try {
				GetJSON(facultySections, termList, term, tokenData);
				returnEx = null;
			}
			catch (Exception ex) {
				returnEx = ex;
				return false;
			}
			return true;
		}

		public string GetJSON(Person.Colleague.Schedule.FacultySections facultySections, System.Collections.Generic.List<Term> termList, string term, TokenData tokenData) {
			XModComm.Response response = new Response();
			TabContainer tabContainer = new TabContainer();
			string userId = facultySections.UserID;

			if (termList.Count == 0)
				return Person.Utilities.JSON.Serializer.Serialize(response);


			if (tokenData.PageType == "small") {
				//Term sectionTerm = new Term(facultySections.Term, colleagueSession, client);
				Person.Colleague.Term[] terms = termList.Where(x => x.TermId == term).ToArray();
				string heading = term;
				if (terms.Count() > 0) {
					heading = terms[0].TERM_DESC + " Sections";
				}
				Detail detail = new Detail { Title = heading.ToUpper() };

				if (facultySections != null && facultySections.Sections != null) {
					facultySections.Sections.Distinct().ToList().ForEach
					(
						(ClassSchedule.Section section) => {
							detail.Content.Add(section.GetSectionDetails<XModComm.UI.List>(facultySections.Term, userId));
						}
					);
				}

				response.Content.Add(detail);
			}
			else // DEFAULT TO LARGE
			{
				//Term sectionTerm = new Term(facultySections.Term, colleagueSession, client);
				Person.Colleague.Term[] terms = termList.Where(x => x.TermId == term).ToArray();
				string heading = term;
				if (terms.Count() > 0) {
					heading = terms[0].TERM_DESC + " Sections";
				}
				Tab tab = new Tab() { Title = heading.ToUpper() };

				Table table = new Table(0, 0) { Heading = heading.ToUpper() };

				new string[]
				{
					"Course Title", "Course ID", "Students", "Max", "Wait", "Room", "Days", "Times", "Dates", "Evaluation"
				}.ToList().ForEach((string header) => { table.ColumnOptions.Add(new ColumnOption() { Header = header }); });

				if (facultySections != null && facultySections.Sections != null) {
					facultySections.Sections.Distinct().ToList().ForEach
					(
						(ClassSchedule.Section section) => {
							table.Rows.Add(section.GetSectionDetails<Row>(facultySections.Term, userId));
						}
					);
					
				}

				tab.Content.Add(table);
				tabContainer.Tabs.Add(tab);
				response.Content.Add(tabContainer);
			}

			if (termList.Count > 0) {
				termList = termList.Distinct().ToList();
				List<string> termListValues = Person.Utilities.TermSorter.Sort(termList.Select(t => t.TermId).ToArray()).ToList();

				termListValues.Reverse();

				XModComm.Forms.FormInput.Select termSelect = new XModComm.Forms.FormInput.Select();
				termSelect.Name = "term";
				termSelect.Label = "";
				termSelect.Value = term;
				Dictionary<string, string> optionList = new Dictionary<string, string> { };
				List<string> termDescList = termListValues.Select(x => GetTermDesc(x)).ToList();
				int i = 0;
				foreach (string termVal in termListValues) {
					optionList.Add(termVal, termDescList.ElementAt(i));
					i++;
				}

				termSelect.Options = optionList;
				termSelect.Width = "third";
				//termSelect.OptionDescriptions = terms;
				//termSelect.EmptyValue = terms;

				Form form = new Form();
				form.Items.Add(termSelect);  // TEST -> FormItem.FormInputType.Search
				ButtonContainer buttonContainer = new ButtonContainer();
				Button button = new Button() {
					Title = "Change Term",
					Name = "Submit",
					Value = "1"
				};
				buttonContainer.Buttons.Add(button);
				form.Items.Add(buttonContainer);
				response.ContentContainerWidth = "full";
				response.Content.Insert(0, form);
			}

			return Person.Utilities.JSON.Serializer.Serialize(response);
		}
		public static string GetTermDesc(string term) {
			if (term.Length != 6)
				return term;

			string year = term.Substring(0, 4);
			string semester = term.Substring(4, 2);

			switch (semester) {
				case "SP":
					return "Spring " + year;
				case "SU":
					return "Summer " + year;
				case "FA":
					return "Fall " + year;
				case "WI":
					return "Winter " + year;
			}

			return term;
		}
	}
}
