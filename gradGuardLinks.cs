using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
		public bool TestJSON(TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}
		
        public string GetJSON(TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = "Tuition Protection Information";
			detail.Body = "Tuition insurance can reimburse up to 100% of your school payments after an unexpected withdrawal for a covered illness, injury, mental health condition, and more. " +
				"Based on the average cost of attendance and fees, OC recommends to consider purchasing at least $10,000 in protection at the price of $125. Keep in mind that you can choose your coverage limit and reimbursement can include: " +
					"<ul><li>Tuition payments</li>" +
					"<li>Room and board</li>" +
					"<li>Other academic fees</li>" +
					"<li>The plan also includes Student Life Assistance, a 24-hour emergency hotline that helps students stay safe on and off campus - providing you and your family with even more peace of mind.</li></ul>" +
				"Select \"ACCEPT GradGuard\" to protect your college investment, and your contact and student information will be released to GradGuard to follow up and complete your purchase of the Tuition Protection Plan.<br><br>" + 
				"Select \"DECLINE GradGuard\" If you are not interested in the Tuition Protection Plan. By selecting this option, you acknowledge that you are accepting full responsibility for any payments that are not refunded to you under the college's published refund policy.";
            List gradGuardLinks = new List() { Grouped = true };
            gradGuardLinks.Heading = "<br>";

			gradGuardLinks.AddListItem("ACCEPT GradGuard", null, null, new XModuleLink("utilities", "/gradGuardStudent/add"));
			gradGuardLinks.AddListItem("DECLINE GradGuard", null, null, new XModuleLink("utilities", "/gradGuardStudent/decline"));
			gradGuardLinks.AddListItem("Decide Later", null, null, new XModuleLink("utilities", "/gradGuardStudent/defer"));

			detail.Content.Add(gradGuardLinks);

			List disclaimer = new List();
			disclaimer.AddListItem("*Terms, conditions, and exclusions (including for pre-existing conditions) apply. Plans only available to U.S. residents and may not be available in all jurisdictions. Recommended and provided by Grad Guard, a service of Next Generation Insurance Group, LLC (NGI), the licensed agent for all insurance programs. Insurance plans include insurance benefits and assistance services. Pricing may vary by state. Insurance benefits are underwritten by Jefferson Insurance Company (NY, Administrative Office 9950 Mayland Drive, Richmond, VA 23233) rated \"A+\" (Superior) by A.M. Best Co. Non-insurance benefits/services are provided by AGA Service Company. Claims are administered by Allianz Global Assistance (AGA). AGA and Allianz Tuition Insurance are marks of AGA Service Company or its affiliates. AGA Service Company and NGI are affiliates of Jefferson Insurance Company. Except as expressly provided for under the plan, consumer is responsible for charges incurred from outside vendors. Contact AGA Service Company at 888-427-5045 or 9950 Mayland Dr., Richmond, VA 23233.", "", "");
			detail.Content.Add(disclaimer);

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}