using System;
using Person;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(Ethos.UserProfile userProfile, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            Detail detail = new Detail();
            detail.Title = "Ethos Information for " + userProfile.DisplayName;

            List infoList = new List();

            infoList.Heading = "Semester overview";
            infoList.AddListItem("Kudos earned: " + userProfile.CurrentSemesterCheckinCount, null, null, new RelativeLink("/GetEthosHistory"));
            infoList.AddListItem("Kudos required: " + userProfile.RequiredKudos, null, null);
            string notice = null;
            if (userProfile.EstimatedProgress > userProfile.CurrentSemesterCheckinCount)
                notice = (userProfile.EstimatedProgress - userProfile.CurrentSemesterCheckinCount) + " Kudos behind";
            infoList.AddListItem("Kudos pace: " + userProfile.EstimatedProgress, notice, "Kudos you should have this far in the semester to reach your goal");

            detail.Content.Add(infoList);

            List linksList = new List();
            linksList.Heading = "Ethos Links";
            linksList.AddListItem("Upcoming Ethos Events", null, null, new RelativeLink("/GetEthosEvents"));
            linksList.AddListItem("Submit an Ethos Event", null, null, new ExternalLink("http://www.oc.edu/spiritual-life/ethos/ethos-event-approval-form.html"));
            linksList.AddListItem("Ethos Requirements and Guidelines", null, null, new ExternalLink("http://www.oc.edu/spiritual-life/ethos/event-requirements-and-guidelines.html"));

            detail.Content.Add(linksList);

            response.Content.Add(detail);
            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}