using System;
using System.Collections.Generic;
using Person;
using Person.Portal;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;
using System.Data.SqlClient;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Ethos.UserProfile userProfile, List<Ethos.UserProfile> dependentProfiles, Ethos.Event[] events, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(userProfile, dependentProfiles, events, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Ethos.UserProfile userProfile, List<Ethos.UserProfile> dependentProfiles, Ethos.Event[] events, TokenData tokenData = null)
        {
            try
            {
                XModComm.Response response = new Response();
                BlockHeading blockHeading = new BlockHeading();
                blockHeading.Heading = "Spiritual Life";
                blockHeading.HeadingTextColor = "#5c0e1d";
                Image ethosImage = new Image
                {
                    Url = "https://myocfiles.oc.edu/files/chapel/cross.png",
                    BorderRadius = "full",
                    BackgroundColor = "#0ca38b"
                };
                blockHeading.Image = ethosImage;
                blockHeading.ImageHeight = "2.25rem";
                blockHeading.ImageWidth = "2.25rem";
                Dropdown dropdown = new Dropdown
                {
                    ScreenReaderTitle = "List of actions",
                    ToggleSize = "small",
                    ToggleActionStyle = "normal",
                    ToggleBorderColor = "transparent",
                    AccessoryIcon = "overflow",
                    ShowDisclosureIcon = false,
                    ToggleTextColor = "theme:tertiary_text_color",
                    Items = new List<DropdownItem>
                    {
                        new DropdownItem
                        {
                            Title = "My Chapel information",
                            Url = new XModComm.StandardLinks.ModuleLink
                            {
                                Module = new XModComm.StandardLinks.ModuleObject
                                {
                                    ID = "new_ethos"
                                }
                            }
                        },
                        //new DropdownItem
                        //{
                        //    Title = "Submit an Ethos event",
                        //    Url = new XModComm.StandardLinks.ExternalLink
                        //    {
                        //        External = "https://services.oc.edu/redirect/686"
                        //    }
                        //},
                        new DropdownItem
                        {
                            Title = "Requirements and Guidelines",
                            Url = new XModComm.StandardLinks.ExternalLink
                            {
                                External = "https://www.oc.edu/student-life/spiritual-life"
                            }
                        }
                    }
                };
                blockHeading.Buttons = new List<object>()
                {
                    dropdown
                };
                blockHeading.MarginBottom = "tight";
                blockHeading.DescriptionLineClamp = 2;
                blockHeading.HeadingLineClamp = 2;

                XModComm.UI.Container.Container container = new XModComm.UI.Container.Container
                {
                    BorderRadius = "medium",
                    BackgroundColor = "#e8f5f9",
                    Padding = "medium"
                };
                int remainingKudos = (int)(userProfile.RequiredKudos - userProfile.CurrentSemesterCheckinCount);
                if (remainingKudos < 0)
                    remainingKudos = 0;
                Availability availability = new Availability
                {
                    AvailableNumber = (int)userProfile.CurrentSemesterCheckinCount,
                    AvailableLabel = "Credits earned",
                    AvailableColor = "#0ca38b",
                    OtherNumber = remainingKudos,
                    OtherLabel = "more required",
                    BusyNumber = null,
                    MarginTop = "none",
                    MarginBottom = "medium"
                };
                string htmlPace = "";
                if (userProfile.CurrentSemesterCheckinCount < userProfile.EstimatedProgress)
                    htmlPace = "<p style=\"color:red;\" class='kgo-secondary-text'>Watch Out! You're " + (userProfile.EstimatedProgress - userProfile.CurrentSemesterCheckinCount).ToString() + " credit(s) behind this term.</p>";
                else if (userProfile.CurrentSemesterCheckinCount >= userProfile.RequiredKudos)
                    htmlPace = "<p class='kgo-secondary-text'>Congrats! You've earned all of your required credits.</p>";
                else
                    htmlPace = "<p class='kgo-secondary-text'>Great job! You're on pace for this term.</p>";
                HtmlFragment pace = new HtmlFragment
                {
                    Html = htmlPace
                };
                Divider divider = new Divider
                {
                    BorderColor = "#ffffff"
                };
                container.Content.Add(availability);
                container.Content.Add(pace);
                container.Content.Add(divider);

                CardSet eventCardSet = new CardSet
                {
                    MarginTop = "none",
                    MarginBottom = "none",
                    TitleLineClamp = 2,
                    LabelLineClamp = 2,
                    DescriptionLineClamp = 2,
                    Heading = new BlockHeading
                    {
                        Heading = "Chapel Events",
                        HeadingLevel = "3",
                        MarginTop = "none",
                        MarginBottom = "none",
                        HeadingLineClamp = 2,
                        DescriptionLineClamp = 2,
                        Buttons = new List<object>
                        {
                            new LinkButton
                            {
                                Title = "View all",
                                Icon = "drilldown",
                                IconPosition = "right",
                                TextColor = "#0ca38b",
                                URL = new XModComm.StandardLinks.XModuleLink
                                {
                                    XModule = new XModComm.StandardLinks.XModuleObject
                                    {
                                        ID = "ethos",
                                        RelativePath = "/GetEthosEvents"
                                    }
                                }
                            }
                        }
                    },
                    Items = new List<object>
                    {

                    }
                };


                CarouselCard carouselCard = new CarouselCard
                {
                    Size = "small",
                    LoopItems = true,
                    BorderColor = "transparent",
                    TextBlockMargin = "tight",
                    LabelTextColor = "#0ca38b",
                    Items = new List<CarouselCardItem> { }
                };

                foreach (Ethos.Event ethosEvent in events)
                {
                    if (ethosEvent.EventStart < DateTime.Today)
                        continue;

                    if (ethosEvent.EventStart > DateTime.Today.AddDays(7))
                        break;

                    if (ethosEvent.PrivateEvent)
                        continue;

                    carouselCard.Items.Add(new CarouselCardItem
                    {
                        Label = ethosEvent.EventStart.ToString("ddd, MMM dd, h:mm") + "-" + ethosEvent.EventEnd.ToString("t"),
                        Title = ethosEvent.EventName,
                        Description = ethosEvent.Description,
                        Url = new XModComm.StandardLinks.XModuleLink
                        {
                            XModule = new XModComm.StandardLinks.XModuleObject
                            {
                                ID = "ethos",
                                RelativePath = "/GetEthosEvents/Details/" + ethosEvent.ID.ToString()
                            }
                        }
                    });

                }

                eventCardSet.Items.Add(carouselCard);
                container.Content.Add(eventCardSet);



                response.Content.Add(blockHeading);
                response.Content.Add(container);

                return Person.Utilities.JSON.Serializer.Serialize(response);
            }
            catch (Exception ex)
            {
                XModComm.Response response = new Response();
                HtmlFragment htmlFragment = new HtmlFragment();
                htmlFragment.Html = ex.Message;
                response.Content.Add(htmlFragment);
                return Person.Utilities.JSON.Serializer.Serialize(response);
            }
        }





    }
}