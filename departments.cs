using System;
using System.Text.RegularExpressions;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using XModComm.Forms;

namespace Dynamic
{
    public class Script
    {
		public bool TestJSON(Person.CampusInformation.Department[] departments, string searchString, TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(departments, searchString, tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}
		
        public string GetJSON(Person.CampusInformation.Department[] departments, string searchString, TokenData tokenData)
        {
            XModComm.Response response = new Response();
	    response.Content.Add(new XModComm.UI.Divider
            {
                BorderStyle = "none"
            });
            Detail detail = new Detail();
			detail.Title = "Departments";
			
			Form form = new Form();
			form.RelativePath = "/SearchDepartments";

			XModComm.Forms.FormInput.Text textBox = new XModComm.Forms.FormInput.Text();
			textBox.Name = "SearchString";
			textBox.Label = "Department Search";
			textBox.Value = searchString;
			textBox.Required = false;
			form.Items.Add(textBox);
			
            ButtonContainer buttonContainer = new ButtonContainer();
			
            Button button = new Button()
            {
                Title = "Search",
                Name = "Submit",
                Value = "1"
            };
			buttonContainer.Buttons.Add(button);
			
			button = new Button()
            {
                Title = "Clear Search",
                Name = "Clear",
                Value = "1"
            };
            buttonContainer.Buttons.Add(button);
			
            form.Items.Add(buttonContainer);
			detail.Content.Add(form);
			
            List list = new List();
		list.ListStyle = "grouped";

            foreach (Person.CampusInformation.Department dept in departments)
            {
                string phone = dept.Phone;
                if (!String.IsNullOrEmpty(dept.Fax))
                {
                    string faxNumber = Regex.Replace(dept.Fax, @"\D", "");
                    if (faxNumber.Length == 4) // listed as extension
                        faxNumber = String.Format("(405) 425-{0}", faxNumber);
                    else
                        faxNumber = Regex.Replace(faxNumber, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                    phone = String.Format("{0} | Fax: {1}", phone, faxNumber);
                }
                list.AddListItem(dept.Name, null, phone, new RelativeLink(String.Format("/{0}", dept.ID)));
            }

            detail.Content.Add(list);
            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
