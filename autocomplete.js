$("#kgoui_Rcontent_I0_Ritems_I0_searchString").autocomplete({
  delay: 0,
  minLength: 1,
  source: function(request, response) {
    if(request.term.length > 0)
    {
      if(request.term[0] == "\"")
        request.term = request.term.substring(1);
      if(request.term[request.term.length - 1] == "\"")
        request.term = request.term.slice(0, -1);
    }
    $.getJSON("//search.oc.edu/autocomplete.aspx", {
        s: request.term.toLowerCase(),
        g: "1fa1e528-6235-4d35-932c-e9c38402ef97"
      }, function(data) {
        response(data);
      }
    );
  },
  focus: function(event, ui) {
    // prevent autocomplete from updating the textbox
    if($(this).val()[0] == "\"" && $(this).val()[$(this).val().length - 1] != "\"")
      $(this).val("\"" + ui.item.value + "\"");
    else if($(this).val()[0] == "\"" && $(this).val()[$(this).val().length - 1] == "\"")
      $(this).val("\"" + ui.item.value + "\"");
    else
      $(this).val(ui.item.value);
    event.preventDefault();
  },
  select: function(event, ui) {
    // prevent autocomplete from updating the textbox
    event.preventDefault();
    // search for selected item
    if($(this).val()[0] == "\"" && $(this).val()[$(this).val().length - 1] != "\"")
      $(this).val("\"" + ui.item.value + "\"");
    else if($(this).val()[0] == "\"" && $(this).val()[$(this).val().length - 1] == "\"")
      $(this).val("\"" + ui.item.value + "\"");
    else
      $(this).val(ui.item.value);
    $(this).parent().parent().submit();
  }
});
