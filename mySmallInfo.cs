using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.Collections.Generic;
using System.Linq;
using Person.Portal;

namespace Dynamic {
    public class Script {
        public bool TestJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            Person.Shoretel.MailBox voicemail,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            List<Person.Colleague.Restrictions.Restriction> restrictionList,
            Person.Colleague.Schedule.ClassSchedule classSchedule,
            List<Person.Portal.Proxy.ProxyProfile> dependentProfiles,
            List<Ethos.UserProfile> dependentEthosProfiles,
            TokenData tokenData,
            out Exception returnEx
        ) {
            try {
                GetJSON(userProfile, printBalance, mealPlan, voicemail, advisors, vehicleRegistrations, restrictionList, classSchedule, dependentProfiles, dependentEthosProfiles, tokenData);
                returnEx = null;
            }
            catch (Exception ex) {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON
        (
            Person.Portal.UserProfile userProfile,
            string printBalance,
            Person.Portal.MealPlan mealPlan,
            Person.Shoretel.MailBox voicemail,
            List<KeyValuePair<string, Person.Portal.UserProfile>> advisors,
            List<Person.Colleague.VehicleRegistration> vehicleRegistrations,
            List<Person.Colleague.Restrictions.Restriction> restrictionList,
            Person.Colleague.Schedule.ClassSchedule classSchedule,
            List<Person.Portal.Proxy.ProxyProfile> dependentProfiles,
            List<Ethos.UserProfile> dependentEthosProfiles,
            TokenData tokenData
        ) {
            XModComm.Response response = new Response();
            if (restrictionList != null && restrictionList.Count > 0) {
                Banner banner = new Banner {
                    Message = "You have " + restrictionList.Count + " checklist items to complete",
                    Type = "info",
                    Link = new XModuleLink("restrictions", "")
                };
                response.Metadata.AddBanner(banner);
            }
            if (tokenData.PageType == "small") {
                return Person.Utilities.JSON.Serializer.Serialize(response);
            }

            BlockHeading blockHeading = new BlockHeading {
                Heading = "My Dashboard",
                HeadingTextColor = "#5c0e1d",
                Image = new Image {
                    Url = "https://static.modolabs.com/ocu/launch.png",
                    BorderRadius = "full",
                    BackgroundColor = "#f04e3e"
                },
                ImageWidth = "2.25rem",
                ImageHeight = "2.25rem",
                MarginBottom = "tight"
            };
            response.Content.Add(blockHeading);

            XModComm.UI.Container.Container container = new XModComm.UI.Container.Container {
                MarginTop = "none",
                Padding = "none",
                Content = new List<object>
                {
                    new Divider
                    {
                        MarginTop = "xtight",
                        MarginBottom = "none",
                        BorderWidth = "2px",
                        BorderColor = "theme:subfocal_background_color"
                    }
                }

            };

            if (userProfile.isStudent) {
                Collapsible classes = new Collapsible() {
                    Collapsed = false,
                    MarginTop = "none",
                    MarginBottom = "none",
                    BorderTopWidth = "2px",
                    BorderTopColor = "theme:subfocal_background_color",
                    BorderBottomWidth = "2px",
                    BorderBottomColor = "theme:subfocal_background_color",
                    ContentBackgroundColor = "theme:focal_background_color",
                    Title = "My Classes"
                };

                try {
                    XModComm.UI.Event.EventList eventList = new XModComm.UI.Event.EventList() {
                        TitleLineClamp = 3,
                        DescriptionLineClamp = 1,
                        MarginTop = "none",
                        MarginBottom = "none",
                        ShowTopBorder = false,
                        ShowBottomBorder = false,
                        Heading = new BlockHeading {
                            Heading = DateTime.Today.ToString("dddd, MMMM dd"),
                            HeadingLevel = "3",
                            MarginTop = "none",
                            MarginBottom = "none",
                            Buttons = new List<object>
                    {
                        new LinkButton
                        {
                            Title = "View all",
                            AccessoryIcon = "drilldown",
                            IconPosition = "right",
                            Link = new XModComm.StandardLinks.XModuleLink
                            {
                                XModule = new XModComm.StandardLinks.XModuleObject
                                {
                                    ID = "classes"
                                }
                            }
                        }


                    }
                        },
                        Items = new List<XModComm.UI.Event.EventListItem>()
                    };


                    var dayOfWeek = new Dictionary<string, string>() { { "SU", "Sun" }, { "M", "Mon" }, { "T", "Tue" }, { "W", "Wed" }, { "TH", "Thu" }, { "F", "Fri" }, { "S", "Sat" } };
                    var classList = new List<XModComm.UI.Event.EventListItem>() { };
                    Regex regex = new Regex(@"\w{4}-\d{4}-([2,4,6,7,8,9]|10)\d");
                    foreach (Person.Colleague.Schedule.ClassSchedule.Section section in classSchedule.Sections) {
                        if ((regex.Match(section.Name).Success) || (section.Name.Contains("GRAD-5000")) || (section.Name.Contains("GRAD-4000")) || (section.Name.Contains("CRMJ-4903")) || (section.Name.Contains("SPAN-4711-03")) || (section.ShortTitle.Contains("Esports"))) {
                            continue;
                        }
                        foreach (Person.Colleague.Schedule.ClassSchedule.MeetingTime meetingTime in section.MeetingTimes) {
                            if (meetingTime.Days == null)
                                continue;

                            foreach (string day in meetingTime.Days) {
                                DateTime startDate = DateTime.Now;
                                DateTime endDate = DateTime.Now;
                                DateTime.TryParseExact(meetingTime.StartDate, "MM/dd/yy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out startDate);
                                DateTime.TryParseExact(meetingTime.EndDate, "MM/dd/yy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out endDate);
                                if (startDate <= DateTime.Now && endDate >= DateTime.Now) {
                                    if (dayOfWeek[day] == DateTime.Today.ToString("ddd")) {
                                        try {
                                            TimeSpan duration = DateTime.Parse(meetingTime.EndTime) - DateTime.Parse(meetingTime.StartTime);
                                            if (duration < new TimeSpan(1, 0, 0))
                                                duration = new TimeSpan(1, 0, 0);
                                            if (duration < new TimeSpan(1, 30, 0) && duration > new TimeSpan(1, 10, 0))
                                                duration = new TimeSpan(1, 30, 0);
                                            if (duration > new TimeSpan(1, 45, 0) && duration < new TimeSpan(2, 15, 0))
                                                duration = new TimeSpan(2, 0, 0);
                                            if (duration > new TimeSpan(2, 45, 0) && duration < new TimeSpan(3, 0, 0))
                                                duration = new TimeSpan(3, 0, 0);

                                            classList.Add(new XModComm.UI.Event.EventListItem {

                                                Title = section.ShortTitle,
                                                DatetimePrimaryLine = meetingTime.StartTime,
                                                DatetimeSecondaryLine = duration.TotalHours.ToString() + "h",
                                                Description = meetingTime.Building.Name + ", Room " + meetingTime.Room,
                                                DividerColor = "#f04e3e",
                                                Url = new XModComm.StandardLinks.XModuleLink {
                                                    XModule = new XModComm.StandardLinks.XModuleObject {
                                                        ID = "classes",
                                                        RelativePath = String.Format("/ClassDetails/{0}/{1}", "2023SP", section.STTR_SCHEDULE_ID)
                                                    }
                                                }
                                            });
                                        }
                                        catch {
                                            continue;
                                        }
                                    }
                                }
                            }
                        }

                    }

                    eventList.Items = classList.OrderBy(x => DateTime.Parse(Convert.ToString(x.GetType().GetProperty("DatetimePrimaryLine").GetValue(x, null)))).ToList<XModComm.UI.Event.EventListItem>();
                    classes.Content.Add(eventList);
                }
                catch (Exception ex) {
                    Person.Utilities.Email.SendFromServer("peyton.chenault@oc.edu", "gateway@oc.edu", "Error in mySmallInfo", userProfile.UserName + " (" + userProfile.UserID + ") had an issue with classes in myInfo:\n\n " + ex.Message + "\n" + ex.InnerException + "\n\n" + ex.TargetSite + "\n" + ex.StackTrace);
                    classes.Content.Add(new Detail { Title = "Classes are currently unavailable" });
                }
                container.Content.Add(classes);
            }

            //Restrictions
            if (restrictionList != null && restrictionList.Count > 0 && userProfile != null) {
                Collapsible myChecklistCon = new Collapsible() {
                    Collapsed = true,
                    MarginTop = "none",
                    MarginBottom = "none",
                    BorderTopWidth = "2px",
                    BorderTopColor = "theme:subfocal_background_color",
                    BorderBottomWidth = "2px",
                    BorderBottomColor = "theme:subfocal_background_color",
                    ContentBackgroundColor = "theme:focal_background_color",
                    Title = "My Checklist"
                };
                List myChecklistList = new List();
                myChecklistList.ItemSize = "small";
                myChecklistList.ListStyle = "separated";
                myChecklistList.MarginTop = "none";
                myChecklistList.MarginBottom = "none";

                foreach (Person.Colleague.Restrictions.Restriction restriction in restrictionList) {
                    myChecklistList.AddListItem
                    (
                        restriction.Title, "", restriction.DetailedDescription, new XModuleLink("restrictions", "")
                    );
                }
                myChecklistCon.Badge = new Badge {
                    Label = restrictionList.Count.ToString() + " items",
                    ScreenReaderLabel = "Checklist items to do",
                    BorderRadius = "full",
                    Size = "medium"
                };

                myChecklistCon.Content.Add(myChecklistList);
                container.Content.Add(myChecklistCon);
            }

            // Housing and Mailroom
            Collapsible housingMailroomCon = new Collapsible() {
                Collapsed = true,
                MarginTop = "none",
                MarginBottom = "none",
                BorderTopWidth = "2px",
                BorderTopColor = "theme:subfocal_background_color",
                BorderBottomWidth = "2px",
                BorderBottomColor = "theme:subfocal_background_color",
                ContentBackgroundColor = "theme:focal_background_color",
                Title = "Address & Mailroom"
            };
            List housingMailroom = new List();
            housingMailroom.ItemSize = "small";
            housingMailroom.ListStyle = "separated";
            housingMailroom.MarginTop = "none";
            housingMailroom.MarginBottom = "none";
            //housingMailroom.Heading = "HOUSING AND MAILROOM INFORMATION";

            if (!String.IsNullOrEmpty(userProfile.HousingRoomNumber) && !String.IsNullOrEmpty(userProfile.HousingBuilding))
                housingMailroom.AddListItem("Housing Assignment", "", userProfile.HousingBuilding + ", Room " + userProfile.HousingRoomNumber);
            else if (!String.IsNullOrEmpty(userProfile.HousingBuilding))
                housingMailroom.AddListItem("Housing Building", "", userProfile.HousingBuilding);
            if (!String.IsNullOrEmpty(userProfile.MailBox))
                housingMailroom.AddListItem("Mailbox", "", userProfile.MailBox);
            if (!String.IsNullOrEmpty(userProfile.MailBoxCombo))
                housingMailroom.AddListItem("Mailbox Combo", "", userProfile.MailBoxCombo);
            string addressString = "No Home Address on File";
	try {
            if (userProfile.Addresses.Count > 0) {
                if (!String.IsNullOrEmpty(userProfile.Addresses.First().AddressLines.Lines.First())) {
                    addressString = "";
                    foreach (string line in userProfile.Addresses.First().AddressLines.Lines)
                        if (!String.IsNullOrEmpty(line))
                            addressString += line + "\r\n";
                    addressString = addressString.TrimEnd();
                    if (!String.IsNullOrEmpty(userProfile.Addresses.First().City))
                        addressString += ", " + userProfile.Addresses.First().City;
                    if (!String.IsNullOrEmpty(userProfile.Addresses.First().State))
                        addressString += ", " + userProfile.Addresses.First().State;
                    if (!String.IsNullOrEmpty(userProfile.Addresses.First().Zip))
                        addressString += " " + userProfile.Addresses.First().Zip;
                }
            }
		}
		catch { addressString = "Error Retrieving Address";}
            housingMailroom.AddListItem("My Home Address", "", addressString);
            housingMailroom.AddListItem("Update your Home Address", "", "", new ExternalLink("https://services.oc.edu/552"), "edit");


            if (housingMailroom.Items.Count > 0) {
                housingMailroomCon.Content.Add(housingMailroom);
                container.Content.Add(housingMailroomCon);
            }

            // Advisors
            if (advisors != null && advisors.Count > 0) {
                Collapsible advisorListCon = new Collapsible() {
                    Collapsed = true,
                    MarginTop = "none",
                    MarginBottom = "none",
                    BorderTopWidth = "2px",
                    BorderTopColor = "theme:subfocal_background_color",
                    BorderBottomWidth = "2px",
                    BorderBottomColor = "theme:subfocal_background_color",
                    ContentBackgroundColor = "theme:focal_background_color",
                    Title = "My Advisors",
                    Content = new List<object> { }
                };
                CardSet cardSet = new CardSet() {
                    MarginTop = "none",
                    MarginBottom = "none",
                    Items = new List<object> { }
                };

                foreach (KeyValuePair<string, Person.Portal.UserProfile> kvp in advisors) {
                    Person.Portal.UserProfile advisorProfile = kvp.Value;
                    ContentCard advisorCard = new ContentCard {
                        Size = "small",
                        ImageStyle = "hero",
                        Image = new Image {
                            Url = advisorProfile.PicIDURL != null ? XModComm.Utilities.GetUserPhotoURL(advisorProfile.UserID, advisorProfile.PicIDURL, 480) : String.Empty,
                            Alt = advisorProfile.DisplayName != null ? advisorProfile.DisplayName : String.Empty
                        }
                    };

                    string label = "";
                    switch (kvp.Key) {
                        case "PFC":
                            label = "Personal Financial Counselor";
                            break;
                        case "Academic":
                            label = "Academic Advisor";
                            break;
                        case "Resident Director":
                            label = "Resident Director";
                            break;
                        case "Mentor":
                            label = "Mentor";
                            break;
                    }
                    advisorCard.Label = label;
                    advisorCard.Title = kvp.Value.DisplayName;
                    advisorCard.Link = new XModComm.StandardLinks.XModuleLink {
                        XModule = new XModComm.StandardLinks.XModuleObject {
                            ID = "userdetails",
                            RelativePath = "/" + advisorProfile.IDToken
                        }
                    };

                    cardSet.Items.Add(advisorCard);
                }
                advisorListCon.Content.Add(cardSet);
                container.Content.Add(advisorListCon);
            }

            // security
            Collapsible securityCont = new Collapsible() {
                Collapsed = true,
                MarginTop = "none",
                MarginBottom = "none",
                BorderTopWidth = "2px",
                BorderTopColor = "theme:subfocal_background_color",
                BorderBottomWidth = "2px",
                BorderBottomColor = "theme:subfocal_background_color",
                ContentBackgroundColor = "theme:focal_background_color",
                Title = "Pin & Password"
            };
            List security = new List();
            security.ItemSize = "small";
            security.ListStyle = "separated";
            security.MarginTop = "none";
            security.MarginBottom = "none";

            if (!String.IsNullOrEmpty(userProfile.IDPin))
                security.AddListItem("ID Card Pin: " + userProfile.IDPin, "", "");
            if (!String.IsNullOrEmpty(userProfile.PasswordExpireDate.ToString())) {
                security.AddListItem("Change Password", "", "Password expires on " + userProfile.PasswordExpireDate.ToShortDateString(), new ExternalLink("https://account.oc.edu/password"));
            }
            if (security.Items.Count > 0)
                securityCont.Content.Add(security);
            container.Content.Add(securityCont);

            //Person.Utilities.Email.SendFromServer("luther.hartman@oc.edu", "gateway.svc@oc.edu", "Info for user " + userProfile.UserID, "We got to line 387");


            // HR Information
            if (userProfile.isEmployee) {
                Collapsible hrInfoCon = new Collapsible() {
                    Collapsed = true,
                    MarginTop = "none",
                    MarginBottom = "none",
                    BorderTopWidth = "2px",
                    BorderTopColor = "theme:subfocal_background_color",
                    BorderBottomWidth = "2px",
                    BorderBottomColor = "theme:subfocal_background_color",
                    ContentBackgroundColor = "theme:focal_background_color",
                    Title = "Employment Info"
                };
                if (!userProfile.Roles.Contains(UserType.Student)) {
                    hrInfoCon.Collapsed = false;
                }
                List hrInfo = new List();
                hrInfo.ItemSize = "small";
                hrInfo.ListStyle = "separated";
                hrInfo.MarginTop = "none";
                hrInfo.MarginBottom = "none";

                if (userProfile.Titles != null) {
                    foreach (string title in userProfile.Titles) {
                        string titleReplaced = Regex.Replace(title, @"\\", "");
                        hrInfo.AddListItem(titleReplaced, "Job Title", "");
                    }
                }

                hrInfo.AddListItem(userProfile.OfficeBuilding + " (" + userProfile.OfficeBuildingCode + ") " + userProfile.OfficeRoomNumber, "On Campus", userProfile.Department);

                string homeAddress = null;
                if (userProfile.Addresses != null) {
                    foreach (Person.Portal.Address address in userProfile.Addresses) {
                        if (address.Type == Person.Portal.AddressType.Home &&
                            address.AddressLines != null &&
                            address.City != "" &&
                            address.State != "" &&
                            address.Zip != "") {
                            homeAddress = address.AddressLines.Lines[0] + address.City + ", " + address.State + " " + address.Zip;
                            break;
                        }
                    }
                    hrInfo.AddListItem("Home Address", "Off Campus", homeAddress);
                }

                if (userProfile.Extensions != null && userProfile.Extensions.Count > 0) {
                    if (!String.IsNullOrEmpty(userProfile.Extensions.ToString()))
                        hrInfo.AddListItem("Office Extension", "", userProfile.Extensions[0].Number);// TODO: Loop through all numbers
                    foreach (Person.Portal.LongDistancePin pin in userProfile.LongDistancePin) {
                        hrInfo.AddListItem("Long Distance Phone Pin", pin.Description, pin.Pin);
                    }
                    //phoneList.AddListItem("Voicemails", "", voicemail.NumberOfMessages.ToString(), new ExternalLink("https://shoretel.oc.edu"));
                }

                if (userProfile.PhoneNumbers != null) {
                    foreach (Person.Portal.PhoneNumber phone in userProfile.PhoneNumbers) {
                        hrInfo.AddListItem(phone.NumberType.ToString(), "", phone.Number);
                    }
                }

                hrInfo.AddListItem("Update your information with HR", "", "", new ExternalLink("https://services.oc.edu/158"), "edit");

                hrInfoCon.Content.Add(hrInfo);
                container.Content.Add(hrInfoCon);
            }

            // Proxy Information
            if (userProfile.Roles.Contains(UserType.Proxy)) {
                if (dependentProfiles.Count() == 0) {
                    Collapsible dependantCont = new Collapsible() {
                        Collapsed = false,
                        MarginTop = "none",
                        MarginBottom = "none",
                        BorderTopWidth = "2px",
                        BorderTopColor = "theme:subfocal_background_color",
                        BorderBottomWidth = "2px",
                        BorderBottomColor = "theme:subfocal_background_color",
                        ContentBackgroundColor = "theme:focal_background_color",
                        Title = "No Proxy Access"
                    };
                    List Info = new List() {
                        ItemSize = "small",
                        ListStyle = "separated",
                        MarginTop = "none",
                        MarginBottom = "none"
                    };
                    Info.AddListItem("Access not yet granted", "", "Please contact your student to grant access.", new ExternalLink("https://support.oc.edu/hc/en-us/articles/203199493-Granting-Third-Party-Access"));
                    dependantCont.Content.Add(Info);
                    container.Content.Add(dependantCont);
                }
                foreach (Person.Portal.Proxy.ProxyProfile proxyProfile in dependentProfiles) {
                    Collapsible dependantCont = new Collapsible() {
                        Collapsed = false,
                        MarginTop = "none",
                        MarginBottom = "none",
                        BorderTopWidth = "2px",
                        BorderTopColor = "theme:subfocal_background_color",
                        BorderBottomWidth = "2px",
                        BorderBottomColor = "theme:subfocal_background_color",
                        ContentBackgroundColor = "theme:focal_background_color",
                        Title = "Proxy Info for " + proxyProfile.DependentUserProfile.DisplayName
                    };
                    List contactInfo = new List() {
                        ItemSize = "small",
                        ListStyle = "separated",
                        MarginTop = "none",
                        MarginBottom = "none"
                    };
                    // Email & Address
                    contactInfo.Items.Add(new ListItem {
                        Title = "Email",
                        Description = proxyProfile.DependentUserProfile.OCEmailAddress,
                        Url = new XModComm.StandardLinks.ExternalLink { External = "mailto:" + proxyProfile.DependentUserProfile.OCEmailAddress },
                        ShowTopBorder = false,
                        ShowBottomBorder = false,
                        TitleLineClamp = 1,
                        DescriptionLineClamp = 1,
                        StickySectionHeaders = true
                    });
                    if (!string.IsNullOrEmpty(proxyProfile.DependentUserProfile.MailBox)) {
                        contactInfo.Items.Add(new ListItem {
                            Title = "Mailing Address",
                            Description = proxyProfile.DependentUserProfile.FirstName + " " + proxyProfile.DependentUserProfile.LastName + ", SB# " + proxyProfile.DependentUserProfile.MailBox +
                            "<br/>Oklahoma Christian University <br/>2501 E. Memorial Rd. Edmond, OK 73013-5599",
                            DescriptionLineClamp = 3,
                            ShowTopBorder = false,
                            ShowBottomBorder = false,
                            TitleLineClamp = 1,
                            StickySectionHeaders = true
                        });
                    }
                    // CashNet
                    if (proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.FinancialAccount)) {
                        ListItem financialAccountItem =
                            new ListItem {
                                Title = "Student Account",
                                Description = "View account balance here",
                                Url = new XModComm.StandardLinks.ModuleLink {
                                    Module = new XModComm.StandardLinks.ModuleObject {
                                        ID = "utilities",
                                        Page = "page",
                                        QueryParameters = new {
                                            relativePath = "/RedirectProxyToCashNet/" + proxyProfile.DependentID
                                        }

                                    }
                                },
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true
                            };

                        contactInfo.Items.Add(financialAccountItem);
                    }
                    // Financial Aid
                    if (proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.FinancialAid)) {
                        ListItem financialAidItem =
                            new ListItem {
                                Title = "Financial Aid Info",
                                Description = "View financial aid information here",
                                Url = new XModComm.StandardLinks.ModuleLink {
                                    Module = new XModComm.StandardLinks.ModuleObject {
                                        ID = "utilities",
                                        Page = "page",
                                        QueryParameters = new {
                                            relativePath = "/RedirectProxyToFinancialAid/" + proxyProfile.DependentUserProfile.IDToken
                                        }

                                    }
                                },
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true
                            };

                        contactInfo.Items.Add(financialAidItem);
                    }
                    if (proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.ScheduleView)) {
                        ListItem gradeItem =
                            new ListItem {
                                Title = "Gradebook Info",
                                Description = "View classes and grades here",
                                Url = new XModComm.StandardLinks.ModuleLink {
                                    Module = new XModComm.StandardLinks.ModuleObject {
                                        ID = "utilities",
                                        Page = "page",
                                        QueryParameters = new {
                                            relativePath = "/RedirectProxyToFinancialAid/" + proxyProfile.DependentUserProfile.IDToken
                                        }

                                    }
                                },
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true
                            };

                        contactInfo.Items.Add(gradeItem);
                    }
                    if (proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.TaxDocuments)) {
                        ListItem gradeItem =
                            new ListItem {
                                Title = "Tax Documents (1098-T)",
                                Description = "View and download tax documents here",
                                Url = new XModComm.StandardLinks.ModuleLink {
                                    Module = new XModComm.StandardLinks.ModuleObject {
                                        ID = "utilities",
                                        Page = "page",
                                        QueryParameters = new {
                                            relativePath = "/RedirectProxyToFinancialAid/" + proxyProfile.DependentUserProfile.IDToken
                                        }

                                    }
                                },
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true
                            };

                        contactInfo.Items.Add(gradeItem);
                    }
                    // Spiritual Life
                    foreach (Ethos.UserProfile dependentEthosProfile in dependentEthosProfiles) {
                        if (dependentEthosProfile.UserID == proxyProfile.DependentID && (proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.SpiritualLife) || proxyProfile.ProxyPermissions.Contains(Proxy.ProxyPermissions.ChapelView))) {
                            ListItem spiritualLifeItem =
                            new ListItem {
                                Title = "Earned Chapel Credits",
                                Description = dependentEthosProfile.CurrentSemesterCheckinCount.ToString(),
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true

                            };

                            contactInfo.Items.Add(spiritualLifeItem);
                            ListItem spiritualLifeItemTwo =
                            new ListItem {
                                Title = "Required Chapel Credits",
                                Description = dependentEthosProfile.RequiredKudos.ToString(),
                                ShowTopBorder = false,
                                ShowBottomBorder = false,
                                TitleLineClamp = 1,
                                DescriptionLineClamp = 1,
                                StickySectionHeaders = true

                            };

                            contactInfo.Items.Add(spiritualLifeItemTwo);
                        }
                    }
                    dependantCont.Content.Add(contactInfo);
                    container.Content.Add(dependantCont);
                }

            }



            //// Phone Information
            //if (userProfile.Extensions != null && userProfile.Extensions.Count > 0)
            //{
            //    Collapsible phoneListCon = new Collapsible()
            //    {
            //        Collapsed = true,
            //        MarginTop = "none",
            //        MarginBottom = "none",
            //        BorderTopWidth = "2px",
            //        BorderTopColor = "theme:subfocal_background_color",
            //        BorderBottomWidth = "2px",
            //        BorderBottomColor = "theme:subfocal_background_color",
            //        ContentBackgroundColor = "theme:focal_background_color",
            //        Title = "Phone Info"
            //    };
            //    List phoneList = new List();
            //    phoneList.ItemSize = "small";
            //    phoneList.ListStyle = "separated";
            //    phoneList.MarginTop = "none";
            //    phoneList.MarginBottom = "none";
            //    //phoneList.Heading = "PHONE INFORMATION";

            //    if (!String.IsNullOrEmpty(userProfile.Extensions.ToString()))
            //        phoneList.AddListItem("Extension", "", userProfile.Extensions[0].Number);// TODO: Loop through all numbers
            //    foreach (Person.Portal.LongDistancePin pin in userProfile.LongDistancePin)
            //    {
            //        phoneList.AddListItem("Long Distance Pin", pin.Description, pin.Pin);
            //    }
            //    //phoneList.AddListItem("Voicemails", "", voicemail.NumberOfMessages.ToString(), new ExternalLink("https://shoretel.oc.edu"));
            //    if (voicemail != null)
            //        phoneList.AddListItem("Voicemail Password", "", voicemail.Password); //TODO: add link and code to change password

            //    phoneListCon.Content.Add(phoneList);
            //    container.Content.Add(phoneListCon);
            //}

            //// mealList.AddListItem("Transactions", null, null, new XModuleLink("my_info", "/Transactions"));

            response.Content.Add(container);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }

}