using System;
using System.Collections.Generic;
using Person;
using Person.Portal;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;
using System.Data.SqlClient;

namespace Dynamic
{
    public class Script
    {
		public bool TestJSON(Ethos.UserProfile userProfile, List<Ethos.UserProfile> dependentProfiles, TokenData tokenData, out Exception returnEx)
		{
			try
			{
				GetJSON(userProfile, dependentProfiles, tokenData);
				returnEx = null;
			}
			catch (Exception ex)
			{
				returnEx = ex;
				return false;
			}
			return true;
		}
		
        public string GetJSON(Ethos.UserProfile userProfile, List<Ethos.UserProfile> dependentProfiles, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
		Detail bigDetail = new Detail();
		bigDetail.Title = "Spiritual Life";
            TabContainer tabContainer = new TabContainer();

            if (userProfile.isEmployee || userProfile.isStudent)
            {
                Tab tab = GetEthosInfo(userProfile);
				tabContainer.Tabs.Add(tab);
            }

            if (dependentProfiles != null && dependentProfiles.Count > 0)
            {
				foreach (var dependentProfile in dependentProfiles)
				{
					Tab tab = GetEthosInfoDependent(dependentProfile);
					tabContainer.Tabs.Add(tab);
				}
            }
            else if (userProfile.PrimaryRole.ToString() == "Proxy")
            {
                Tab tab = GetEthosInfo(userProfile);
				tabContainer.Tabs.Add(tab);
            }
			bigDetail.Content.Add(tabContainer);
			response.Content.Add(bigDetail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public Tab GetEthosInfo(Ethos.UserProfile userProfile)
        {
            Tab tab = new Tab();
            Detail detail = new Detail();
            //detail.Title = "Spiritual Life";

            if (userProfile.isStudent)
            {
                List infoList = new List();

                infoList.ListStyle = "grouped";
				infoList.TitleLineClamp = 1;
				infoList.DescriptionLineClamp = 1;
				infoList.Heading = "Semester Overview";
				infoList.Heading = infoList.Heading.ToUpper();
                infoList.AddListItem("Credits Earned: " + userProfile.CurrentSemesterCheckinCount, null, null, new XModuleLink("ethos", "/GetEthosHistory"));
                infoList.AddListItem("Credits Required: " + userProfile.RequiredKudos, null, null);
                string notice = null;
                if (userProfile.EstimatedProgress > userProfile.CurrentSemesterCheckinCount)
                    notice = (userProfile.EstimatedProgress - userProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                infoList.AddListItem("Current Pace: " + userProfile.EstimatedProgress, notice, "Credits you should have this far in the semester to reach your goal");

                detail.Content.Add(infoList);
            }

            List linksList = new List();
			linksList.ListStyle = "grouped";
			linksList.TitleLineClamp = 1;
			linksList.DescriptionLineClamp = 1;
            linksList.Heading = "Additional Links";
			linksList.Heading = linksList.Heading.ToUpper();
            linksList.AddListItem("Upcoming Chapel Events", null, null, new XModuleLink("ethos", "/GetEthosEvents"));
            //linksList.AddListItem("Submit an Ethos Event", null, null, new ExternalLink("https://services.oc.edu/redirect/686"));
            linksList.AddListItem("Requirements and Guidelines", null, null, new ExternalLink("http://www.oc.edu/spiritual-life/ethos/event-requirements-and-guidelines.html"));

            detail.Content.Add(linksList);

            tab = new Tab();
            tab.Title = userProfile.DisplayName;
            tab.Content.Add(detail);

            return tab;
        }

	public Tab GetEthosInfoDependent(Ethos.UserProfile userProfile)
        {
            Tab tab = new Tab();
            Detail detail = new Detail();
            detail.Title = "Ethos Information for " + userProfile.DisplayName;

            if (userProfile.isStudent)
            {
                List infoList = new List();

                infoList.ListStyle = "grouped";
				infoList.TitleLineClamp = 1;
				infoList.DescriptionLineClamp = 1;
				infoList.Heading = "Semester Overview";
				infoList.Heading = infoList.Heading.ToUpper();
                infoList.AddListItem("Kudos Earned: " + userProfile.CurrentSemesterCheckinCount, null, null, new XModuleLink("ethos", "/GetEthosHistory"));
                infoList.AddListItem("Kudos Required: " + userProfile.RequiredKudos, null, null);
                string notice = null;
                if (userProfile.EstimatedProgress > userProfile.CurrentSemesterCheckinCount)
                    notice = (userProfile.EstimatedProgress - userProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                infoList.AddListItem("Kudos Pace: " + userProfile.EstimatedProgress, notice, "Kudos you should have this far in the semester to reach your goal");

                detail.Content.Add(infoList);
            }

            tab = new Tab();
            tab.Title = userProfile.DisplayName;
            tab.Content.Add(detail);

            return tab;
        }

    }
}