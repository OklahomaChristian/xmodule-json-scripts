using System;
using Person;
using XModComm;
using System.Text;
using XModComm.UI;

namespace Dynamic
{
    public class Script
    {
        public string GetJSON(System.Collections.Generic.List<object> objectList, TokenData tokenData)
        {
            XModComm.Response response = new Response();

            HtmlFragment htmlFragment = new HtmlFragment();
            StringBuilder htmlStringBuilder = new StringBuilder();

            foreach (object obj in objectList)
            {
                Person.Portal.UserProfile userProfile = obj as Person.Portal.UserProfile;

                if (obj == null)
                {
                    //Console.WriteLine("ERROR: CAST FAILED");
                    continue;
                }

                htmlStringBuilder.Append("<a href='../userdetails/page?relativePath=%2F").Append(userProfile.IDToken).Append("'>").Append(userProfile.DisplayName).AppendLine("</a><br>");
            }

            htmlFragment.Html = htmlStringBuilder.ToString();
            response.Content.Add(htmlFragment);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}
