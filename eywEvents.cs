﻿using System;
using System.Collections.Generic;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;

namespace Dynamic
{
    public class EywEvents
    {
        public string Date { get; set; }
        public string DateTitle { get; set; }
        public List<Event> Events { get; set; }

        public EywEvents()
        {
            this.Events = new List<Event>();
        }
    }

    public class Event
    {
        public string Event_Name { get; set; }
        public string Location_Name { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public DateTime Start_Time { get; set; }
        public DateTime End_Time { get; set; }
    }

    public class Script
    {
        public string GetJSON(string json, TokenData tokenData = null)
        {
            XModComm.Response response = new Response();

            Detail detail = new Detail();
            detail.Title = "Schedule";

            Person.Utilities.JSON.Parser parser = new Person.Utilities.JSON.Parser();
            List<EywEvents> eventList = new List<EywEvents>();
            eventList = (List<EywEvents>)parser.DeserializeObject(json, eventList);

            foreach (EywEvents eywEvent in eventList)
            {
                List list = new List() { Grouped = true };
                list.Heading = eywEvent.DateTitle;

                foreach (Event _event in eywEvent.Events)
                {
                    list.Items.Add(
                        new Item()
                        {
                            Label = String.Format("{0} - {1}", _event.Start_Time.ToString("h:mm tt"), _event.End_Time.ToString("h:mm tt")),
                            Title = _event.Event_Name,
                            Description = _event.Location_Name
                        }
                    );
                }

                detail.Content.Add(list);
            }

            response.Content.Add(detail);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}