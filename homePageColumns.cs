using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using Person;
using Person.Portal;
using Ethos;
using XModComm;
using XModComm.UI;
using XModComm.UI.MultiColumn;
using XModComm.Links;
using XModComm.Arguments;
using System.Linq;
using XModComm.UI.Carousel;
using Image = XModComm.UI.Carousel.Image;

namespace Dynamic
{
    public class Script
    {
        private HomePageArugment Argument { get; set; }

        public bool TestJSON(HomePageArugment homePageArgument, string columnName, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(homePageArgument, columnName, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(HomePageArugment homePageArugment, string columnName, TokenData tokenData)
        {
            Argument = homePageArugment;

            XModComm.Response response = new Response();

            response.Metadata.Banners = null;

            if (homePageArugment.RestrictionList != null && homePageArugment.RestrictionList.Count() > 0)
            {
                Banner banner = new Banner
                {
                    Message = "You have " + homePageArugment.RestrictionList.Count() + " checklist items to complete",
                    Type = "info",
                    Link = new XModuleLink("restrictions", "")
                };
                response.Metadata.AddBanner(banner);
            }
            //			if (!homePageArugment.CovidStatus && (homePageArugment.UserProfile.Department == "Information Services" || homePageArugment.UserProfile.UserID == "1000742"))
            if (!homePageArugment.CovidStatus && (homePageArugment.UserProfile.PrimaryRole == UserType.Faculty || homePageArugment.UserProfile.PrimaryRole == UserType.Staff))
            {
                Banner banner = new Banner
                {
                    Message = "You need to complete the daily COVID self-assessment",
                    Type = "warning",
                    Link = new ExternalLink("https://services.oc.edu/forms/CovidAssessment")
                };
                //response.Metadata.AddBanner(banner);
            }

            Person.Portal.UserProfile userProfile = homePageArugment.UserProfile;

            ///////////////////
            // CORONAVIRUS NEWS
            ///////////////////
            FeedPortlet coronavirus = new FeedPortlet
            {
                NavbarTitle = "Coronavirus Notes (Employees)",
                Feed = "coronavirus_updates",
                Module = "coronavirus_updates",
                Height = "small"
            };

            ///////////////////
            // ANNOUNCEMENTS
            ///////////////////
            FeedPortlet announcements = new FeedPortlet
            {
                NavbarTitle = "Announcements",
                Feed = "student_news",
                Module = "student_news",
                Height = "small"
            };

            if (userProfile.isEmployee)
            {
                announcements.Feed = "employee_news";
                announcements.Module = "employee_news";
            }

            ///////////////////
            // SOCIAL
            ///////////////////
            FeedPortlet social = new FeedPortlet
            {
                NavbarTitle = "Social",
                Feed = "twitter",
                Module = "social",
                ContentType = "social",
                Height = "small"
            };

            ///////////////////
            // PHOTOS
            ///////////////////
            // Josh - 10/22/2019
            //FeedPortlet photos = new FeedPortlet
            //{
            //    NavbarTitle = "Photos",
            //    Feed = "instagram",
            //    Module = "photos",
            //    Height = "medium"
            //};

            ///////////////////
            // VIDEOS
            ///////////////////
            FeedPortlet videos = new FeedPortlet
            {
                NavbarTitle = "Videos",
                Feed = "youtube_channel",
                Module = "video",
                Height = "medium"
            };

            ///////////////////
            // MY CHECK LIST
            ///////////////////
            Portlet myChecklist = LoadChecklistPortlet(userProfile, homePageArugment.RestrictionList);

            /////////////////
            // MY INFO
            /////////////////
            Portlet myInfo = LoadMyInfoPortlet(userProfile, homePageArugment.MealPlan);

            /////////////////
            // ETHOS
            /////////////////
            Portlet ethos = null;
            if (userProfile != null)
            {
                if (userProfile.PrimaryRole == Person.UserType.Student)
                {
                    ethos = LoadStudentEthosPortlet(homePageArugment.EthosProfile);
                }
                else if (userProfile.PrimaryRole == Person.UserType.Proxy)
                {
                    ethos = LoadProxyEthosPortlet(homePageArugment.DependentEthosProfiles);
                }
                else
                {
                    ethos = LoadFacStaffEthosPortlet(homePageArugment.EthosEvents, homePageArugment.EthosLocations);
                }
            }

            /////////////////
            // QUCIKLINKS
            /////////////////
            Portlet quicklinks = LoadQuicklinksPortlet(homePageArugment.QuickLinks, userProfile, tokenData);

            /////////////////
            // PROXY LINKS
            /////////////////
            Portlet proxyLinks = null;
            if (homePageArugment.DependentProfiles != null && userProfile.Roles.Contains(UserType.Proxy))
            {
                proxyLinks = LoadProxylinksPortlet(homePageArugment.DependentProfiles.ToArray());
            }

            ////////////////////////
            // DEPENDENT ADDRESSES
            ////////////////////////
            Portlet proxyDependentAddresses = null;
            if (homePageArugment.DependentProfiles != null && userProfile.PrimaryRole != UserType.Student)
            {
                proxyDependentAddresses = LoadDependentAddressesPortlet(homePageArugment.DependentProfiles.ToArray());
            }

            ///////////////////
            // BUILD LAYOUT
            ///////////////////
            System.Collections.Generic.List<object> column1 = new System.Collections.Generic.List<object>();
            System.Collections.Generic.List<object> column2 = new System.Collections.Generic.List<object>();
            System.Collections.Generic.List<object> column3 = new System.Collections.Generic.List<object>();

            if (tokenData.IsTablet())
            {
                int announcementsLength = 4;
                int ethosLength = 7;
                int quicklinksLength = quicklinks.Content.Count;
                int myinfoLength = myInfo.Content.Count;
                int socialLength = 4;
                int photosLength = 8;
                int videosLength = 8;

                int leftColumnLength = announcementsLength + myinfoLength + ethosLength;
                int rightColumnLength = quicklinksLength;

                if (myChecklist != null && myChecklist.Content.Count > 0)
                    column1.Add(myChecklist);
                if (userProfile.isEmployee)
                    column2.Add(coronavirus);
                column1.Add(announcements);
                if (myInfo != null && myInfo.Content.Count > 0)
                    column1.Add(myInfo);
                if (ethos != null && ethos.Content.Count > 0 && !userProfile.isEmployee)
                    column1.Add(ethos);
                if (proxyLinks != null && proxyLinks.Content.Count > 0)
                    column3.Add(proxyLinks);
                if (proxyDependentAddresses != null && proxyDependentAddresses.Content.Count > 0)
                    column3.Add(proxyDependentAddresses);
                if (quicklinks != null && quicklinks.Content.Count > 0)
                    column2.Add(quicklinks);

                if (rightColumnLength < leftColumnLength)
                {
                    column2.Add(social);
                    rightColumnLength += socialLength;
                }
                else
                {
                    column1.Add(social);
                    leftColumnLength += socialLength;
                }

                if (rightColumnLength < leftColumnLength)
                {
                    //column2.Add(photos);
                    //rightColumnLength += photosLength;
                }
                else
                {
                    //column1.Add(photos);
                    //leftColumnLength += photosLength;
                }

                if (rightColumnLength < leftColumnLength)
                {
                    column2.Add(videos);
                    rightColumnLength += videosLength;
                }
                else
                {
                    column1.Add(videos);
                    leftColumnLength += videosLength;
                }

            }
            else
            {
                column1.Add(social);
                //column1.Add(photos);
                column1.Add(videos);

                if (myChecklist != null && myChecklist.Content.Count > 0)
                    column2.Add(myChecklist);

                if (userProfile.isEmployee)
                    column2.Add(coronavirus);

                column2.Add(announcements);

                if (myInfo != null && myInfo.Content.Count > 0)
                    column2.Add(myInfo);
                if (ethos != null && ethos.Content.Count > 0 && !userProfile.isEmployee)
                    column2.Add(ethos);

                if (proxyLinks != null && proxyLinks.Content.Count > 0)
                    column3.Add(proxyLinks);
                if (proxyDependentAddresses != null && proxyDependentAddresses.Content.Count > 0)
                    column3.Add(proxyDependentAddresses);
                if (quicklinks != null && quicklinks.Content.Count > 0)
                    column3.Add(quicklinks);
            }

            ///////////////////
            // COMBINE
            ///////////////////

            //if (columnName == "A")
            //    response.Content.Add(column2);
            //if (columnName == "B1")
            //    response.Content.Add(column1);
            //if (columnName == "B2")
            //    response.Content.Add(column3);

            XModComm.UI.Carousel.Carousel testCarousel = new XModComm.UI.Carousel.Carousel();
            testCarousel.Items.Add(new CarouselItem
            {
                Title = "Test TITLE",
                Image = new Image { URL = "http://xmodule-examples.modolabs.net/showcase/student_dashboard/images/basketball_stock.jpg" }
            });
            testCarousel.Items.Add(new CarouselItem
            {
                Title = "SECOND Test TITLE",
                Subtitle = "This is where the subtitle goes",
                Image = new Image { URL = "http://xmodule-examples.modolabs.net/showcase/student_dashboard/images/basketball_stock.jpg" }
            });
            testCarousel.Items.Add(new CarouselItem
            {
                Title = "And no image?",
                Subtitle = "This is where the subtitle goes. This is where the subtitle goes. This is where the subtitle goes. This is where the subtitle goes. This is where the subtitle goes. This is where the subtitle goes"
            });

            Portlet testPortlet = new Portlet();
            testPortlet.Content.Add(testCarousel);

            MultiColumn multiColumn = new MultiColumn();
            if (column3.Count > 0 && columnName == "B1")
            {
                multiColumn.Columns.Add(column3);
                //response.Content.Add(column3);
            }
            if (column2.Count > 0 && columnName == "A")
            {
                //multiColumn.Columns.Add(column2);
                response.Content.Add(column1);
            }
            if (column1.Count > 0 && columnName == "B2")
            {
                //multiColumn.Columns.Add(column1);
                response.Content.Add(testPortlet);
            }


            Person.Utilities.Email.SendFromServer("luther.hartman@oc.edu", "ModoTest@oc.edu", "homePageColumn debugging for column: " + columnName, Person.Utilities.JSON.Serializer.Serialize(response), false);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

        public static Portlet LoadFacStaffEthosPortlet(Ethos.Event[] events, Dictionary<int, Ethos.Location> ethosLocations)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Ethos",
                NavbarIcon = "religious_services",
                NavbarLink = new XModuleLink("ethos", "")
            };

            try
            {
                List eventsList = new List
                {
                    Heading = "Upcoming Events"
                };

                if (events == null || events.Length == 0)
                {
                    eventsList.Items.Add(new Item() { Title = "No Events Found" });
                    portlet.Content.Add(eventsList);
                    return portlet;
                }

                for (int i = 0; i < 6; i++)
                {
                    if (i >= events.Length)
                    {
                        break;
                    }

                    if (events[i].PrivateEvent)
                    {
                        continue;
                    }

                    StringBuilder eventDescription = new StringBuilder(256);

                    Ethos.Location eventLocation = null;
                    eventDescription
                        .Append(events[i].EventStart.ToString("MMM dd, h:mm tt"))
                        .Append("-")
                        .Append(events[i].EventEnd.ToString("h:mm tt"))
                        .Append(", ")
                        .Append(events[i].Room.All(char.IsDigit) ? "Room " + events[i].Room : events[i].Room)
                        .Append(ethosLocations.TryGetValue(i, out eventLocation) ? ", " + eventLocation.Title : "");

                    eventsList.Items.Add
                    (
                        new Item()
                        {
                            Title = events[i].EventName,
                            Description = eventDescription.ToString(),
                            Link = new XModuleLink("ethos", "/GetEthosEvents/Details/" + events[i].ID)
                        }
                    );
                }

                if (eventsList.Items.Count == 0)
                {
                    eventsList.Items.Add(new Item() { Title = "No Events Found" });
                }

                portlet.Content.Add(eventsList);
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            return portlet;
        }

        public static Portlet LoadStudentEthosPortlet(Ethos.UserProfile ethosUserProfile)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Ethos",
                NavbarIcon = "religious_services",
                NavbarLink = new XModuleLink("ethos", "")
            };

            try
            {
                List infoList = new List
                {
                    Heading = "Semester Overview"
                };
                infoList.Items.Add
                (
                    new Item()
                    {
                        Title = "Kudos Earned: " + ethosUserProfile.CurrentSemesterCheckinCount,
                        Link = new XModuleLink("ethos", "/GetEthosHistory")
                    }
                );

                infoList.Items.Add(new Item() { Title = "Kudos Required: " + ethosUserProfile.RequiredKudos });

                string notice = null;
                if (ethosUserProfile.EstimatedProgress > ethosUserProfile.CurrentSemesterCheckinCount)
                {
                    notice = (ethosUserProfile.EstimatedProgress - ethosUserProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                }
                infoList.Items.Add
                (
                    new Item()
                    {
                        Title = "Kudos Pace: " + ethosUserProfile.EstimatedProgress,
                        Label = notice,
                        Description = "Kudos you should have this far in the semester to reach your goal"
                    }
                );

                infoList.Items.Add
                (
                    new Item() { Title = "Upcoming Ethos Events", Link = new XModuleLink("ethos", "/GetEthosEvents") }
                );

                portlet.Content.Add(infoList);
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            return portlet;
        }

        public static Portlet LoadProxyEthosPortlet(List<Ethos.UserProfile> dependentEthosProfiles)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Ethos",
                NavbarIcon = "religious_services",
                NavbarLink = new XModuleLink("ethos", "")
            };

            try
            {
                if (dependentEthosProfiles == null || dependentEthosProfiles.Count == 0)
                {
                    return portlet;
                }

                foreach (Ethos.UserProfile ethosProfile in dependentEthosProfiles)
                {
                    List infoList = new List
                    {
                        Heading = "Ethos Information for " + ethosProfile.DisplayName
                    };
                    infoList.Items.Add
                    (
                        new Item()
                        {
                            Title = "Kudos Earned: " + ethosProfile.CurrentSemesterCheckinCount,
                            Link = new XModuleLink("ethos", "/GetEthosHistory")
                        }
                    );
                    infoList.Items.Add
                    (
                        new Item()
                        {
                            Title = "Kudos Required: " + ethosProfile.RequiredKudos
                        }
                    );
                    string notice = null;
                    if (ethosProfile.EstimatedProgress > ethosProfile.CurrentSemesterCheckinCount)
                    {
                        notice = (ethosProfile.EstimatedProgress - ethosProfile.CurrentSemesterCheckinCount) + " Kudos behind";
                    }
                    infoList.Items.Add
                    (
                        new Item()
                        {
                            Title = "Kudos Pace: " + ethosProfile.EstimatedProgress,
                            Label = notice,
                            Description = "Kudos you should have this far in the semester to reach your goal"
                        }
                    );
                    portlet.Content.Add(infoList);
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            return portlet;
        }

        public static Portlet LoadMyInfoPortlet(Person.Portal.UserProfile userProfile, Person.Portal.MealPlan mealPlan)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "My Info",
                NavbarIcon = "info",
                NavbarLink = new XModuleLink("my_info", "")
            };

            List myInfoList = new List();
            try
            {
                if (userProfile.PrimaryRole == Person.UserType.Student)
                {
                    if (!String.IsNullOrEmpty(userProfile.HousingBuildingCode))
                    {
                        myInfoList.Items.Add(new Item() { Title = "Housing : " + userProfile.HousingBuildingCode });
                    }

                    if (!String.IsNullOrEmpty(userProfile.HousingRoomNumber))
                    {
                        myInfoList.Items.Add(new Item() { Title = "Housing Room # : " + userProfile.HousingRoomNumber });
                    }

                    if (!String.IsNullOrEmpty(userProfile.MailBox))
                    {
                        myInfoList.Items.Add
                        (
                            new Item()
                            {
                                Title = "Mail Box # : " + userProfile.MailBox + ": " + userProfile.MailBoxCombo,
                                Link = new ExternalLink("https://support.oc.edu/hc/en-us/articles/203893509-How-To-Open-Your-Mailbox")
                            }
                        );
                    }

                    string printBalance = Person.uniFLOW.UserInformation.GetUserPrintBalance(userProfile.UserID);
                    if (!String.IsNullOrEmpty(printBalance))
                    {
                        decimal balance = System.Convert.ToDecimal(printBalance);
                        myInfoList.Items.Add
                        (
                            new Item()
                            {
                                Title = "Print Balance : " + balance.ToString("C3", new CultureInfo("en-US")),
                                Link = new ExternalLink("https://printers.oc.edu/pwclient")
                            }
                        );
                    }
                }

                if (!String.IsNullOrEmpty(mealPlan.MealBoard))
                {
                    myInfoList.Items.Add
                    (
                        new Item() { Title = "Plan Name : " + mealPlan.MealPlanName }
                    );

                    myInfoList.Items.Add
                    (
                        new Item()
                        {
                            Title = "Meals Remaining : " + mealPlan.MealBoard,
                            Link = new ExternalLink("https://services.oc.edu/redirect/520")
                        }
                    );
                }

                if (!String.IsNullOrEmpty(mealPlan.MealPoints))
                {
                    myInfoList.Items.Add
                    (
                        new Item()
                        {
                            Title = "Eagle Bucks Remaining : " + mealPlan.MealPoints,
                            Link = new ExternalLink("https://services.oc.edu/redirect/521")
                        }
                    );
                }

                if (!String.IsNullOrEmpty(userProfile.IDPin))
                {
                    myInfoList.Items.Add
                    (
                        new Item() { Title = "ID Card Pin #", Link = new XModuleLink("my_info", "/IdPin") }
                    );
                }

//                if (userProfile.PasswordExpireDate > DateTime.Now && false)
//                {
//                    myInfoList.Items.Add
//                    (
//                        new Item()
//                        {
//                            Title = "Password Expires on " + userProfile.PasswordExpireDate.ToString("MM/dd/yyyy"),
//                            Link = new ExternalLink("https://account.oc.edu/password")
//                        }
//                    );
//                }

                if (userProfile.isEmployee)
                {
                    if (userProfile.LongDistancePin.Count > 0)
                    {
                        foreach (Person.Portal.LongDistancePin longDistancePin in userProfile.LongDistancePin)
                        {
                            myInfoList.Items.Add(new Item() { Title = "Long Distance Pin # : " + longDistancePin.Pin.ToString() });
                        }
                    }

                    //if (userProfile.Extensions != null && userProfile.Extensions.Count > 0)
                    //{
                    //    Person.Shoretel.MailBox mailBox = new Person.Shoretel.MailBox(userProfile.Extensions[0].Number.ToString());
                    //    myInfoList.AddListItem("Voicemail Password : " + mailBox.Password, null, null);
                    //    myInfoList.AddListItem("Voicemails : " + mailBox.NumberOfMessages.ToString(), null, null);
                    //}
                }

                portlet.Content.Add(myInfoList);
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            return portlet;
        }

        public static Portlet LoadChecklistPortlet(Person.Portal.UserProfile userProfile, List<Person.Colleague.Restrictions.Restriction> restrictionList)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "My Checklist",
                NavbarIcon = "info"
            };

            try
            {
                if (restrictionList != null && restrictionList.Count() > 0 && userProfile != null)
                {
                    List myChecklistList = new List();

                    foreach (Person.Colleague.Restrictions.Restriction restriction in restrictionList)
                    {
                        myChecklistList.AddListItem
                        (
                            restriction.Title, restriction.Description, restriction.DetailedDescription
                        );
                    }

                    portlet.Content.Add(myChecklistList);
                };

                return portlet;
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                if (restrictionList != null && restrictionList.Count() > 0)
                {
                    portlet.NavbarTitle += " ERROR";
                }
            }

            return portlet;
        }

        public static Portlet LoadDependentAddressesPortlet(Proxy.ProxyProfile[] dependentProfiles)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Student Addresses",
                NavbarIcon = "mail"
            };

            List addressLinksReturn = new List();

            try
            {
                if (dependentProfiles.Count() == 0)
                {
                    return portlet;
                }

                foreach (Proxy.ProxyProfile dependentProfile in dependentProfiles)
                {
                    addressLinksReturn.Items.Add(new Item()
                    {
                        Title = dependentProfile.DependentUserProfile.OCEmailAddress,
                        Label = "Email",
                        Link = new ExternalLink("mailto:" + dependentProfile.DependentUserProfile.OCEmailAddress)
                    }
                    );
                    string mailingAddress = String.Format
                    (
                        "{0} {1}, SB# {2}<br>Oklahoma Christian University<br>2501 E. Memorial Rd." +
                        "<br>Edmond, OK 73013-5599",
                        dependentProfile.DependentUserProfile.PreferedFirstName,
                        dependentProfile.DependentUserProfile.LastName,
                        dependentProfile.DependentUserProfile.MailBox
                    );
                    addressLinksReturn.Items.Add(new Item() { Title = mailingAddress, Label = "Mailing Address" });
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            portlet.Content.Add(addressLinksReturn);

            return portlet;
        }

        public static Portlet LoadProxylinksPortlet(Proxy.ProxyProfile[] dependentProfiles)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Proxy Links",
                NavbarIcon = "links"
            };

            try
            {
                if (dependentProfiles.Count() == 0)
                {
                    portlet.Content.Add(new Item() { Title = "Access has not yet been granted by any of your students." });
                    return portlet;
                }

                List studentAccountList = new List { Heading = "Student Account" };
                List finAidList = new List { Heading = "Financial Aid" };

                foreach (Proxy.ProxyProfile dependent in dependentProfiles)
                {
                    if (dependent.ProxyPermissions.Contains(Proxy.ProxyPermissions.FinancialAccount))
                    {
                        studentAccountList.Items.Add
                        (
                            new Item()
                            {
                                Title = "Student Account for " + dependent.DependentUserProfile.DisplayName,
                                Link = new XModuleLink("utilities", "/RedirectProxyToCashNet/" + dependent.DependentID)
                            }
                        );
                    }

                    if (dependent.ProxyPermissions.Contains(Proxy.ProxyPermissions.FinancialAid))
                    {
                        finAidList.Items.Add
                        (
                            new Item()
                            {
                                Title = "Financial Aid for " + dependent.DependentUserProfile.DisplayName,
                                Link = new XModuleLink("utilities", "/RedirectProxyToFinancialAid/" + dependent.DependentUserProfile.IDToken)
                            }
                        );
                    }
                }

                // If there's nothing to show, do not create portlet
                if (studentAccountList.Items.Count == 0 && finAidList.Items.Count == 0)
                    return null;

                if (studentAccountList.Items.Count == 0)
                {
                    studentAccountList.Items.Add(new Item() { Title = "Access to CASHNet is not available for any of your students." });
                }

                if (finAidList.Items.Count == 0)
                {
                    finAidList.Items.Add(new Item() { Title = "Access to Financial Aid is not available for any of your students." });
                }

                portlet.Content.Add(studentAccountList);
                portlet.Content.Add(finAidList);
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            return portlet;
        }

        public static Portlet LoadQuicklinksPortlet(Person.Portal.Service[] quickLinks, Person.Portal.UserProfile userProfile, TokenData tokenData)
        {
            Portlet portlet = new Portlet
            {
                NavbarTitle = "Quicklinks",
                NavbarIcon = "links",
                NavbarLink = new XModuleLink("quicklinks", "/edit")
            };

            List list = new List();
            list.Grouped = false;

            try
            {
                if (quickLinks.Length == 0)
                {
                    if (userProfile != null && userProfile.PrimaryRole != Person.UserType.Proxy)
                        list.Items.Add(new Item() { Title = "No results found" });
                    return portlet;
                }

                if (userProfile.PrimaryRole == Person.UserType.Student || userProfile.isEmployee)
                {
                    list.Items.Add(new Item() { Title = "Blackboard", Link = new ExternalLink("https://bb.oc.edu") });
                    list.Items.Add(new Item() { Title = "OC Email", Link = new ExternalLink("http://gmail.oc.edu") });
                    list.Items.Add(new Item() { Title = "Services", Link = new ExternalLink("https://my.oc.edu/default/services/index") });
                    if (userProfile.isEmployee)
                    {
                        list.Items.Add(new Item()
                        {
                            Title = "Submit a Campus Announcement",
                            Link = new ExternalLink("mailto:campus.announcements@oc.edu")
                        });
                    }
                    list.Items.Add(new Item()
                    {
                        Title = "Submit a Student Announcement",
                        Link = new ExternalLink("mailto:student.announcements@oc.edu")
                    });
                }

                Array.Sort(quickLinks, (x, y) => x.Title.CompareTo(y.Title));

                foreach (Person.Portal.Service quickLink in quickLinks)
                {
                    if (tokenData.PageType == "small" || tokenData.IsTablet())
                    {
                        list.Items.Add
                        (
                            new Item()
                            {
                                Title = quickLink.Title,
                                Link = new XModuleLink("utilities", String.Format("/SSORedirect/{0}", quickLink.ID))
                            }
                        );
                    }
                    else
                    {
                        list.Items.Add
                        (
                            new Item()
                            {
                                Title = quickLink.Title,
                                Link = new ExternalLink(quickLink.RedirectURI.ToString())
                            }
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                portlet.NavbarTitle += " ERROR";
            }

            portlet.Content.Add(list);
            return portlet;
        }

        public static void SendErrorEmail(string methodName, Exception ex)
        {
            Person.Utilities.Email.SendFromServer
            (
                new string[] { "luther.hartman@oc.edu" },
                "myoc@oc.edu",
                methodName,
                ex.Message + "\r\n" + ex.StackTrace,
                false
            );
        }
    }
}