using System;
using System.Collections.Generic;
using Person;
using Person.Portal;
using XModComm;
using Ethos;
using XModComm.UI;
using XModComm.Links;
using System.Data.SqlClient;
using System.Linq;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(Dictionary<string, XModComm.MenuInfo.Menu> menuList, TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(menuList, tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(Dictionary<string, XModComm.MenuInfo.Menu> menuList, TokenData tokenData)
        {
            XModComm.Response response = new Response();
            response.ContentContainerWidth = "full";
            XModComm.UI.Column.ThreeColumn threeColumn = new XModComm.UI.Column.ThreeColumn() { Gutters = true };
            //XModComm.UI.Image openImage = new Image { Url = "https://myocfiles.oc.edu/files/myoc/opencircle.png" };
            //XModComm.UI.Image closedImage = new Image { Url = "https://myocfiles.oc.edu/files/myoc/closedcircle.png" };


            XModComm.UI.MultiColumn.Portlet fiveSpicePortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["fivespice"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#edd9b7" };
            XModComm.UI.MultiColumn.Portlet flamePortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["flame"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#edb7b7" };
            XModComm.UI.MultiColumn.Portlet freshPortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["fresh"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#b9edb7" };
            XModComm.UI.MultiColumn.Portlet globalFusionPortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["globalfusion"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#d9b7ed" };
            XModComm.UI.MultiColumn.Portlet kitchenPortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["kitchentable"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#b7caed" };
            XModComm.UI.MultiColumn.Portlet pastaPortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["scramblepastatoss"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#ecedb7" };
            XModComm.UI.MultiColumn.Portlet nourishPortlet = new XModComm.UI.MultiColumn.Portlet { Heading = new BlockHeading { Heading = menuList["nourish"].Title, Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false } } }, Content = new List<object> { new HtmlFragment { Html = "" } }, ShadowOpacity = 0.27, Shadow = "tight", BackgroundColor = "#BCBC82" };
            Dictionary<string, XModComm.UI.MultiColumn.Portlet> portletDictionary = new Dictionary<string, XModComm.UI.MultiColumn.Portlet> { };
            portletDictionary.Add("fivespice", fiveSpicePortlet);
            portletDictionary.Add("flame", flamePortlet);
            portletDictionary.Add("fresh", freshPortlet);
            portletDictionary.Add("globalfusion", globalFusionPortlet);
            portletDictionary.Add("kitchentable", kitchenPortlet);
            portletDictionary.Add("scramblepastatoss", pastaPortlet);
            portletDictionary.Add("nourish", nourishPortlet);

            foreach (XModComm.MenuInfo.Menu menu in menuList.Values)
            {
                string key = menuList.FirstOrDefault(x => x.Value == menu).Key;
				try{
					string titleTimeModifier = "";
					int eventIndex = menuList[key].EventList.IndexOf(menuList[key].EventList.FirstOrDefault(item => item.EndTime > DateTime.Now));
					if (menu.Open)
					{
						titleTimeModifier = "\t\tOpen: " + menuList[key].EventList[eventIndex].StartTime.ToShortTimeString() + " - " + menuList[key].EventList[eventIndex].EndTime.ToShortTimeString() + ", " + menuList[key].EventList[eventIndex].StartTime.ToString("ddd");
						portletDictionary[key].Heading.Buttons = new List<object> { new Dropdown { AccessoryIcon = "confirm", ShowDisclosureIcon = false, ToggleActionStyle = "constructive", InitiallyHidden = false, Items = new List<DropdownItem> { new DropdownItem { Description = titleTimeModifier, Disabled = true } } } };
					}
					else
					{
						titleTimeModifier = "\t\tNext Open: " + menuList[key].EventList[eventIndex].StartTime.ToShortTimeString() + " - " + menuList[key].EventList[eventIndex].EndTime.ToShortTimeString() + ", " + menuList[key].EventList[eventIndex].StartTime.ToString("ddd");
						portletDictionary[key].Heading.Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false, Items = new List<DropdownItem> { new DropdownItem { Description = titleTimeModifier, Disabled = true } } } };
					}

					string nutritionInfoOne = "";
					if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Serving))
						nutritionInfoOne += "Serving Size: " + menuList[key].EventList[eventIndex].Food[0].Serving + "<br/>";
					if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Calories))
						nutritionInfoOne += "Calories: " + menuList[key].EventList[eventIndex].Food[0].Calories + "<br/>";
					if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Fat))
						nutritionInfoOne += "Total Fat: " + menuList[key].EventList[eventIndex].Food[0].Fat + "<br/>";
					if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Carbs))
						nutritionInfoOne += "Carbs: " + menuList[key].EventList[eventIndex].Food[0].Carbs + "<br/>";
					if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Protein))
						nutritionInfoOne += "Protein: " + menuList[key].EventList[eventIndex].Food[0].Protein + "<br/>";
					if (string.IsNullOrEmpty(nutritionInfoOne))
						nutritionInfoOne = "Nutrition information is not available";

					portletDictionary[key].Content = new List<object> { new Collapsible { Title = menuList[key].EventList[eventIndex].Food[0].Name, Collapsed = true, TitleFontSize = "xsmall", Content = new List<object> { new HtmlFragment { Html = nutritionInfoOne } }, BorderBottomStyle = "none", BorderTopStyle = "none" } };
					//if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[0].Serving))
					List<string> foodList = new List<string> { menuList[key].EventList[eventIndex].Food[0].Name };
					if (menuList[key].EventList[eventIndex].Food[0].Name.ToUpper().Contains("CLOSE"))
					{
						titleTimeModifier = "\t\tNext Open: " + menuList[key].EventList[eventIndex].StartTime.ToShortTimeString() + " - " + menuList[key].EventList[eventIndex].EndTime.ToShortTimeString() + ", " + menuList[key].EventList[eventIndex].StartTime.ToString("ddd");
						portletDictionary[key].Heading.Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false, Items = new List<DropdownItem> { new DropdownItem { Description = titleTimeModifier, Disabled = true } } } };
					}
					for (int i = 1; i < menuList[key].EventList[eventIndex].Food.Count; i++)
					{
						if (!foodList.Contains(menuList[key].EventList[eventIndex].Food[i].Name) && !menuList[key].EventList[eventIndex].Food[i].Name.Contains("located"))
						{
							string nutritionInfo = "";
							if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[i].Serving))
								nutritionInfo += "Serving Size: " + menuList[key].EventList[eventIndex].Food[i].Serving + "<br/>";
							if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[i].Calories))
								nutritionInfo += "Calories: " + menuList[key].EventList[eventIndex].Food[i].Calories + "<br/>";
							if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[i].Fat))
								nutritionInfo += "Total Fat: " + menuList[key].EventList[eventIndex].Food[i].Fat + "<br/>";
							if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[i].Carbs))
								nutritionInfo += "Carbs: " + menuList[key].EventList[eventIndex].Food[i].Carbs + "<br/>";
							if (!string.IsNullOrEmpty(menuList[key].EventList[eventIndex].Food[i].Protein))
								nutritionInfo += "Protein: " + menuList[key].EventList[eventIndex].Food[i].Protein + "<br/>";
							if (string.IsNullOrEmpty(nutritionInfo))
								nutritionInfo = "Nutrition information is not available";
							portletDictionary[key].Content.Add(new Collapsible { Title = menuList[key].EventList[eventIndex].Food[i].Name, Collapsed = true, TitleFontSize = "xsmall", Content = new List<object> { new HtmlFragment { Html = nutritionInfo } }, BorderBottomStyle = "none", BorderTopStyle = "none" });
							foodList.Add(menuList[key].EventList[eventIndex].Food[i].Name);
							if (menuList[key].EventList[eventIndex].Food[i].Name.ToUpper().Contains("CLOSE"))
							{
								titleTimeModifier = "\t\tNext Open: " + menuList[key].EventList[eventIndex].StartTime.ToShortTimeString() + " - " + menuList[key].EventList[eventIndex].EndTime.ToShortTimeString() + ", " + menuList[key].EventList[eventIndex].StartTime.ToString("ddd");
								portletDictionary[key].Heading.Buttons = new List<object> { new Dropdown { AccessoryIcon = "deny", ShowDisclosureIcon = false, ToggleActionStyle = "destructive", InitiallyHidden = false, Items = new List<DropdownItem> { new DropdownItem { Description = titleTimeModifier, Disabled = true } } } };
							}
						}
					}
				}
				catch{
					portletDictionary[key].Content = new List<object> { new HtmlFragment { Html = "ERROR"}};
				}
                //portletDictionary[key].Heading.Heading = menuList[key].Title + titleTimeModifier;
            }

            //if (menuList["fivespice"].Open)
            //{
            //    //fiveSpicePortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };
            //    fiveSpicePortlet.Heading.Image = openImage;
            //}
            //if (menuList["flame"].Open)
            //    flamePortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };
            //if (menuList["fresh"].Open)
            //    freshPortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };
            //if (menuList["globalfusion"].Open)
            //    globalFusionPortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };
            //if (menuList["kitchentable"].Open)
            //    kitchenPortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };
            //if (menuList["scramblepastatoss"].Open)
            //    pastaPortlet.Content = new List<object> { new HtmlFragment { Html = "Open!!" } };

            threeColumn.PrimaryColumn = new XModComm.UI.Column.ColumnContents { Content = new List<object> { portletDictionary["nourish"], portletDictionary["kitchentable"], portletDictionary["scramblepastatoss"] } };
            threeColumn.SecondaryColumn1 = new XModComm.UI.Column.ColumnContents { Content = new List<object> { portletDictionary["fivespice"], portletDictionary["globalfusion"] } };
            threeColumn.SecondaryColumn2 = new XModComm.UI.Column.ColumnContents { Content = new List<object> { portletDictionary["flame"], portletDictionary["fresh"] } };


            BlockHeading title = new BlockHeading
            {
                Heading = "Today's U Dining Menu",
                HeadingTextColor = "#5c0e1d",
                ImageWidth = "2.25rem",
                ImageHeight = "2.25rem",
                MarginBottom = "tight",
                HeadingTextAlignment = "center",
                DescriptionLineClamp = 2,
                HeadingLineClamp = 2
            };

            response.Content.Add(title);
            response.Content.Add(threeColumn);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }

    }
}