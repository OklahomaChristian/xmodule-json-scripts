using System;
using Person;
using XModComm;
using XModComm.UI;
using XModComm.Links;
using System.IO;

namespace Dynamic
{
    public class Script
    {
        public bool TestJSON(TokenData tokenData, out Exception returnEx)
        {
            try
            {
                GetJSON(tokenData);
                returnEx = null;
            }
            catch (Exception ex)
            {
                returnEx = ex;
                return false;
            }
            return true;
        }

        public string GetJSON(TokenData tokenData = null)
        {
            XModComm.Response response = new Response();
            int rand = new Random().Next(1, 6);
            string randNumString = rand.ToString("00");

            string imagePath = "https://myocfiles.oc.edu/files/myocbanner/bg" + randNumString + ".jpg";
            Image bannerImage = new Image()
            {
                Url = imagePath,
                Alt = "An image at the top of myOC 3"
            };

            Hero banner = new Hero()
            {
                Height = "fluid",
                BackgroundImage = bannerImage,
                ContentContainerWidth = "full",
                Content = new System.Collections.Generic.List<object> { new HeroHeading { Heading = " ", TextAlignment = "left", MarginLeft = "2em", MarginBottom = "5em" } }
            };

            response.Content.Add(banner);

            return Person.Utilities.JSON.Serializer.Serialize(response);
        }
    }
}